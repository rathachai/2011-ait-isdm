package com.btnc.ila.test;

import java.util.*;
import junit.framework.TestCase;
import com.btnc.ila.controller.authentication.LoginController;
import com.btnc.ila.entity.task.*;
import com.btnc.ila.entity.user.*;
import com.btnc.ila.entity.common.Status;
import com.btnc.ila.entity.schedule.*;

public class UpdateTaskStatusTest  extends TestCase{
	
	Job task = new Job();
	
	public UpdateTaskStatusTest(){
		super();
	}
	
	@Override
	protected void setUp() throws Exception{
		
		
	}
	
	public void test_initialTask(){
		Status status = Status.UNKNOWN;
		task.setStatus(status);
		assertEquals(task.getStatus(), status);
	}
	
	public void test_SetTaskStatusToBeDone(){
		Status status = Status.DONE;
		task.setStatus(status);
		assertEquals(task.getStatus(), status);
	}
	
	public void test_SetTaskStatusToBeCancel(){
		Status status = Status.CANCEL;
		task.setStatus(status);
		assertEquals(task.getStatus(), status);
	}
	
	public void test_SetTaskStatusToBeWait(){
		Status status = Status.WAIT;
		task.setStatus(status);
		assertEquals(task.getStatus(), status);
	}
	
	public void test_SetTaskStatusToBeWaitThenCancel(){
		Status status = Status.WAIT;
		task.setStatus(status);
		status = Status.CANCEL;
		task.setStatus(status);
		assertEquals(task.getStatus(), status);
	}
	
	public void test_SetTaskStatusToBeWaitThenDone(){
		Status status = Status.WAIT;
		task.setStatus(status);
		status = Status.DONE;
		task.setStatus(status);
		assertEquals(task.getStatus(), status);
	}
	
	public void test_SetTaskStatusToBeDoneThenCancel(){
		Status status = Status.DONE;
		task.setStatus(status);
		status = Status.CANCEL;
		task.setStatus(status);
		assertEquals(task.getStatus(), status);
	}
	
	public void test_SetTaskStatusToBeDoneThenWait(){
		Status status = Status.DONE;
		task.setStatus(status);
		status = Status.WAIT;
		task.setStatus(status);
		assertEquals(task.getStatus(), status);
	}
	
	public void test_SetTaskStatusToBeCancelThenDone(){
		Status status = Status.CANCEL;
		task.setStatus(status);
		status = Status.DONE;
		task.setStatus(status);
		assertEquals(task.getStatus(), status);
	}

}
