package com.btnc.ila.test;

import java.util.*;
import junit.framework.TestCase;
import com.btnc.ila.controller.authentication.LoginController;
import com.btnc.ila.entity.task.*;
import com.btnc.ila.entity.user.*;
import com.btnc.ila.entity.schedule.*;

public class LoginTest extends TestCase {

	public LoginTest() {
		super();
	}
	
	@Override
	protected void setUp() throws Exception{
		
		
	}
	
	public void test_LoginWithCorrectUsernameAndPassword() {
		LoginController loginController = LoginController.getInstance();
		boolean res = loginController.validateUser("theerachai", "theerachai");
		assertTrue(res);
	}
	
	public void test_LoginWithCorrectUsernameAndWrongPassword() {
		LoginController loginController = LoginController.getInstance();
		boolean res = loginController.validateUser("therachai", "xxx");
		assertFalse(res);
	}
	
	public void test_LoginWithWrongUsernameAndCorrectPassword() {
		LoginController loginController = LoginController.getInstance();
		boolean res = loginController.validateUser("xxx", "theerachai");
		assertFalse(res);
	}
	
	public void test_LoginWithInvalidCharInUsername() {
		LoginController loginController = LoginController.getInstance();
		boolean res = loginController.validateUser("#*&%", "theerachai");
		assertFalse(res);
	}
	
	public void test_LoginWithInvalidCharInPassword() {
		LoginController loginController = LoginController.getInstance();
		boolean res = loginController.validateUser("therachai", "#*&%");
		assertFalse(res);
	}
	
	public void test_LoginWithEmptyUsername() {
		LoginController loginController = LoginController.getInstance();
		boolean res = loginController.validateUser("", "theerachai");
		assertFalse(res);
	}
	
	public void test_LoginWithEmptyPassword() {
		LoginController loginController = LoginController.getInstance();
		boolean res = loginController.validateUser("therachai", "");
		assertFalse(res);
	}
	
	public void test_LoginWithEmptyUsernameAndPassword() {
		LoginController loginController = LoginController.getInstance();
		boolean res = loginController.validateUser("", "");
		assertFalse(res);
	}
	
	public void test_LoginWithTooLongUsername() {
		LoginController loginController = LoginController.getInstance();
		boolean res = loginController.validateUser("theerachaitheerachaitheerachaitheerachaitheerachai", "theerachai");
		assertFalse(res);
	}
	
	public void test_LoginWithTooLongPassword() {
		LoginController loginController = LoginController.getInstance();
		boolean res = loginController.validateUser("therachai", "theerachaitheerachaitheerachaitheerachaitheerachai");
		assertFalse(res);
	}
	
	public void test_LoginWithTooLongUsernameAndPassword() {
		LoginController loginController = LoginController.getInstance();
		boolean res = loginController.validateUser("theerachaitheerachaitheerachaitheerachaitheerachai", "theerachaitheerachaitheerachaitheerachaitheerachai");
		assertFalse(res);
	}
	
	public void test_LoginWithUnsercureUsername() {
		LoginController loginController = LoginController.getInstance();
		boolean res = loginController.validateUser("or true", "theerachai");
		assertFalse(res);
	}
	
	public void test_LoginWithUnsercureUsernameWithPipe() {
		LoginController loginController = LoginController.getInstance();
		boolean res = loginController.validateUser("| true", "theerachai");
		assertFalse(res);
	}
	
	public void test_LoginWithUnsercurePassword() {
		LoginController loginController = LoginController.getInstance();
		boolean res = loginController.validateUser("therachai", "or true");
		assertFalse(res);
	}
	
	public void test_LoginWithUnsercurePasswordWithPipe() {
		LoginController loginController = LoginController.getInstance();
		boolean res = loginController.validateUser("therachai", "| true");
		assertFalse(res);
	}
	
	public void test_LoginWithUnsercureUsernameAndPassword() {
		LoginController loginController = LoginController.getInstance();
		boolean res = loginController.validateUser("| true", "| true");
		assertFalse(res);
	}
	
	public void test_LoginWithAllCapital() {
		LoginController loginController = LoginController.getInstance();
		boolean res = loginController.validateUser("THEERACHAI", "THEERACHAI");
		assertFalse(res);
	}
	
	public void test_LoginWithCapitalUsername() {
		LoginController loginController = LoginController.getInstance();
		boolean res = loginController.validateUser("THEERACHAI", "theerachai");
		assertFalse(res);
	}
	
	public void test_LoginWithCapitalPassword() {
		LoginController loginController = LoginController.getInstance();
		boolean res = loginController.validateUser("therachai", "THERACHAI");
		assertFalse(res);
	}
	
	public void test_NewDriver() {
		Driver d = new Driver();
		assertNotNull(d!=null);
	}
	
	public void test_NewDriverWithParams() {
		Driver d = new Driver("Theerachai", "Thee", "Theerachai","ASDFEREREW", null);
		assertNotNull(d!=null);
	}
	
	public void test_DriverName() {
		Driver d = new Driver();
		String name = "Theerachai";
		d.setFullName(name);
		assertEquals(d.getFullName(), name);
	}
	public void test_DriverNameWithEmptyValue() {
		Driver d = new Driver();
		String name = "";
		d.setFullName(name);
		assertEquals(d.getFullName(), name);
	}
	
	public void test_DriverNickName() {
		Driver d = new Driver();
		String name = "Theerachai";
		d.setNickName(name);
		assertEquals(d.getNickName(), name);
	}
	
	public void test_DriverNickNameWithEmptyValue() {
		Driver d = new Driver();
		String name = "";
		d.setNickName(name);
		assertEquals(d.getNickName(), name);
	}
	
	public void test_DriverRoute (){
		Driver d = new Driver();
		Route r =new Route();
		d.setRoute(r);
		assertEquals(d.getRoute(), r);
	}
	
	public void test_DriverPosList (){
		Driver d = new Driver();
		List<PointOfSale> lpos = new ArrayList<PointOfSale>();
		d.setRelatedPointOfSale(lpos);
		assertNotNull(d.getRelatedPointOfSale());
	}
	
	public void test_DriverUsername (){
		Driver d = new Driver();
		String name = "theerachai";
		d.setUserName(name);
		assertEquals(d.getUserName(), name);
	}

}

