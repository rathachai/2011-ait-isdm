package com.btnc.ila.test;

import java.util.*;

import junit.framework.TestCase;
import com.btnc.ila.entity.task.*;
import com.btnc.ila.entity.common.*;
import com.btnc.ila.entity.schedule.*;
import com.btnc.ila.entity.traffic.*;

public class GetScheduleTest extends TestCase {
	@Override
	protected void setUp() throws Exception{
		
		
	}
	
	public void test_NewPos() {
		PointOfSale p = new PointOfSale();
		assertNotNull(p);
	}
	public void test_SetPosName() {
		PointOfSale p = new PointOfSale();
		String name = "Central";
		p.setFullName(name);
		assertEquals(p.getFullName(),name);
		
	}
	public void test_PosIdentifier() {
		PointOfSale p = new PointOfSale();
		String name = "POS123";
		p.setIdentifier(name);
		assertEquals(p.getIdentifier(),name);
	}
	public void test_PosWarehouse() {
		PointOfSale p = new PointOfSale();
		assertFalse(p.isWareHouse());
		p.setWareHouse(true);
		assertTrue(p.isWareHouse());
	}
	public void test_PosDistance() {
		PointOfSale p = new PointOfSale();
		Hashtable<PointOfSale, Distance> h = new Hashtable<PointOfSale, Distance>();
		p.setDistances(h);
		assertEquals(p.getDistanceList(), h);
	}
	public void test_PosAfterHour() {
		PointOfSale p = new PointOfSale();
		int num = 5;
		p.setAfterHour(num);
		assertEquals(p.getAfterHour(), num);
	}
	public void test_NewTask() {
		Task t = new Task();
		assertNotNull(t);
	}
	public void test_TaskPos() {
		PointOfSale p = new PointOfSale();
		Task t = new Task();
		t.setSourcePointOfSale(p);
		assertEquals(t.getSourcePointOfSale(), p);
	}
	public void test_TaskPriority() {
		Task t = new Task();
		int num = 2;
		t.setPriority(num);
		assertEquals(t.getPriority(), num);
	}
	public void test_TaskCurrentStatus() {
		Task t = new Task();
		Status s = Status.CANCEL;
		t.setCurrentStatus(s);
		assertEquals(t.getCurrentStatus(), s);
	}
	public void test_TaskEnvelope() {
		Envelope e = new Envelope("EV123");
		Task t = new Task();
		t.setEnvelope(e);
		assertEquals(t.getEnvelope(), e);
	}
	public void test_TaskTransfer() {
		Task t = new Task();
		String num = "224";
		t.setTransferOrderId(num);
		assertEquals(t.getTransferOrderId(), num);
	}
	public void test_TaskType() {
		TaskType tt = TaskType.REFILL;
		Task t = new Task();
		t.setTaskType(tt);
		assertEquals(t.getTaskType(), tt);
	}
	public void test_TaskName() {
		Task t = new Task();
		assertNotNull(t);
	}
	public void test_NewRoute() {
		Route r = new Route();
		assertNotNull(r);
	}
	public void test_RouteTask() {
		Route r = new Route();
		List<Task> lt = new ArrayList<Task>();
		r.setTasks(lt);
		assertEquals(r.getTasks(), lt);
	}
	public void test_NewTraffyLink() {
		TraffyLink tf = new TraffyLink();
		assertNotNull(tf);
	}
	public void test_TraffyLinkPos() {
		TraffyLink tf = new TraffyLink();
		PointOfSale p = new PointOfSale();
		assertNotNull(tf);
		assertNotNull(p);
		p = new PointOfSale();
		assertNotNull(tf);
		assertNotNull(p);
	}
	public void test_TraffyLinkDistance() {
		TraffyLink tf = new TraffyLink();
		PointOfSale p = new PointOfSale();
		assertNotNull(tf);
		assertNotNull(p);
	}
	public void test_TraffyLinkTime() {
		TraffyLink tf = new TraffyLink();
		String ss ="high";
		tf.setStatus(ss);
		assertEquals(tf.getStatus(), ss);
	}
	public void test_TraffyLinkInBetween() {
		TraffyLink tf = new TraffyLink();
		PointOfSale p = new PointOfSale();
		assertNotNull(tf);
		assertNotNull(p);
	}
	public void test_NewSchedule() {
		Schedule s = new Schedule();
		assertNotNull(s);
	}
	public void test_ScheduleBySetShortDistanceWeight() {
		Schedule s = new Schedule();
		int num = 2;
		s.setShortDistanceWeight(num);
		assertEquals(s.getShortDistanceWeight(), num);
	}
	public void test_ScheduleBySetShorTimeWeight() {
		Schedule s = new Schedule();
		int num = 2;
		s.setShortTimeWeight(num);
		assertEquals(s.getShortTimeWeight(), num);
	}
	public void test_ScheduleBySetHighBusinessWeight() {
		Schedule s = new Schedule();
		int num = 2;
		s.setShortDistanceWeight(num);
		assertEquals(s.getShortDistanceWeight(), num);
	}
	public void test_ScheduleByBuildSchedule() {
		Schedule s = new Schedule();
		PointOfSale p = new PointOfSale();
		Hashtable<String, PointOfSale> ph = new Hashtable<String, PointOfSale>();
		ph.put("", p);
		s.setPointOfSaleHashTable(ph);
		s.setStartPointOfSale(p);
		s.setWareHouse(p);
		s.buildSchedule();
		int num = 2;
		s.setShortDistanceWeight(num);
		assertNotNull(s.getMissionList());
	}
	public void test_ScheduleByGetAllTime() {
		Schedule s = new Schedule();
		PointOfSale p = new PointOfSale();
		Hashtable<String, PointOfSale> ph = new Hashtable<String, PointOfSale>();
		ph.put("", p);
		s.setPointOfSaleHashTable(ph);
		s.setStartPointOfSale(p);
		s.setWareHouse(p);
		s.buildSchedule();
		int num = 2;
		s.setShortDistanceWeight(num);
		assertTrue(s.getAllTimeMin()>=0);
	}
	public void test_ScheduleByGetAllDistance() {
		Schedule s = new Schedule();
		PointOfSale p = new PointOfSale();
		Hashtable<String, PointOfSale> ph = new Hashtable<String, PointOfSale>();
		ph.put("", p);
		s.setPointOfSaleHashTable(ph);
		s.setStartPointOfSale(p);
		s.setWareHouse(p);
		s.buildSchedule();
		int num = 2;
		s.setShortTimeWeight(num);
		assertTrue(s.getAllTimeMin()>=0);
	}
	public void test_ScheduleByGetBusiness() {
		Schedule s = new Schedule();
		PointOfSale p = new PointOfSale();
		Hashtable<String, PointOfSale> ph = new Hashtable<String, PointOfSale>();
		ph.put("", p);
		s.setPointOfSaleHashTable(ph);
		s.setStartPointOfSale(p);
		s.setWareHouse(p);
		s.buildSchedule();
		int num = 2;
		s.setShortDistanceWeight(num);
		assertTrue(s.getAllDistanceKm()>=0);
	}
	public void test_GetDistanceCost() {
		Schedule s = new Schedule();
		PointOfSale p = new PointOfSale();
		Hashtable<String, PointOfSale> ph = new Hashtable<String, PointOfSale>();
		ph.put("", p);
		s.setPointOfSaleHashTable(ph);
		s.setStartPointOfSale(p);
		s.setWareHouse(p);
		s.buildSchedule();
		int num = 2;
		s.setBusinessWeight(num);
		assertTrue(s.getAllBusiness()>=0);
	}
	
	public void test_NewPosWithEmptyParameter() {
		PointOfSale p = new PointOfSale();
		assertNotNull(p);
	}
	public void test_SetPosNameWithNullParameter() {
		PointOfSale p = new PointOfSale();
		String name = "Central";
		p.setFullName(name);
		assertEquals(p.getFullName(),name);
		
	}
	public void test_PosIdentifierWithEmptyParameter() {
		PointOfSale p = new PointOfSale();
		String name = "POS123";
		p.setIdentifier(name);
		assertEquals(p.getIdentifier(),name);
	}
	public void test_PosWarehouseWithZeroValue() {
		PointOfSale p = new PointOfSale();
		assertFalse(p.isWareHouse());
		p.setWareHouse(true);
		assertTrue(p.isWareHouse());
	}
	public void test_PosDistanceWithZeroValue() {
		PointOfSale p = new PointOfSale();
		Hashtable<PointOfSale, Distance> h = new Hashtable<PointOfSale, Distance>();
		p.setDistances(h);
		assertEquals(p.getDistanceList(), h);
	}
	public void test_PosAfterHourWithEmptyParameter() {
		PointOfSale p = new PointOfSale();
		int num = 5;
		p.setAfterHour(num);
		assertEquals(p.getAfterHour(), num);
	}
	public void test_NewTaskWithNullParameter() {
		Task t = new Task();
		assertNotNull(t);
	}
	public void test_TaskPosWithNullParameter() {
		PointOfSale p = new PointOfSale();
		Task t = new Task();
		t.setSourcePointOfSale(p);
		assertEquals(t.getSourcePointOfSale(), p);
	}
	public void test_TaskPriorityWithEmptyParameter() {
		Task t = new Task();
		int num = 2;
		t.setPriority(num);
		assertEquals(t.getPriority(), num);
	}
	public void test_TaskCurrentStatusWithNullParameter() {
		Task t = new Task();
		Status s = Status.CANCEL;
		t.setCurrentStatus(s);
		assertEquals(t.getCurrentStatus(), s);
	}
	public void test_TaskEnvelopeWithNullParameter() {
		Envelope e = new Envelope("EV123");
		Task t = new Task();
		t.setEnvelope(e);
		assertEquals(t.getEnvelope(), e);
	}
	public void test_TaskTransferWithNullParameter() {
		Task t = new Task();
		String num = "224";
		t.setTransferOrderId(num);
		assertEquals(t.getTransferOrderId(), num);
	}
	public void test_TaskTypeWithNullParameter() {
		TaskType tt = TaskType.REFILL;
		Task t = new Task();
		t.setTaskType(tt);
		assertEquals(t.getTaskType(), tt);
	}
	public void test_TaskNameWithEmptyParameter() {
		Task t = new Task();
		assertNotNull(t);
	}
	public void test_NewRouteWithNullParameter() {
		Route r = new Route();
		assertNotNull(r);
	}
	public void test_RouteTaskWithNullParameter() {
		Route r = new Route();
		List<Task> lt = new ArrayList<Task>();
		r.setTasks(lt);
		assertEquals(r.getTasks(), lt);
	}
	public void test_NewTraffyLinkWithEmptyParameter() {
		TraffyLink tf = new TraffyLink();
		assertNotNull(tf);
	}
	public void test_TraffyLinkPosWithNullParameter() {
		TraffyLink tf = new TraffyLink();
		PointOfSale p = new PointOfSale();
		assertNotNull(tf);
		assertNotNull(p);
		p = new PointOfSale();
		assertNotNull(tf);
		assertNotNull(p);
	}
	public void test_TraffyLinkDistanceWithNullParameter() {
		TraffyLink tf = new TraffyLink();
		PointOfSale p = new PointOfSale();
		assertNotNull(tf);
		assertNotNull(p);
	}
	public void test_TraffyLinkTimeWithEmptyParameter() {
		TraffyLink tf = new TraffyLink();
		String ss ="high";
		tf.setStatus(ss);
		assertEquals(tf.getStatus(), ss);
	}
	public void test_TraffyLinkInBetweenWithNullParameter() {
		TraffyLink tf = new TraffyLink();
		PointOfSale p = new PointOfSale();
		assertNotNull(tf);
		assertNotNull(p);
	}
	public void test_NewScheduleWithEmptyParameter() {
		Schedule s = new Schedule();
		assertNotNull(s);
	}
	public void test_ScheduleBySetShortDistanceWeightWithNullParameter() {
		Schedule s = new Schedule();
		int num = 2;
		s.setShortDistanceWeight(num);
		assertEquals(s.getShortDistanceWeight(), num);
	}
	public void test_ScheduleBySetShorTimeWeightWithZeroValue() {
		Schedule s = new Schedule();
		int num = 0;
		s.setShortTimeWeight(num);
		assertEquals(s.getShortTimeWeight(), num);
	}
	public void test_ScheduleBySetHighBusinessWeightWithEmptyParameter() {
		Schedule s = new Schedule();
		int num = 2;
		s.setShortDistanceWeight(num);
		assertEquals(s.getShortDistanceWeight(), num);
	}
	public void test_ScheduleByBuildScheduleWithNullParameter() {
		Schedule s = new Schedule();
		PointOfSale p = new PointOfSale();
		Hashtable<String, PointOfSale> ph = new Hashtable<String, PointOfSale>();
		ph.put("", p);
		s.setPointOfSaleHashTable(ph);
		s.setStartPointOfSale(p);
		s.setWareHouse(p);
		s.buildSchedule();
		int num = 2;
		s.setShortDistanceWeight(num);
		assertNotNull(s.getMissionList());
	}
	public void test_ScheduleByGetAllTimeWithEmptyParameter() {
		Schedule s = new Schedule();
		PointOfSale p = new PointOfSale();
		Hashtable<String, PointOfSale> ph = new Hashtable<String, PointOfSale>();
		ph.put("", p);
		s.setPointOfSaleHashTable(ph);
		s.setStartPointOfSale(p);
		s.setWareHouse(p);
		s.buildSchedule();
		int num = 2;
		s.setShortDistanceWeight(num);
		assertTrue(s.getAllTimeMin()>=0);
	}
	public void test_ScheduleByGetAllDistanceWithZeroValue() {
		Schedule s = new Schedule();
		PointOfSale p = new PointOfSale();
		Hashtable<String, PointOfSale> ph = new Hashtable<String, PointOfSale>();
		ph.put("", p);
		s.setPointOfSaleHashTable(ph);
		s.setStartPointOfSale(p);
		s.setWareHouse(p);
		s.buildSchedule();
		int num = 0;
		s.setShortTimeWeight(num);
		assertTrue(s.getAllTimeMin()>=0);
	}
	public void test_ScheduleByGetBusinessWithZeroValue() {
		Schedule s = new Schedule();
		PointOfSale p = new PointOfSale();
		Hashtable<String, PointOfSale> ph = new Hashtable<String, PointOfSale>();
		ph.put("", p);
		s.setPointOfSaleHashTable(ph);
		s.setStartPointOfSale(p);
		s.setWareHouse(p);
		s.buildSchedule();
		int num = 0;
		s.setShortDistanceWeight(num);
		assertTrue(s.getAllDistanceKm()>=0);
	}
	public void test_GetDistanceCostWithZeroValue() {
		Schedule s = new Schedule();
		PointOfSale p = new PointOfSale();
		Hashtable<String, PointOfSale> ph = new Hashtable<String, PointOfSale>();
		ph.put("", p);
		s.setPointOfSaleHashTable(ph);
		s.setStartPointOfSale(p);
		s.setWareHouse(p);
		s.buildSchedule();
		int num = 0;
		s.setBusinessWeight(num);
		assertTrue(s.getAllBusiness()>=0);
	}
}
