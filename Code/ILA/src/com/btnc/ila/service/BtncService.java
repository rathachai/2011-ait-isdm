package com.btnc.ila.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

import android.util.Log;

import com.btnc.ila.entity.common.Product;
import com.btnc.ila.entity.common.Status;
import com.btnc.ila.entity.common.TaskType;
import com.btnc.ila.entity.task.Envelope;
import com.btnc.ila.entity.task.PointOfSale;
import com.btnc.ila.entity.task.Task;
import com.btnc.ila.entity.user.Driver;
import com.btnc.ila.entity.user.Person;
import com.btnc.ila.entity.user.Role;
import com.btnc.ila.service.iface.BtncServiceIface;

public class BtncService implements BtncServiceIface {

	// This is Temporary
	// BTNC system return secure token for each user login

	private SecureRandom secureRandom = new SecureRandom();

	public String nextSessionId() {
		return new BigInteger(130, secureRandom).toString(16);
	}

	public Timestamp currentTimestamp() {
		return new Timestamp(Calendar.getInstance().getTime().getTime());
	}

	public boolean validateUser(String username, String password) {
		// Call BTNC System to validate the user
		if (username.trim().equals("theerachai"))
			return true;
		return false;
	}

	public Person getValidUserDetail(String username) {
		// Call BTNC System to get validated user detail
		// Parsing of information
		// Suppose Driver
		// Create Person
		Person person = new Driver("Theerachai", "Theerachai", "theerachai",
				nextSessionId(), currentTimestamp());
		if (person.getRole().equals(Role.DRIVER)) {
			// Get Driver related Point of Sale
			// Mock up
//			String[] driverPos = "777,S38,302,118,360,309,336,331".split(",");
			Driver driver = (Driver) person;
			List<PointOfSale> driverPosList = new ArrayList<PointOfSale>();
			try {
				Locale eng = Locale.ENGLISH;
				ResourceBundle bundle = ResourceBundle.getBundle(
						"com.btnc.ila.pos.detail", eng);
				Enumeration<String> enumeration = bundle.getKeys();
				while (enumeration.hasMoreElements()) {
					String pos = enumeration.nextElement();
					String[] posDetail = bundle.getString(pos).split(",");
					driverPosList.add(new PointOfSale(pos, posDetail[0],
							posDetail[1], Double.valueOf(posDetail[2])
									.doubleValue(), Double
									.valueOf(posDetail[3]).doubleValue(),
							Boolean.valueOf(posDetail[4]).booleanValue(),
							Integer.valueOf(posDetail[5]).intValue()));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			driver.setRelatedPointOfSale(driverPosList);
			return driver;
		}
		return person;
	}

	private PointOfSale checkPointOfSaleStatus(String posId) {
		if (posId.equals("777")) {
			return new PointOfSale(posId, true, 0);
		} else if (posId.equals("302")) {
			return new PointOfSale(posId, false, 16);
		} else if (posId.equals("309")) {
			return new PointOfSale(posId, false, 16);
		} else {
			return new PointOfSale(posId);
		}
	}

	public List<Task> getDriverTask(Person driver, String path)
			throws FileNotFoundException, IOException, ParseException {
		// Call BTNC System to get csv task list for driver
		// parse csv and extract each Task

		return parseCSVFile(path);

	}

	private List<Task> parseCSVFile(String localCSVArchive)
			throws FileNotFoundException, IOException, ParseException {

		List<Task> driverTaskList = new ArrayList<Task>();
		// Create BufferedReader to read CSV File
		BufferedReader br = new BufferedReader(new FileReader(localCSVArchive));
		String strLine = "";
		StringTokenizer st = null;
		Task task;

		SimpleDateFormat format = new SimpleDateFormat("M/dd/yy HH:mm");

		// Read comma separated file line by line
		while ((strLine = br.readLine()) != null) {

			st = new StringTokenizer(strLine, ",");
			while (st.hasMoreTokens()) {
				task = new Task();
				task.setEnvelope(new Envelope(st.nextToken()));
				task.setSourcePointOfSale(new PointOfSale(st.nextToken()));
				task.setDestinationPointOfSale(new PointOfSale(st.nextToken()));
				task.setProduct(Product.valueOf(st.nextToken()));
				task.setTransferOrderId(st.nextToken());
				task.setCreatedDate(format.parse(st.nextToken()));
				task.setTaskType(TaskType.valueOf(st.nextToken()));
				task.setPriority(Integer.parseInt(st.nextToken()));
				task.setCurrentStatus(Status.valueOf(st.nextToken()));
				task.setCurrentLocation(st.nextToken());
				task.setCurrentRecordedDate(new Date(currentTimestamp().getTime()));
				driverTaskList.add(task);
			}
		}

		new File(localCSVArchive).deleteOnExit();
		return driverTaskList;
	}

}
