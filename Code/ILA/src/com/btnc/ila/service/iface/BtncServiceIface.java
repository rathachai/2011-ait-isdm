package com.btnc.ila.service.iface;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import com.btnc.ila.entity.task.Task;
import com.btnc.ila.entity.user.Person;

public interface BtncServiceIface {

	public boolean validateUser(String username, String password);
	
	public Person getValidUserDetail(String username);
	
	public List<Task> getDriverTask(Person driver, String path) throws FileNotFoundException, IOException, ParseException;

}
