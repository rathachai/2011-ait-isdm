package com.btnc.ila.service.iface;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import com.btnc.ila.entity.traffic.Distance;
import com.btnc.ila.entity.traffic.Link;
import com.btnc.ila.entity.traffic.News;
import com.btnc.ila.entity.traffic.TraffyLink;

public interface TraffyServiceIface {

	public List<News> getIncidentNews(Date startDate) throws Exception;
	
	public TraffyLink getTrafficCongestion(Link link);
	
	public List<Distance> getDistanceAndLinksBetweenPos(String path) throws FileNotFoundException, IOException;

}
