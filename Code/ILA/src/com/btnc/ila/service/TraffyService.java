package com.btnc.ila.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.URI;
import java.security.MessageDigest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.xml.sax.InputSource;

import com.btnc.ila.entity.task.PointOfSale;
import com.btnc.ila.entity.traffic.Distance;
import com.btnc.ila.entity.traffic.Link;
import com.btnc.ila.entity.traffic.News;
import com.btnc.ila.entity.traffic.TraffyLink;
import com.btnc.ila.parser.SAXIncidentParser;
import com.btnc.ila.parser.SAXTrafficCongestionParser;
import com.btnc.ila.service.iface.TraffyServiceIface;

public class TraffyService implements TraffyServiceIface {

	private ResourceBundle bundle = ResourceBundle.getBundle(
			"com.btnc.ila.config.traffy", Locale.ENGLISH);

	private String appId = bundle.getString("appId");
	private String hiddenKey = bundle.getString("hiddenKey");
	private String getKeyUrl = bundle.getString("getKeyUrl");
	private String apitraffyUrl = bundle.getString("apitraffyUrl");
	private String key = "";
	private String passKey = "";

	public TraffyLink getTrafficCongestion(Link link) {
		try {
			// String appid = "00001441";
			// String keytext = "";
			// String hiddenkey = "nkgkvY8YyM";
			String url = getKeyUrl + appId;

			key = httpGetSimple(url);
			passKey = MD5(appId + key.trim()) + MD5(hiddenKey + key.trim());

			url = apitraffyUrl + "?format=XML&api=getCL&q=all&appid=" + appId
					+ "&key=" + passKey + "&linkid=" + link.getIdentifier();

			Iterator<TraffyLink> traffyLinkItr = httpGetTrafficCongestion(url)
					.iterator();
			while (traffyLinkItr.hasNext()) {
				TraffyLink traffyLink = (TraffyLink) traffyLinkItr.next();
				return traffyLink;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String httpGetSimple(String url) throws Exception {
		BufferedReader in = null;
		String result = "";
		try {
			HttpClient client = new DefaultHttpClient();
			HttpGet request = new HttpGet();
			request.setURI(new URI(url));
			HttpResponse response = client.execute(request);
			in = new BufferedReader(new InputStreamReader(response.getEntity()
					.getContent()));
			StringBuffer sb = new StringBuffer("");
			String line = "";
			String NL = System.getProperty("line.separator");
			while ((line = in.readLine()) != null) {
				sb.append(line + NL);
			}
			in.close();
			result = sb.toString();
		} finally {
		}

		return result;
	}

	public List<TraffyLink> httpGetTrafficCongestion(String url)
			throws ParseException, Exception {
		BufferedReader in = null;
		List<TraffyLink> traffyLinkList = null;
		try {
			HttpClient client = new DefaultHttpClient();
			HttpGet request = new HttpGet();
			request.setURI(new URI(url));
			HttpResponse response = client.execute(request);
			in = new BufferedReader(new InputStreamReader(response.getEntity()
					.getContent(), "UTF-8"));

			InputSource is = new InputSource(in);
			is.setEncoding("UTF-8");

			SAXTrafficCongestionParser saxTraffyParser = new SAXTrafficCongestionParser();
			traffyLinkList = saxTraffyParser.run(is);

			in.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return traffyLinkList;
	}

	public static String MD5(String text) throws Exception {
		final MessageDigest m = MessageDigest.getInstance("MD5");
		byte[] data = text.getBytes();
		m.update(data, 0, data.length);
		BigInteger i = new BigInteger(1, m.digest());
		return String.format("%1$032X", i);
	}

	public List<Distance> getDistanceAndLinksBetweenPos(String path)
			throws FileNotFoundException, IOException {
		List<Distance> distanceAndLinks = new ArrayList<Distance>();

		BufferedReader br = new BufferedReader(new FileReader(path));
		String strLine = "";
		StringTokenizer st = null;

		while ((strLine = br.readLine()) != null) {
			st = new StringTokenizer(strLine, ",");
			while (st.hasMoreTokens()) {
				distanceAndLinks.add(new Distance(new PointOfSale(st
						.nextToken()), new PointOfSale(st.nextToken()), Float
						.parseFloat(st.nextToken()), Integer.parseInt(st
						.nextToken()), getLinkList(st.nextToken(), ">")));
			}
		}
		new File(path).deleteOnExit();
		return distanceAndLinks;
	}

	private List<Link> getLinkList(String links, String deliminator) {
		List<Link> linkList = new ArrayList<Link>();
		String[] linksArray = links.split(deliminator);
		for (String link : linksArray) {
			linkList.add(new Link(link));
		}
		return linkList;
	}

	public List<News> getIncidentNews(Date startDate) throws Exception {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String url = getKeyUrl + appId;
		key = httpGetSimple(url);
		passKey = MD5(appId + key.trim()) + MD5(hiddenKey + key.trim());

		url = apitraffyUrl + "?format=XML&api=getIncident&appid=" + appId
				+ "&key=" + passKey + "&limit=10&offset=0&type=all&from="
				+ format.format(startDate);

		BufferedReader in = null;
		List<News> newsList = null;
		try {
			HttpClient client = new DefaultHttpClient();
			HttpGet request = new HttpGet();
			request.setURI(new URI(url));
			HttpResponse response = client.execute(request);
			in = new BufferedReader(new InputStreamReader(response.getEntity()
					.getContent(), "UTF-8"));

			InputSource is = new InputSource(in);
			is.setEncoding("UTF-8");

			SAXIncidentParser saxIncidentParser = new SAXIncidentParser();
			saxIncidentParser.run(is);

			in.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return newsList;
	}

}
