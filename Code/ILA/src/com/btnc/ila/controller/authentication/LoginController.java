package com.btnc.ila.controller.authentication;

import com.btnc.ila.controller.BaseController;
import com.btnc.ila.entity.user.Person;
import com.btnc.ila.service.BtncService;
import com.btnc.ila.service.iface.BtncServiceIface;

public class LoginController extends BaseController {

	private static LoginController lcInstance;

	private BtncServiceIface btncService = new BtncService();

	private LoginController() {

	}

	public static LoginController getInstance() {
		if (lcInstance == null) {
			lcInstance = new LoginController();
		}
		return lcInstance;
	}

	public boolean validateUser(String username, String password) {
		return btncService.validateUser(username, password);
	}

	public Person getValidUserDetail(String username) {
		Person person = (Person) btncService.getValidUserDetail(username);
		storePerson(person);
		return person;
	}

	public void storePerson(Person person) {
		initialize();
		getDb4oManager().store(person);
		commitAll();
	}

}
