package com.btnc.ila.controller.logistic;

import java.util.ArrayList;
import java.util.List;

import com.btnc.ila.controller.BaseController;
import com.btnc.ila.entity.task.Task;

public class SubmitStatusController extends BaseController {

	private static SubmitStatusController sscInstance;

	private SubmitStatusController() {

	}

	public static SubmitStatusController getInstance() {
		if (sscInstance == null) {
			sscInstance = new SubmitStatusController();
		}
		return sscInstance;
	}

	List<Task> currentDriverTask = new ArrayList<Task>();

	@SuppressWarnings("unchecked")
	private void getDriverTaskFromDb(){
		// Get Driver Tasks
//		initialize();
//		currentDriverTask = (List<Task>) getDb4oManager().retrieve(new Task());
//		Log.d("size", currentDriverTask.size()+"");
//		commitAll();
		
		ScheduleViewingController scheduleViewingController = ScheduleViewingController.getInstance();
		currentDriverTask = scheduleViewingController.getDriverTasks();
	}
	public List<Task> getDriverTaskList(String userName) {
		getDriverTaskFromDb();
		
//		
//		Task mockTask = new Task();
//		mockTask.setEnvelope(new Envelope("32333333"));
//		mockTask.setSourcePointOfSale(new PointOfSale("302"));
//		mockTask.setDestinationPointOfSale(new PointOfSale("334"));
//		mockTask.setCurrentStatus(Status.DELIVERED);
//		mockTask.setCurrentRecordedDate(new Date());
//		currentDriverTask.add(mockTask);
//
//		mockTask = new Task();
//		mockTask.setEnvelope(new Envelope("554455554"));
//		mockTask.setSourcePointOfSale(new PointOfSale("430"));
//		mockTask.setDestinationPointOfSale(new PointOfSale("343"));
//		mockTask.setCurrentStatus(Status.DELIVERED);
//		mockTask.setCurrentRecordedDate(new Date());
//		currentDriverTask.add(mockTask);
//
//		mockTask = new Task();
//		mockTask.setEnvelope(new Envelope("223332333"));
//		mockTask.setSourcePointOfSale(new PointOfSale("360"));
//		mockTask.setDestinationPointOfSale(new PointOfSale("343"));
//		mockTask.setCurrentStatus(Status.ENROUTE);
//		mockTask.setCurrentRecordedDate(new Date());
//		currentDriverTask.add(mockTask);

		return currentDriverTask;
	}

	public void sendStatusReport(String statusInXml) {
		// do Something
	}
}
