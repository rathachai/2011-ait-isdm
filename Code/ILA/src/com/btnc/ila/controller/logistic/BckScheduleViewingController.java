package com.btnc.ila.controller.logistic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.btnc.ila.controller.BaseController;
import com.btnc.ila.entity.common.Status;

public class BckScheduleViewingController extends BaseController {

	private List<Map<String, String>> planList = new ArrayList<Map<String, String>>();
	private List<Map<String, String>> detailPlanList = new ArrayList<Map<String, String>>();
	private List<Map<String, String>> individualPOS = new ArrayList<Map<String, String>>();

	private Map<String, String> genericMap = new HashMap<String, String>();

	private static final String PLAN = "PLAN";
	private static final String TIME = "TIME";
	private static final String DISTANCE = "DISTANCE";
	private static final String PATH = "PATH";
	private static final String POS = "POS";
	private static final String POSNAME = "POSNAME";
	private static final String DETAIL = "DETAIL";

	private int posPosition = 0;

	public BckScheduleViewingController() {
		// TODO Auto-generated constructor stub
	}

	public void setPosPosition(int posPosition) {
		this.posPosition = posPosition;
	}

	public List<Map<String, String>> getPlanList() {
		// Query to Interface
		// Mocking for Testing
		planList = new ArrayList<Map<String, String>>();

		genericMap = new HashMap<String, String>();
		genericMap.put(PLAN, "Plan 1");
		genericMap.put(TIME, new String(154 / 60 + ""));
		genericMap.put(DISTANCE, "116.9 KM");
		genericMap.put(PATH, convertListToString(mockPathList(0)));
		planList.add(genericMap);

		genericMap = new HashMap<String, String>();
		genericMap.put(PLAN, "Plan 2");
		genericMap.put(TIME, new String(134 / 60 + ""));
		genericMap.put(DISTANCE, "87.8 KM");
		genericMap.put(PATH, convertListToString(mockPathList(1)));
		planList.add(genericMap);

		return planList;
	}

	public List<HashMap<String, String>> getDetailPlanList() {
		// Query to Interface
		// Mocking for Testing
		return mockPathList(posPosition);
	}

	public List<Map<String, String>> getIndividualPOS() {
		individualPOS.clear();

		genericMap.clear();
		genericMap.put(DETAIL, "No Detail");
		genericMap.put("STATUS", getPosStatus(posPosition));
		individualPOS.add(genericMap);
		return individualPOS;
	}

	private Map<Integer, String> posStatus = new HashMap<Integer, String>();

	public void setPosStatus(Integer position, String status) {
		if (posStatus.containsKey(position)) {
			posStatus.remove(position);
		}
		posStatus.put(position, status);
	}

	public String getPosStatus(Integer position) {
		if (posStatus.containsKey(position)) {
			return posStatus.get(position);
		}
		return Status.UNKNOWN.getStatus();
	}

	private String convertListToString(List<HashMap<String, String>> mapList) {
		String setToString = "";
		Iterator<HashMap<String, String>> itr = mapList.iterator();
		while (itr.hasNext()) {
			setToString = setToString + "->" + (String) itr.next().get("POS");
		}
		setToString = setToString.substring(0, setToString.lastIndexOf("->"));
		return setToString;
	}

	private List<HashMap<String, String>> mockPathList(int key) {
		List<HashMap<String, String>> posSet = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> det = new HashMap<String, String>();
		switch (key) {
		case 0:
			posSet = new ArrayList<HashMap<String, String>>();
			det = new HashMap<String, String>();
			det.put("POS", "777");
			det.put("DETAIL", "BTNC WareHouse");
			posSet.add(det);
			det = new HashMap<String, String>();
			det.put("POS", "360");
			det.put("DETAIL", "Tung-Hua-Seng Banglumpoo");
			posSet.add(det);
			det = new HashMap<String, String>();
			det.put("POS", "118");
			det.put("DETAIL", "GL Pinklao");
			posSet.add(det);
			det = new HashMap<String, String>();
			det.put("POS", "S38");
			det.put("DETAIL", "Booth Market Rama 2");
			posSet.add(det);
			det = new HashMap<String, String>();
			det.put("POS", "336");
			det.put("DETAIL", "The Mall Bangkae");
			posSet.add(det);
			det = new HashMap<String, String>();
			det.put("POS", "302");
			det.put("DETAIL", "CT Rama 2");
			posSet.add(det);
			det = new HashMap<String, String>();
			det.put("POS", "777");
			det.put("DETAIL", "BTNC WareHouse");
			posSet.add(det);
			return posSet;
		case 1:
			posSet = new ArrayList<HashMap<String, String>>();
			det = new HashMap<String, String>();
			det.put("POS", "777");
			det.put("DETAIL", "BTNC WareHouse");
			posSet.add(det);
			det = new HashMap<String, String>();
			det.put("POS", "360");
			det.put("DETAIL", "Tung-Hua-Seng Banglumpoo");
			posSet.add(det);
			det = new HashMap<String, String>();
			det.put("POS", "118");
			det.put("DETAIL", "GL Pinklao");
			posSet.add(det);
			det = new HashMap<String, String>();
			det.put("POS", "S38");
			det.put("DETAIL", "Booth Market Rama 2");
			posSet.add(det);
			det = new HashMap<String, String>();
			det.put("POS", "302");
			det.put("DETAIL", "CT Rama 2");
			posSet.add(det);
			det = new HashMap<String, String>();
			det.put("POS", "336");
			det.put("DETAIL", "The Mall Bangkae");
			posSet.add(det);
			det = new HashMap<String, String>();
			det.put("POS", "777");
			det.put("DETAIL", "BTNC WareHouse");
			posSet.add(det);
			return posSet;
		default:
			return posSet;
		}
	}

	private List<String> mockPosDetail(int key) {
		List<String> ind = new ArrayList<String>();
		switch (key) {
		case 0:
			ind = new ArrayList<String>();
			ind.add("Identifier : 777");
			ind.add("Detail : BTNC Warehouse");
			return ind;
		case 1:
			ind = new ArrayList<String>();
			ind.add("Identifier : 360");
			ind.add("Detail : Tung-Hua-Seng Banglumpoo");
			return ind;
		case 2:
			ind = new ArrayList<String>();
			ind.add("Identifier : 118");
			ind.add("Detail : GL Pinklao");
			return ind;
		case 3:
			ind = new ArrayList<String>();
			ind.add("Identifier : 334");
			ind.add("Detail : XXX");
			return ind;
		case 4:
			ind = new ArrayList<String>();
			ind.add("Identifier : S38");
			ind.add("Detail : Booth Market Rama 2");
			return ind;
		case 5:
			ind = new ArrayList<String>();
			ind.add("Identifier : 336");
			ind.add("Detail : The Mall Bangkae");
			return ind;
		case 6:
			ind = new ArrayList<String>();
			ind.add("Identifier : 302");
			ind.add("Detail : CT Rama 2");
			return ind;
		case 7:
			ind = new ArrayList<String>();
			ind.add("Identifier : 777");
			ind.add("Detail : BTNC Warehouse");
			return ind;

		default:
			return ind;
		}

	}
}
