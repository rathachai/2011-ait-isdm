package com.btnc.ila.controller.logistic;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import android.util.Log;

import com.btnc.ila.R;
import com.btnc.ila.controller.BaseController;
import com.btnc.ila.entity.common.Status;
import com.btnc.ila.entity.common.TaskType;
import com.btnc.ila.entity.schedule.Schedule;
import com.btnc.ila.entity.schedule.ScheduleComparator;
import com.btnc.ila.entity.task.Envelope;
import com.btnc.ila.entity.task.Job;
import com.btnc.ila.entity.task.Mission;
import com.btnc.ila.entity.task.PointOfSale;
import com.btnc.ila.entity.task.Task;
import com.btnc.ila.entity.traffic.Distance;
import com.btnc.ila.entity.user.Driver;
import com.btnc.ila.service.BtncService;
import com.btnc.ila.service.TraffyService;
import com.btnc.ila.service.iface.BtncServiceIface;
import com.btnc.ila.service.iface.TraffyServiceIface;
import com.db4o.query.Predicate;

public class ScheduleViewingController extends BaseController {

	private static ScheduleViewingController svcInstance;

	private BtncServiceIface btncService = new BtncService();
	private TraffyServiceIface traffyService = new TraffyService();

	private List<Task> driverTasks = new ArrayList<Task>();
	private List<Distance> distanceList = new ArrayList<Distance>();
	PointOfSale wareHouse;
	LinkedList<PointOfSale> posList = new LinkedList<PointOfSale>();
	Hashtable<String, PointOfSale> posHash = new Hashtable<String, PointOfSale>();
	LinkedList<Job> jobList = new LinkedList<Job>();
	LinkedList<Distance> routeList = new LinkedList<Distance>();
	LinkedList<Schedule> scheduleList = new LinkedList<Schedule>();

	// Three type of Schedules
	Hashtable<String, Schedule> scheduleTable = new Hashtable<String, Schedule>();
	private static String SHORTTIME = "SHORTTIME";
	private static String SHORTDISTANCE = "SHORTDISTANCE";
	private static String GOODBUSINESS = "GOODBUSINESS";

	Hashtable<String, String> scheduleNameDetail = new Hashtable<String, String>();

	private String selectedSchedule;
	private boolean isScheduleSelected = false;

	private PointOfSale startPointOfSale;

	private ScheduleViewingController() {

		scheduleNameDetail.put(SHORTTIME, "Shortest Time Based");
		scheduleNameDetail.put(SHORTDISTANCE, "Shortest Distance Based");
		scheduleNameDetail.put(GOODBUSINESS, "Priority Based");
	}

	public static ScheduleViewingController getInstance() {
		if (svcInstance == null) {
			svcInstance = new ScheduleViewingController();
		}
		return svcInstance;
	}

	@SuppressWarnings("serial")
	public Driver getDriverFromUserName(final String username) {
		initialize();
		Driver driver = (Driver) getDb4oManager().retrieveByPredicate(
				new Predicate<Object>() {

					@Override
					public boolean match(Object object) {
						Driver d = (Driver) object;
						return d.getUserName().trim().equals(username);
					}
				}).next();
		commitAll();
		return driver;
	}

	public String getSelectedSchedule() {
		return selectedSchedule;
	}

	public void setSelectedSchedule(String selectedSchedule) {
		this.selectedSchedule = selectedSchedule;
	}

	public boolean isScheduleSelected() {
		return isScheduleSelected;
	}

	public void setScheduleSelected(boolean isScheduleSelected) {
		this.isScheduleSelected = isScheduleSelected;
	}

	public Hashtable<String, String> getScheduleNameDetail() {
		return scheduleNameDetail;
	}

	public List<Map<String, String>> getPosPlanDetail(String username,
			String planId) {
		List<Map<String, String>> posSet = new ArrayList<Map<String, String>>();
		Map<String, String> det = new HashMap<String, String>();
		Schedule s = scheduleTable.get(planId);
		for (Mission m : s.getMissionList()) {
			det = new HashMap<String, String>();
			det.put("POS", m.getPlace().getIdentifier());
			det.put("POSNAME", m.getPlace().getFullName());
			det.put("ICON", checkAllJobDone(m));
			SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
			det.put("TIME", format.format(m.getEstimatedTime().getTime()));
			det.put("PRIORITY", m.getPriorty() + "");
			det.put("AFTERHOUR", m.getAfterHour() + "");

			posSet.add(det);
		}
		Collections.reverse(posSet);
		return posSet;
	}

	private String checkAllJobDone(Mission mission) {
		for (Job job : mission.getJobList()) {
			if (!job.getStatus().equals(Status.DONE)) {
				return null;
			}
		}
		return String.valueOf(R.drawable.check);
	}

	public List<Map<String, String>> getPosTasks(String username,
			String planId, String posId, int location) {
		List<Map<String, String>> posTasks = new ArrayList<Map<String, String>>();
		Map<String, String> map = new HashMap<String, String>();
		Schedule s = scheduleTable.get(planId);
		Mission m = s.getMissionList().get(location);
		for (Job j : m.getJobList()) {
			map = new HashMap<String, String>();
			map.put("ENVELOPE", j.getTask().getEnvelope().getIdentifier());
			map.put("ACTION", j.getAction().getType());
			map.put("STATE", j.getStatus().getStatus());
			map.put("ICON", jobIcon(j.getStatus()));
			posTasks.add(map);
		}
		Collections.reverse(posTasks);
		return posTasks;
	}

	private String jobIcon(Status jobStatus) {
		if (jobStatus.equals(Status.DONE)) {
			return String.valueOf(R.drawable.done);
		} else if (jobStatus.equals(Status.WAIT)) {
			return String.valueOf(R.drawable.wait);
		} else if (jobStatus.equals(Status.CANCEL)) {
			return String.valueOf(R.drawable.cancel);
		} else {
			return null;
		}
	}

	public List<Map<String, String>> getSchedule(Driver driver) {
		List<Map<String, String>> planList = new ArrayList<Map<String, String>>();
		Map<String, String> map = new HashMap<String, String>();

		if (isScheduleSelected) {
			Schedule s = scheduleTable.get(selectedSchedule);
			map = new HashMap<String, String>();
			map.put("ID", selectedSchedule);
			map.put("TIME", String.valueOf(formatIntoHHMMSS(Double.valueOf(
					s.getAllTimeMin()).intValue())));
			map.put("DISTANCE", String.valueOf(s.getAllDistanceKm()));
			map.put("PRIORITY", String.valueOf(s.getAllBusiness()));
			map.put("FLOW", getMissionFlow(s));
			map.put("ICON", checkAllTaskDone());
			planList.add(map);
			return planList;
		}
		for (String key : scheduleTable.keySet()) {
			Schedule s = scheduleTable.get(key);
			map = new HashMap<String, String>();
			map.put("ID", key);
			map.put("TIME", String.valueOf(formatIntoHHMMSS(Double.valueOf(
					s.getAllTimeMin()).intValue())));
			map.put("DISTANCE", String.valueOf(s.getAllDistanceKm()));
			map.put("PRIORITY", String.valueOf(s.getAllBusiness()));
			map.put("FLOW", getMissionFlow(s));
			map.put("ICON", null);
			planList.add(map);
		}
		return planList;
	}

	private String checkAllTaskDone() {
		for (Task task : driverTasks) {
			if (!task.getCurrentStatus().equals(Status.DELIVERED)) {
				return null;
			}
		}
		return String.valueOf(R.drawable.check);
	}

	private String getMissionFlow(Schedule schedule) {
		String flow = "";
		for (Mission m : schedule.getMissionList()) {
			flow += m.getPlace().getIdentifier() + ", ";
		}
		return flow.substring(0, flow.lastIndexOf(", "));
	}

	private String formatIntoHHMMSS(int minsIn) {
		int secsIn = minsIn * 60;
		int hours = secsIn / 3600, remainder = secsIn % 3600, minutes = remainder / 60, seconds = remainder % 60;

		return ((hours < 10 ? "0" : "") + hours + ":"
				+ (minutes < 10 ? "0" : "") + minutes + ":"
				+ (seconds < 10 ? "0" : "") + seconds);

	}

	public void calculateSchedule(String username) {
		calculateSchedule(getDriverFromUserName(username));
	}

	public void calculateJobList(Driver driver) {
		// Do this JUST once
		Log.d("CALL", "JUST ONCE");

		// Initialize Pos lists
		posList = new LinkedList<PointOfSale>(driver.getRelatedPointOfSale());
		posHash = new Hashtable<String, PointOfSale>();
		for (PointOfSale pos : posList) {
			if (pos.isWareHouse()) {
				wareHouse = pos;
			}
			posHash.put(pos.getIdentifier(), pos);
		}
		// Add new temporary POS into hash
		posHash.put("X1", new PointOfSale("X1"));
		posHash.put("X2", new PointOfSale("X2"));

		// Get distances between POS
		routeList = new LinkedList<Distance>(distanceList);
		// initialize();
		// List<Distance> distanceList = (List<Distance>) (getDb4oManager()
		// .retrieve(new Distance(null, null, 0.0f, 0)));
		// routeList.addAll(distanceList);
		// commitAll();

		// for (Distance route : routeList) {
		// route.getStartPointOfSale().getDistanceList().put(
		// route.getEndPointOfSale(), route);
		// }

		for (Distance route : routeList) {
			if (posHash
					.containsKey(route.getStartPointOfSale().getIdentifier())) {
				posHash.get(route.getStartPointOfSale().getIdentifier())
						.getDistanceList()
						.put(route.getEndPointOfSale(), route);
			}
		}

		// Get Driver Tasks
		// initialize();
		// List<Task> driverTasks = (List<Task>) getDb4oManager().retrieve(
		// new Task(null, null, null, null, null, null, null, null, 0,
		// null, null, null));
		// commitAll();

		// Load Job List
		jobList = new LinkedList<Job>();
		for (Task task : driverTasks) {
			// if (!task.getCurrentStatus().equals(Status.DELIVERED)) {
			PointOfSale from = posHash.containsKey(task.getSourcePointOfSale()
					.getIdentifier()) == true ? posHash.get(task
					.getSourcePointOfSale().getIdentifier()) : wareHouse;
			PointOfSale to = posHash.containsKey(task
					.getDestinationPointOfSale().getIdentifier()) == true ? posHash
					.get(task.getDestinationPointOfSale().getIdentifier())
					: wareHouse;
			if (posHash.containsKey(from.getIdentifier())
					&& posHash.containsKey(to.getIdentifier())) {
				Job job1 = new Job(driver, task, from, null, TaskType.PICKUP);
				// job1.getTask().setPriority(0);
				job1.setStatus(Status.BLANK);

				Job job2 = new Job(driver, task, to, job1, TaskType.DROPOFF);
				// job2.getTask().setPriority(task.getPriority());
				job2.setAfterHour(to.getAfterHour());
				job2.setStatus(Status.BLANK);

				jobList.add(job1);
				jobList.add(job2);
			}
			// }
		}

		for (PointOfSale pos : posList) {
			if (!pos.isWareHouse()) {
				// Create task for each BILL pickup from POS
				Task task = new Task();
				task.setEnvelope(new Envelope("BILL_" + pos.getIdentifier()));
				task.setSourcePointOfSale(pos);
				task.setDestinationPointOfSale(wareHouse);
				task.setTaskType(TaskType.PICKUP);
				task.setPriority(0);
				task.setCurrentStatus(Status.BLANK);
				task.setCurrentLocation(pos.getIdentifier());
				task.setCurrentRecordedDate(new Date(currentTimestamp()
						.getTime()));

				Job job = new Job(driver, task, pos, null, TaskType.PICKUP);
				job.setPickBill(true);

				driverTasks.add(task);
				jobList.add(job);
			}
		}

		startPointOfSale = wareHouse;
	}

	public void calculateSchedule(Driver driver) {

		Calendar currentTime = Calendar.getInstance();

		scheduleList = new LinkedList<Schedule>();
		for (int i = 0; i < 20; i++) {

			// currentTime.set(Calendar.HOUR, 12);

			Schedule s = new Schedule(jobList, posList, posHash,
					startPointOfSale, currentTime);

			s.buildSchedule();
			s.setShortTimeWeight(i % 3 + 2);
			s.setShortDistanceWeight(i % 3 + 1);
			s.setBusinessWeight(i % 3 + 3);
			scheduleList.add(s);
		}

		scheduleTable = new Hashtable<String, Schedule>();

		for (Schedule schedule : scheduleList) {
			schedule.setShortTimeWeight(3);
			schedule.setShortDistanceWeight(1);
			schedule.setBusinessWeight(1);
		}

		Collections.sort(scheduleList, new ScheduleComparator());
		Collections.reverse(scheduleList);
		scheduleTable.put(SHORTTIME, scheduleList.get(0));

		for (Schedule schedule : scheduleList) {
			schedule.setShortTimeWeight(1);
			schedule.setShortDistanceWeight(3);
			schedule.setBusinessWeight(1);
		}

		Collections.sort(scheduleList, new ScheduleComparator());
		Collections.reverse(scheduleList);
		scheduleTable.put(SHORTDISTANCE, scheduleList.get(0));

		for (Schedule schedule : scheduleList) {
			schedule.setShortTimeWeight(1);
			schedule.setShortDistanceWeight(1);
			schedule.setBusinessWeight(3);
		}

		Collections.sort(scheduleList, new ScheduleComparator());
		Collections.reverse(scheduleList);
		scheduleTable.put(GOODBUSINESS, scheduleList.get(0));

		// Reset
		setScheduleSelected(false);
		setSelectedSchedule("");

	}

	/**
	 * Get Driver Tasks from BTNC and save into database.
	 * 
	 * @param driver
	 */
	public void getDriverTasksInformation(Driver driver, String filePath) {
		try {
			driverTasks = btncService.getDriverTask(driver, filePath);
			// initialize();
			// for (Task task : driverTasks) {
			// getDb4oManager().store(task);
			// }
			// commitAll();
		} catch (FileNotFoundException e) {
			Log.e("FileNotFoundException", e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			Log.e("IOException", e.getMessage());
			e.printStackTrace();
		} catch (ParseException e) {
			Log.e("ParseException", e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * Get Distance and Traffy Links between 2 Point of Sale and save into
	 * database.
	 */
	public void findDistanceAndTraffyLinksBetweenPos(String filePath) {
		try {
			distanceList = traffyService
					.getDistanceAndLinksBetweenPos(filePath);
			// initialize();
			// for (Distance distance : distanceList) {
			// getDb4oManager().store(distance);
			// }
			// commitAll();
		} catch (FileNotFoundException e) {
			Log.e("FileNotFoundException", e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			Log.e("IOException", e.getMessage());
			e.printStackTrace();
		}
	}

	public List<Task> getDriverTasks() {
		return driverTasks;
	}

	public void updateJobStatus(String planId, String posId, String envelopeId,
			String status) {

		Schedule s = scheduleTable.get(planId);
		for (Mission m : s.getMissionList()) {
			if (m.getPlace().getIdentifier().equals(posId)) {
				startPointOfSale = m.getPlace();
				for (Job j : m.getJobList()) {
					if (j.getTask().getEnvelope().getIdentifier().equals(
							envelopeId)) {
						j.setStatus(Status.valueOf(status));
						j
								.setStatusUpdated(new Date(System
										.currentTimeMillis()));
						if (j.getStatus().equals(Status.DONE)) {
							if (j.getAction().equals(TaskType.DROPOFF)) {
								updateDriverTasks(envelopeId, j
										.getStatusUpdated());
							}
							if (j.isPickBill()) {
								updateDriverTasks(envelopeId, j
										.getStatusUpdated());
							}
						}

					}
				}
			}
		}

	}

	private void updateDriverTasks(String envelopeId, Date statusUpdated) {
		for (Task task : driverTasks) {
			if (task.getEnvelope().getIdentifier().equals(envelopeId)) {
				task.setCurrentStatus(Status.DELIVERED);
				task.setCurrentRecordedDate(statusUpdated);
			}
		}

	}

}
