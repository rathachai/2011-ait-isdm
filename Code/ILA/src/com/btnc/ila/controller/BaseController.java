package com.btnc.ila.controller;

import java.sql.Timestamp;
import java.util.Calendar;

import com.btnc.ila.db.Db4oManager;

public abstract class BaseController {

	public Timestamp currentTimestamp() {
		return new Timestamp(Calendar.getInstance().getTime().getTime());
	}
	
	private Db4oManager db4oManager = Db4oManager.getInstance();

	public void initialize() {
		db4oManager.initialize();
	}
	
	public Db4oManager getDb4oManager() {
		return db4oManager;
	}

	public void commitAll() {
		db4oManager.finalize();
	}

	public void reset() {
		db4oManager.reset();
	}
}
