package com.btnc.ila.entity.traffic;

import java.util.List;

import com.btnc.ila.entity.task.PointOfSale;

public class Distance {

	private PointOfSale startPointOfSale;
	private PointOfSale endPointOfSale;
	private float distanceInKiloMeters = 0.0f;
	private int timeInMinutes = 0;
	private List<Link> traffyLinksInBetween;

	public Distance(PointOfSale startPointOfSale, PointOfSale endPointOfSale,
			float distanceInKiloMeters, int timeInMinutes,
			List<Link> traffyLinksInBetween) {
		super();
		this.startPointOfSale = startPointOfSale;
		this.endPointOfSale = endPointOfSale;
		this.distanceInKiloMeters = distanceInKiloMeters;
		this.timeInMinutes = timeInMinutes;
		this.traffyLinksInBetween = traffyLinksInBetween;
	}

	public Distance(PointOfSale startPointOfSale, PointOfSale endPointOfSale,
			float distanceInKiloMeters, int timeInMinutes) {
		super();
		this.startPointOfSale = startPointOfSale;
		this.endPointOfSale = endPointOfSale;
		this.distanceInKiloMeters = distanceInKiloMeters;
		this.timeInMinutes = timeInMinutes;
	}

	public PointOfSale getStartPointOfSale() {
		return startPointOfSale;
	}

	public void setStartPointOfSale(PointOfSale startPointOfSale) {
		this.startPointOfSale = startPointOfSale;
	}

	public PointOfSale getEndPointOfSale() {
		return endPointOfSale;
	}

	public void setEndPointOfSale(PointOfSale endPointOfSale) {
		this.endPointOfSale = endPointOfSale;
	}

	public float getDistanceInKiloMeters() {
		return distanceInKiloMeters;
	}

	public void setDistanceInKiloMeters(float distanceInKiloMeters) {
		this.distanceInKiloMeters = distanceInKiloMeters;
	}

	public int getTimeInMinutes() {
		return timeInMinutes;
	}

	public void setTimeInMinutes(int timeInMinutes) {
		this.timeInMinutes = timeInMinutes;
	}

	public List<Link> getTraffyLinksInBetween() {
		return traffyLinksInBetween;
	}

	public void setTraffyLinksInBetween(List<Link> traffyLinksInBetween) {
		this.traffyLinksInBetween = traffyLinksInBetween;
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(getStartPointOfSale());
		sb.append(":");
		sb.append(getEndPointOfSale());
		sb.append(":");
		sb.append(getDistanceInKiloMeters());
		sb.append(":");
		sb.append(getTimeInMinutes());
		return sb.toString();
	}

}
