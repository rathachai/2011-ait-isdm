package com.btnc.ila.entity.traffic;

import java.util.Date;

import com.btnc.ila.entity.common.LatLonPoint;

public class News {
	private String newsType;
	private String primarySource;
	private String secondarySource;
	private Date startTime;
	private Date endTime;
	private String title;
	private String description;
	private LatLonPoint location;

	public News() {
		super();
	}

	public String getNewsType() {
		return newsType;
	}

	public void setNewsType(String newsType) {
		this.newsType = newsType;
	}

	public String getPrimarySource() {
		return primarySource;
	}

	public void setPrimarySource(String primarySource) {
		this.primarySource = primarySource;
	}

	public String getSecondarySource() {
		return secondarySource;
	}

	public void setSecondarySource(String secondarySource) {
		this.secondarySource = secondarySource;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LatLonPoint getLocation() {
		return location;
	}

	public void setLocation(LatLonPoint location) {
		this.location = location;
	}

	@Override
	public String toString() {
		return "News [description=" + description + ", endTime=" + endTime
				+ ", location=" + location + ", newsType=" + newsType
				+ ", primarySource=" + primarySource + ", secondarySource="
				+ secondarySource + ", startTime=" + startTime + ", title="
				+ title + "]";
	}

}
