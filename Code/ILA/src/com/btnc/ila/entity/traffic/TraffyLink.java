package com.btnc.ila.entity.traffic;

import java.util.Date;

public class TraffyLink {

	private Link link;
	private String status;
	private Date lastUpdate;

	public TraffyLink() {
	}

	public TraffyLink(Link link, String status, Date lastUpdate) {
		super();
		this.link = link;
		this.status = status;
		this.lastUpdate = lastUpdate;
	}

	public Link getLink() {
		return link;
	}

	public void setLink(Link link) {
		this.link = link;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("Link:" + getLink().getIdentifier());
		sb.append(", ");
		sb.append("Date:" + getLastUpdate().toString());
		sb.append(", ");
		sb.append("Status:" + getStatus());
		sb.append(".");

		return sb.toString();

	}

}
