package com.btnc.ila.entity.traffic;

import com.btnc.ila.entity.common.LatLonPoint;

public class Link {

	private String identifier;
	private String roadName;
	private String startSectionName;
	private String endSectionName;
	private LatLonPoint startLocationPoint;
	private LatLonPoint endLocationPoint;
	private float lengthInMeters = 0.0f;

	private TraffyLink traffyLink;

	public Link() {

	}

	public Link(String identifier, String roadName, String startSectionName,
			String endSectionName, LatLonPoint startLocationPoint,
			LatLonPoint endLocationPoint, float lengthInMeters) {
		super();
		this.identifier = identifier;
		this.roadName = roadName;
		this.startSectionName = startSectionName;
		this.endSectionName = endSectionName;
		this.startLocationPoint = startLocationPoint;
		this.endLocationPoint = endLocationPoint;
		this.lengthInMeters = lengthInMeters;
	}

	public Link(String identifier) {
		this.identifier = identifier;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public String getRoadName() {
		return roadName;
	}

	public void setRoadName(String roadName) {
		this.roadName = roadName;
	}

	public String getStartSectionName() {
		return startSectionName;
	}

	public void setStartSectionName(String startSectionName) {
		this.startSectionName = startSectionName;
	}

	public String getEndSectionName() {
		return endSectionName;
	}

	public void setEndSectionName(String endSectionName) {
		this.endSectionName = endSectionName;
	}

	public LatLonPoint getStartLocationPoint() {
		return startLocationPoint;
	}

	public void setStartLocationPoint(LatLonPoint startLocationPoint) {
		this.startLocationPoint = startLocationPoint;
	}

	public LatLonPoint getEndLocationPoint() {
		return endLocationPoint;
	}

	public void setEndLocationPoint(LatLonPoint endLocationPoint) {
		this.endLocationPoint = endLocationPoint;
	}

	public void setLengthInMeters(float lengthInMeters) {
		this.lengthInMeters = lengthInMeters;
	}

	public float getLengthInMeters() {
		return lengthInMeters;
	}

	public TraffyLink getTraffyLink() {
		return traffyLink;
	}

	public void setTraffyLink(TraffyLink traffyLink) {
		this.traffyLink = traffyLink;
	}

}
