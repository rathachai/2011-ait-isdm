package com.btnc.ila.entity.schedule;

import java.util.Comparator;

public class ScheduleComparator implements Comparator<Schedule> {

	public int compare(Schedule schedule1, Schedule schedule2) {
		// int compare = 0;
		// int time = (int) schedule1.getAllTimeMin()
		// - (int) schedule2.getAllTimeMin();
		//
		// if (schedule1.getShortDistanceWeight() > 2) {
		// compare = (int) schedule1.getAllDistanceKm()
		// - (int) schedule2.getAllDistanceKm();
		// } else if (schedule1.getBusinessWeight() > 2) {
		// compare = (int) schedule1.getAllBusiness()
		// - (int) schedule2.getAllBusiness();
		// }
		//
		// if (compare == 0) {
		// compare = time;
		// }
		//
		// return compare;

		if(schedule1.getShortTimeWeight() == 3)
		return (schedule1.getAllTimeMin() > schedule2.getAllTimeMin() ? -1
				: (schedule1.getAllTimeMin() == schedule1.getAllTimeMin() ? 0
						: 1));
		else if(schedule1.getShortDistanceWeight() == 3)
			return (schedule1.getAllDistanceKm() > schedule2.getAllDistanceKm() ? -1
					: (schedule1.getAllDistanceKm() == schedule1.getAllDistanceKm() ? 0
							: 1));
		else if(schedule1.getBusinessWeight() == 3)
			return (schedule1.getAllBusiness() < schedule2.getAllBusiness() ? -1
					: (schedule1.getAllBusiness() == schedule1.getAllBusiness() ? 0
							: 1));
		else
			return 0;
	}
}
