package com.btnc.ila.entity.schedule;

import java.util.Calendar;
import java.util.Collections;
import java.util.Hashtable;
import java.util.LinkedList;

import com.btnc.ila.entity.common.Status;
import com.btnc.ila.entity.common.TaskType;
import com.btnc.ila.entity.task.Envelope;
import com.btnc.ila.entity.task.Job;
import com.btnc.ila.entity.task.Mission;
import com.btnc.ila.entity.task.PointOfSale;
import com.btnc.ila.entity.task.Task;
import com.btnc.ila.entity.traffic.Distance;

public class Schedule {

	private PointOfSale wareHouse;
	private PointOfSale startPointOfSale;
	private LinkedList<PointOfSale> pointOfSaleList = new LinkedList<PointOfSale>();
	private Hashtable<String, PointOfSale> pointOfSaleHash = new Hashtable<String, PointOfSale>();
	private LinkedList<PointOfSale> allPointOfSales = new LinkedList<PointOfSale>();
	private LinkedList<Job> depJobList = new LinkedList<Job>();
	private LinkedList<Job> indepJobList = new LinkedList<Job>();

	private Calendar startTime = Calendar.getInstance();

	private LinkedList<Mission> missionList = new LinkedList<Mission>();
	private int businessWeight = 1;
	private int shortTimeWeight = 1;
	private int shortDistanceWeight = 1;

	private double allDistanceKm = 0;
	private double allTimeMin = 0;
	private double allBusiness = 0;

	public Schedule() {
	}

	public Schedule(LinkedList<Job> jobs, LinkedList<PointOfSale> poses,
			Hashtable<String, PointOfSale> posdict, PointOfSale startPos,
			Calendar startTime) {
		this.allPointOfSales.addAll(poses);
		this.startPointOfSale = startPos;
		this.pointOfSaleHash = posdict;
		this.startTime.set(Calendar.HOUR, startTime.get(Calendar.HOUR));
		this.startTime.set(Calendar.MINUTE, startTime.get(Calendar.MINUTE));

		// Find WareHouse
		for (int i = 0; i < poses.size(); i++) {
			PointOfSale pos = poses.get(i);
			if (pos.isWareHouse()) {
				this.wareHouse = pos;
				break;
			}
		}

		this.pointOfSaleList.add(this.wareHouse);

		// Separate independence and dependence mission
		for (Job job : jobs) {

			// Ignore current POS job
			// if(job.getPlace()!=wareHouse && job.getPlace()==startPos &&
			// job.getStatus()==Status.WAIT){
			// continue;
			// }

			// Ignore cancel and done job
			if (job.getStatus().equals(Status.CANCEL)
					|| job.getStatus().equals(Status.DONE)) {
				continue;
			}
			if (job.getDependentJob() == null) {
				this.indepJobList.add(job);
			} else {

				// Check dependency job status that is Cancel or Done
				if (job.getDependentJob().getStatus().equals(Status.CANCEL)) {
					continue;
				}

				// If the pickup job is done, it will be independence job
				if (job.getDependentJob().getStatus().equals(Status.DONE)) {
					this.indepJobList.add(job);
				} else {
					this.depJobList.add(job);
				}
			}

			if (!this.pointOfSaleList.contains(job.getPlace())) {
				this.pointOfSaleList.add(job.getPlace());
			}
		}
	}

	public void buildSchedule() {
		try {

			gatherIndependentPos();
			sortIndependentPos();

			addDependence();

			cleanSchedule1();
			cleanSchedule2();
			cleanSchedule3();
			cleanSchedule2();

			calculateScore();

		} catch (Exception e) {

		}
	}

	private void gatherIndependentPos() {
		// Add first WH in mission
		boolean setStart = false;

		if (startPointOfSale.equals(wareHouse)) {
			Mission missionFromWH = new Mission();
			missionFromWH.setPlace(wareHouse);
			missionList.add(missionFromWH);

			for (int i = 0; i < indepJobList.size(); i++) {
				Job job = indepJobList.get(i);
				if (job.getPlace().isWareHouse()) {
					missionFromWH.getJobList().add(job);
				}
			}

			if (missionFromWH.getJobList().size() == 0) {
				Job job = new Job();
				job.setPlace(wareHouse);
				job.setTask(new Task());
				job.getTask().setEnvelope(new Envelope(""));
				job.getTask().setCurrentStatus(Status.BLANK);
				job.setAction(TaskType.START);				
				missionFromWH.getJobList().add(job);
			}

			setStart = true;
		}
		// Add all POS to be Mission

		for (int i = 0; i < pointOfSaleList.size(); i++) {
			if (!pointOfSaleList.get(i).isWareHouse()) {
				Mission m = new Mission();
				m.setPlace(pointOfSaleList.get(i));

				if (!setStart && m.getPlace() == startPointOfSale) {
					missionList.add(0, m);
					setStart = true;
				} else {
					missionList.add(m);
				}
			}
		}

		if (!setStart) {
			Mission m = new Mission();
			m.setPlace(startPointOfSale);
			missionList.add(0, m);
		}

		// Add job to Mission
		for (int j = 0; j < indepJobList.size(); j++) {
			Job job = indepJobList.get(j);
			if (!job.getPlace().isWareHouse()) {
				for (int m = 0; m < missionList.size(); m++) {
					Mission mission = missionList.get(m);
					if (job.getPlace().getIdentifier() == mission.getPlace()
							.getIdentifier()) {
						mission.getJobList().add(job);
					}
				}
			}
		}

		// Add last mission
		Mission missionToWH = new Mission();
		missionToWH.setPlace(wareHouse);
		missionList.add(missionToWH);

		for (int i = 0; i < depJobList.size(); i++) {
			Job job = depJobList.get(i);
			if (job.getPlace().isWareHouse()) {
				missionToWH.getJobList().add(job);
			}
		}

		if (missionToWH.getJobList().size() == 0) {
			Job job = new Job();
			job.setPlace(wareHouse);
			job.setTask(new Task());
			job.getTask().setEnvelope(new Envelope(""));
			job.getTask().setCurrentStatus(Status.BLANK);
			job.setAction(TaskType.END);
			missionToWH.getJobList().add(job);
		}

	}

	private void sortIndependentPos() {
		LinkedList<Mission> suff = new LinkedList<Mission>();

		suff.addAll(missionList);
		Mission first = suff.getFirst();
		Mission last = suff.getLast();

		double cost = 9999999;

		for (int i = 0; i < 40; i++) {
			LinkedList<Mission> s = new LinkedList<Mission>();
			s.addAll(suff);
			s.remove(last);
			s.remove(first);
			Collections.shuffle(s);

			/*
			 * //Add Mission p118 = null; Mission p309 = null;
			 * 
			 * Mission pS38 = null; Mission p302 = null;
			 * 
			 * for(int j=0; j<s.size(); j++){ Mission m = s.get(j);
			 * if(m.Place.ID == "118"){ p118 = m; }else if(m.Place.ID == "S38"){
			 * pS38 = m; }else if(m.Place.ID == "302"){ p302 = m; }else
			 * if(m.Place.ID == "309"){ p309 = m; } }
			 * 
			 * //Swap X1 if(p118!=null && p309!=null){ double b = Math.random();
			 * if(b>0.5){ Mission x = p118; p118=p309; p309=x; } s.remove(p118);
			 * for(int j=0; j<s.size(); j++){ Mission m = s.get(j); if(m==p309){
			 * s.add(j+1, p118); } } }
			 * 
			 * //Swap X2 if(p302!=null && pS38!=null){ double b = Math.random();
			 * if(b>0.5){ Mission x = p302; p302=pS38; pS38=x; } s.remove(pS38);
			 * for(int j=0; j<s.size(); j++){ Mission m = s.get(j); if(m==p302){
			 * s.add(j+1, pS38); } } }
			 */

			// add first / last
			s.addFirst(first);
			s.addLast(last);
			double c = getCost(s);
			if (c < cost) {
				cost = c;
				suff = s;
			}
		}

		// System.out.println("Total = " + cost);

		missionList = suff;
	}

	public double getCost() {
		return Math.round(getCost(missionList) * 100) / 100.0;
	}

	double getCost(LinkedList<Mission> missions) {
		double d = 0.0;

		for (int i = 0; i < missions.size() - 1; i++) {
			Mission e = missions.get(i);
			Mission f = missions.get(i + 1);

			LinkedList<Distance> r = getDistance(e.getPlace(), f.getPlace());

			double c = 0.0;
			if (r.size() > 0) {
				for (int j = 0; j < r.size(); j++) {
					c += getDistanceCost(r.get(j));
				}
			} else {
				c = 40.0;
			}
			// c*=r.size();
			// c = Math.scalb(c, 2);
			c = Math.round(c * 100) / 100.0;

			d += c;

			// System.out.print(e.Place.ID + "(" + c + ") , ");
		}

		// System.out.print(missions.getLast().Place.ID);

		// System.out.println("      =     " + d);

		return d;
	}

	double getDistanceCost(Distance r) {
		double cost = 0;

		if (this.shortDistanceWeight > this.shortTimeWeight) {
			cost = r.getDistanceInKiloMeters();
		} else {
			cost = r.getTimeInMinutes();
		}

		return cost;
	}

	LinkedList<Distance> getDistance(PointOfSale f, PointOfSale t) {

		PointOfSale from = pointOfSaleHash.get(f.getIdentifier());
		PointOfSale to = pointOfSaleHash.get(t.getIdentifier());

		if (from.getIdentifier().equals("309")
				|| from.getIdentifier().equals("118")) {
			from = pointOfSaleHash.get("X1");
		}
		if (from.getIdentifier().equals("302")
				|| from.getIdentifier().equals("S38")) {
			from = pointOfSaleHash.get("X2");
		}

		if (to.getIdentifier().equals("309")
				|| to.getIdentifier().equals("118")) {
			to = pointOfSaleHash.get("X1");
		}
		if (to.getIdentifier().equals("302")
				|| to.getIdentifier().equals("S38")) {
			to = pointOfSaleHash.get("X2");
		}

		LinkedList<Distance> r = new LinkedList<Distance>();
		if (from.getDistanceList().containsKey(to)) {
			r.add(from.getDistanceList().get(to));
		} else {
			double distance = 999999;

			for (PointOfSale p : from.getDistanceList().keySet()) {
				Distance r1 = from.getDistanceList().get(p);
				if (p.getDistanceList().containsKey(to)) {
					Distance r2 = p.getDistanceList().get(to);
					double d = getDistanceCost(r1) + getDistanceCost(r2);
					if (d < distance) {
						distance = d;
						r = new LinkedList<Distance>();
						r.add(r1);
						r.add(r2);
					}
				}
			}
		}
		return r;
	}

	public void calculateScore() {
		allDistanceKm = 0;
		allTimeMin = 0;
		allBusiness = 0;

		Calendar estimate = Calendar.getInstance();
		// estimate.set(Calendar.HOUR, startTime.get(Calendar.HOUR));
		// estimate.set(Calendar.MINUTE, startTime.get(Calendar.MINUTE));

		double bizNum = 0;
		double bizVal = 0;

		for (int i = 0; i < missionList.size() - 1; i++) {
			Mission e = missionList.get(i);
			Mission f = missionList.get(i + 1);

			LinkedList<Distance> r = getDistance(e.getPlace(), f.getPlace());

			if (r.size() > 0) {
				for (int j = 0; j < r.size(); j++) {
					Distance ro = r.get(j);
					allDistanceKm += ro.getDistanceInKiloMeters();
					allTimeMin += ro.getTimeInMinutes() + 20;
					estimate.add(Calendar.MINUTE,
							(int) ro.getTimeInMinutes() + 20);
				}
			} else {
				allDistanceKm += 40;
				allTimeMin += 60;
				estimate.add(Calendar.MINUTE, 60);
			}

			f.getEstimatedTime()
					.set(Calendar.HOUR, estimate.get(Calendar.HOUR));
			f.getEstimatedTime().set(Calendar.MINUTE,
					estimate.get(Calendar.MINUTE));

			if (f.getPriorty() > 0) {
				bizNum++;

				Calendar target = Calendar.getInstance();
				target.set(Calendar.HOUR, startTime.get(Calendar.HOUR));
				target.set(Calendar.MINUTE, startTime.get(Calendar.MINUTE));

				target.add(Calendar.HOUR, f.getPriorty());

				// System.out.println( estimate.get(Calendar.HOUR_OF_DAY) + ":"
				// +estimate.get(Calendar.MINUTE) + " <= " +
				// target.get(Calendar.HOUR_OF_DAY) + ":" +
				// target.get(Calendar.MINUTE));

				if (estimate.get(Calendar.HOUR_OF_DAY) <= target
						.get(Calendar.HOUR_OF_DAY)) {
					bizVal++;

				}
			}

		}

		allDistanceKm = Math.round(allDistanceKm * 100) / 100.00;
		allTimeMin = Math.round(allTimeMin * 100) / 100.00;

		if (bizNum > 0) {
			allBusiness = bizVal * 100.0 / bizNum;
		} else {
			allBusiness = 100;
		}
		allBusiness = Math.round(allBusiness * 100) / 100.00;
	}

	void addDependence() {
		LinkedList<Mission> depMissionList = new LinkedList<Mission>();
		// Add indepJob
		for (int j = 0; j < depJobList.size(); j++) {
			Job job = depJobList.get(j);

			if (!job.getPlace().isWareHouse()) {
				boolean isAdded = false;
				for (int m = 0; m < depMissionList.size(); m++) {
					Mission mission = depMissionList.get(m);
					if (job.getPlace() == mission.getPlace()
							&& job.getDependentJob().getPlace() == mission
									.getDependPos()) {
						mission.getJobList().add(job);
						isAdded = true;
						break;
					}
				}

				if (!isAdded) {
					Mission newMission = new Mission();
					newMission.setPlace(job.getPlace());
					newMission.getJobList().add(job);
					depMissionList.add(newMission);
				}
			}
		}

		// Try to add mission
		for (int i = 0; i < depMissionList.size(); i++) {
			Mission m = depMissionList.get(i);
			addMission(m);
		}

	}

	void addMission(Mission m) {
		// Find start index
		int startIndex = 0;

		for (int i = 0; i < missionList.size(); i++) {
			if (m.getDependPos() == missionList.get(i).getPlace()) {
				startIndex = i + 1;
				break;
			}
		}

		if (startIndex == 0) {

			startIndex = missionList.size() - 1;
		}

		// Loop to get the best position
		double cost = 99999;
		int suitableIndex = startIndex;
		for (int i = startIndex; i < missionList.size(); i++) {
			if (missionList.get(i).getPlace() == m.getPlace()) {
				suitableIndex = i;
				break;
			}
			double c = calculateCostFromAddedIndex(m, i);
			if (c < cost) {
				cost = c;
				suitableIndex = i;
			}
		}
		// System.out.println(startIndex+"<"+suitableIndex);
		// Add the best position

		missionList.add(suitableIndex, m);
	}

	double calculateCostFromAddedIndex(Mission m, int index) {
		LinkedList<Mission> missions = new LinkedList<Mission>();
		missions.addAll(missionList);
		missions.add(index, m);

		return getCost(missions);
	}

	void cleanSchedule1() {
		// Generate EmptyMission and JopedMission
		LinkedList<Mission> billJob = new LinkedList<Mission>();
		LinkedList<Mission> havingJob = new LinkedList<Mission>();

		for (int i = 1; i < missionList.size() - 1; i++) {
			Mission m = missionList.get(i);
			if (m.isOnlyBill()) {
				billJob.add(m);
			} else {
				havingJob.add(m);
			}
		}

		for (int e = 0; e < billJob.size(); e++) {
			Mission bill = billJob.get(e);
			for (int h = 0; h < havingJob.size(); h++) {
				Mission having = havingJob.get(h);
				if (bill != having && bill.getPlace() == having.getPlace()) {
					having.getJobList().add(bill.getJobList().get(0));
					missionList.remove(bill);
					break;
				}

			}
		}

	}

	void cleanSchedule2() {
		LinkedList<Mission> result = new LinkedList<Mission>();
		for (int i = 0; i < missionList.size() - 1; i++) {
			Mission m1 = missionList.get(i);
			Mission m2 = missionList.get(i + 1);

			if (m1.getPlace().equals(m2.getPlace())) {
				m1.getJobList().addAll(m2.getJobList());
				result.add(m1);
				i++;
			} else {
				result.add(m1);
			}
		}
		result.add(missionList.getLast());

		missionList = result;
	}

	void cleanSchedule3() {
		calculateScore();

		LinkedList<Mission> wrong = new LinkedList<Mission>();

		for (int i = 0; i < missionList.size(); i++) {
			Mission m = missionList.get(i);
			int targetHour = m.getAfterHour();
			int estHour = m.getEstimatedTime().get(Calendar.HOUR_OF_DAY);
			if (m.getAfterHour() > 0 && estHour < targetHour) {
				wrong.add(m);
				// System.out.println(estHour + "<" + targetHour);
			}
		}

		for (int i = 0; i < wrong.size(); i++) {
			missionList.remove(wrong.get(i));
		}

		for (int i = 0; i < wrong.size(); i++) {
			Mission m = wrong.get(i);
			addMission2(m);
		}
	}

	void addMission2(Mission m) {
		// Find start index
		int startIndex = 0;

		for (int i = 0; i < missionList.size(); i++) {
			int targetHour = m.getAfterHour();
			int estHour = m.getEstimatedTime().get(Calendar.HOUR_OF_DAY);
			if (estHour > targetHour) {
				startIndex = i + 1;
				break;
			}
		}

		if (startIndex == 0) {

			startIndex = missionList.size() - 1;
		}

		// Loop to get the best position
		double cost = 99999;
		int suitableIndex = startIndex;
		for (int i = startIndex; i < missionList.size(); i++) {
			if (missionList.get(i).getPlace().equals(m.getPlace())) {
				suitableIndex = i;
				break;
			}
			double c = calculateCostFromAddedIndex(m, i);
			if (c < cost) {
				cost = c;
				suitableIndex = i;
			}
		}
		// System.out.println(startIndex+"<"+suitableIndex);
		// Add the best position
		missionList.add(suitableIndex, m);
	}

	public PointOfSale getWareHouse() {
		return wareHouse;
	}

	public void setWareHouse(PointOfSale wareHouse) {
		this.wareHouse = wareHouse;
	}

	public PointOfSale getStartPointOfSale() {
		return startPointOfSale;
	}

	public void setStartPointOfSale(PointOfSale startPointOfSale) {
		this.startPointOfSale = startPointOfSale;
	}

	public LinkedList<PointOfSale> getPointOfSaleList() {
		return pointOfSaleList;
	}

	public void setPointOfSaleList(LinkedList<PointOfSale> pointOfSaleList) {
		this.pointOfSaleList = pointOfSaleList;
	}

	public Hashtable<String, PointOfSale> getPointOfSaleHash() {
		return pointOfSaleHash;
	}

	public void setPointOfSaleHashTable(
			Hashtable<String, PointOfSale> pointOfSaleHash) {
		this.pointOfSaleHash = pointOfSaleHash;
	}

	public LinkedList<PointOfSale> getAllPointOfSales() {
		return allPointOfSales;
	}

	public void setAllPointOfSales(LinkedList<PointOfSale> allPointOfSales) {
		this.allPointOfSales = allPointOfSales;
	}

	public LinkedList<Job> getDepJobList() {
		return depJobList;
	}

	public void setDepJobList(LinkedList<Job> depJobList) {
		this.depJobList = depJobList;
	}

	public LinkedList<Job> getIndepJobList() {
		return indepJobList;
	}

	public void setIndepJobList(LinkedList<Job> indepJobList) {
		this.indepJobList = indepJobList;
	}

	public Calendar getStartTime() {
		return startTime;
	}

	public void setStartTime(Calendar startTime) {
		this.startTime = startTime;
	}

	public LinkedList<Mission> getMissionList() {
		return missionList;
	}

	// public void setMissionList(LinkedList<Mission> missionList) {
	// this.missionList = missionList;
	// }

	public int getBusinessWeight() {
		return businessWeight;
	}

	public void setBusinessWeight(int businessWeight) {
		this.businessWeight = businessWeight;
	}

	public int getShortTimeWeight() {
		return shortTimeWeight;
	}

	public void setShortTimeWeight(int shortTimeWeight) {
		this.shortTimeWeight = shortTimeWeight;
	}

	public int getShortDistanceWeight() {
		return shortDistanceWeight;
	}

	public void setShortDistanceWeight(int shortDistanceWeight) {
		this.shortDistanceWeight = shortDistanceWeight;
	}

	public double getAllDistanceKm() {
		return allDistanceKm;
	}

	public void setAllDistanceKm(double allDistanceKm) {
		this.allDistanceKm = allDistanceKm;
	}

	public double getAllTimeMin() {
		return allTimeMin;
	}

	public void setAllTimeMin(double allTimeMin) {
		this.allTimeMin = allTimeMin;
	}

	public double getAllBusiness() {
		return allBusiness;
	}

	public void setAllBusiness(double allBusiness) {
		this.allBusiness = allBusiness;
	}

	// @Override
	// public String toString() {
	// StringBuffer sb = new StringBuffer();
	// sb.append(getAllDistanceKm());
	// sb.append(":");
	// sb.append(getAllTimeMin());
	// sb.append(":");
	// sb.append(getAllBusiness());
	// sb.append(":");
	// return sb.toString();
	// }

}
