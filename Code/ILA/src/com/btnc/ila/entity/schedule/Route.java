package com.btnc.ila.entity.schedule;

import java.util.List;

import com.btnc.ila.entity.task.Task;
import com.btnc.ila.entity.user.Driver;

public class Route {

	private String identifier;
	private Driver driver;

	private List<Task> tasks;

	public Route() {
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public Driver getDriver() {
		return driver;
	}

	public void setDriver(Driver driver) {
		this.driver = driver;
	}

	public List<Task> getTasks() {
		return tasks;
	}

	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}

}
