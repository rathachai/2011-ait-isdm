package com.btnc.ila.entity.user;

import java.sql.Timestamp;

public class Person {

	private String fullName;
	private String nickName;
	private String userName;
	private Role role;
	private String latestToken;
	private Timestamp latestLoginIn;

	public Person() {
	}

	public Person(String fullName, String nickName, String userName, Role role,
			String latestToken, Timestamp latestLoginIn) {
		super();
		this.fullName = fullName;
		this.nickName = nickName;
		this.userName = userName;
		this.role = role;
		this.latestToken = latestToken;
		this.latestLoginIn = latestLoginIn;
	}

	public Person(String latestToken, Timestamp latestLoginIn) {
		super();
		this.latestToken = latestToken;
		this.latestLoginIn = latestLoginIn;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public String getLatestToken() {
		return latestToken;
	}

	public void setLatestToken(String latestToken) {
		this.latestToken = latestToken;
	}

	public Timestamp getLatestLoginIn() {
		return latestLoginIn;
	}

	public void setLatestLoginIn(Timestamp latestLoginIn) {
		this.latestLoginIn = latestLoginIn;
	}

}
