package com.btnc.ila.entity.user;

public enum Role {

	DRIVER("DRIVER"),TOPMANAGER("TOP MANAGER"), LOGISTICSMANAGER("LOGISTICS MANAGER");
	
	private String roleName;
	
	private Role(String roleName){
		this.roleName = roleName;
	}
	
	public String getRole(){
		return roleName;
	}
}
