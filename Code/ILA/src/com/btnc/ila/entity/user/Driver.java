package com.btnc.ila.entity.user;

import java.sql.Timestamp;
import java.util.List;

import com.btnc.ila.entity.schedule.Route;
import com.btnc.ila.entity.task.PointOfSale;

public class Driver extends Person {

	private Route route;
	private List<PointOfSale> relatedPointOfSale;

	public Driver() {
	}

	public Driver(String fullName, String nickName, String userName,
			String latestToken, Timestamp latestLoginIn) {
		super(fullName, nickName, userName, Role.DRIVER, latestToken, latestLoginIn);
		// TODO Auto-generated constructor stub
	}

	public Driver(String latestToken, Timestamp latestLoginIn) {
		super(latestToken, latestLoginIn);
		// TODO Auto-generated constructor stub
	}

	public Route getRoute() {
		return route;
	}

	public void setRoute(Route route) {
		this.route = route;
	}

	public List<PointOfSale> getRelatedPointOfSale() {
		return relatedPointOfSale;
	}

	public void setRelatedPointOfSale(List<PointOfSale> relatedPointOfSale) {
		this.relatedPointOfSale = relatedPointOfSale;
	}

}
