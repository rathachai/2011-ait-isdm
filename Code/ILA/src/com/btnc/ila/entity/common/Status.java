package com.btnc.ila.entity.common;

public enum Status {

	BLANK(" "), UNKNOWN("UNKNOWN"), DONE("DONE"), WAIT("WAIT"), CANCEL("CANCEL"), READY(
			"READY"), ENROUTE("ENROUTE"), DELIVERED("DELIVERED");

	private String status;

	private Status(String status) {
		this.status = status;
	}

	public String getStatus() {
		return status;
	}
}
