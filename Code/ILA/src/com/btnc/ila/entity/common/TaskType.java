package com.btnc.ila.entity.common;

public enum TaskType {

	NEW("NEW"), TRANSFER("TRANSFER"), REFILL("REFILL"), PICKUP("PICK UP"), DROPOFF(
			"DROP OFF"), START("START"), END("END");

	private String type;

	private TaskType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}
}
