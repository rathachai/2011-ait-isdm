package com.btnc.ila.entity.common;

public enum Product {

	C("C & D"), F("L'OFFICIEL"), G("GUY LAROCHE"), J("JOUSSE"), L("LOUIS FERAUD"), 
	P("GSP Sport"), S("GSP Professional");

	private String productIdentifier;

	private Product(String productIdentifier) {
		this.productIdentifier = productIdentifier;
	}

	public String getProductIdentifier() {
		return productIdentifier;
	}
}
