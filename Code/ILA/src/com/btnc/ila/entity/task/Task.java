package com.btnc.ila.entity.task;

import java.util.Date;

import com.btnc.ila.entity.common.Product;
import com.btnc.ila.entity.common.Status;
import com.btnc.ila.entity.common.TaskType;

public class Task {

	private String identifier;
	private Envelope envelope;
	/*
	 * Point of Sale. "777" and "7BP" are in BTNC company, just name of
	 * warehouse within company. 7BP = new products, 777 = old products
	 */
	private PointOfSale sourcePointOfSale;
	private PointOfSale destinationPointOfSale;
	/*
	 * product brand name
	 */
	private Product product;

	// Transient
	private String transferOrderId;
	private Date createdDate;
	private TaskType taskType;
	/*
	 * merchandiser request but this is only one of many factors. 0 = no
	 * priority
	 */
	private int priority;

	/*
	 * Envelope status ("action" column) that your guys need to know are listed
	 * as follows: 1. READY: ready for pickup => products had been packaged by
	 * POS staff, waiting for driver in pickup. 2. ENROUTE: on the way with
	 * driver 3. DELIVERED: destination POS received envelopes. 4. UNKNOWN:
	 * unknown
	 */
	private Status currentStatus;
	/*
	 * Current status of Task Each Task associated with Envelope Envelope is
	 * with whom such as Driver (DR12) or still at POS (359)? 1. Driver
	 * Identifier 2. Point of Sale Identifier
	 */
	private String currentLocation;

	private Date currentRecordedDate;

	public Task() {
	}

	public Task(String identifier, Envelope envelope,
			PointOfSale sourcePointOfSale, PointOfSale destinationPointOfSale,
			Product product, String transferOrderId, Date createdDate,
			TaskType taskType, int priority, Status currentStatus,
			String currentLocation, Date currentRecordedDate) {
		super();
		this.identifier = identifier;
		this.envelope = envelope;
		this.sourcePointOfSale = sourcePointOfSale;
		this.destinationPointOfSale = destinationPointOfSale;
		this.product = product;
		this.transferOrderId = transferOrderId;
		this.createdDate = createdDate;
		this.taskType = taskType;
		this.priority = priority;
		this.currentStatus = currentStatus;
		this.currentLocation = currentLocation;
		this.currentRecordedDate = currentRecordedDate;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public Envelope getEnvelope() {
		return envelope;
	}

	public void setEnvelope(Envelope envelope) {
		this.envelope = envelope;
	}

	public PointOfSale getSourcePointOfSale() {
		return sourcePointOfSale;
	}

	public void setSourcePointOfSale(PointOfSale sourcePointOfSale) {
		this.sourcePointOfSale = sourcePointOfSale;
	}

	public PointOfSale getDestinationPointOfSale() {
		return destinationPointOfSale;
	}

	public void setDestinationPointOfSale(PointOfSale destinationPointOfSale) {
		this.destinationPointOfSale = destinationPointOfSale;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public String getTransferOrderId() {
		return transferOrderId;
	}

	public void setTransferOrderId(String transferOrderId) {
		this.transferOrderId = transferOrderId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public TaskType getTaskType() {
		return taskType;
	}

	public void setTaskType(TaskType taskType) {
		this.taskType = taskType;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public Status getCurrentStatus() {
		return currentStatus;
	}

	public void setCurrentStatus(Status currentStatus) {
		this.currentStatus = currentStatus;
	}

	public String getCurrentLocation() {
		return currentLocation;
	}

	public void setCurrentLocation(String currentLocation) {
		this.currentLocation = currentLocation;
	}

	public Date getCurrentRecordedDate() {
		return currentRecordedDate;
	}

	public void setCurrentRecordedDate(Date currentRecordedDate) {
		this.currentRecordedDate = currentRecordedDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((destinationPointOfSale == null) ? 0
						: destinationPointOfSale.hashCode());
		result = prime * result
				+ ((envelope == null) ? 0 : envelope.hashCode());
		result = prime
				* result
				+ ((sourcePointOfSale == null) ? 0 : sourcePointOfSale
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Task other = (Task) obj;
		if (destinationPointOfSale == null) {
			if (other.destinationPointOfSale != null)
				return false;
		} else if (!destinationPointOfSale.equals(other.destinationPointOfSale))
			return false;
		if (envelope == null) {
			if (other.envelope != null)
				return false;
		} else if (!envelope.equals(other.envelope))
			return false;
		if (sourcePointOfSale == null) {
			if (other.sourcePointOfSale != null)
				return false;
		} else if (!sourcePointOfSale.equals(other.sourcePointOfSale))
			return false;
		return true;
	}

	
//	@Override
//	public String toString() {
//		StringBuffer sb = new StringBuffer();
//		sb.append(getEnvelope());
//		sb.append(":");
//		sb.append(getSourcePointOfSale());
//		sb.append(":");
//		sb.append(getDestinationPointOfSale());
//		sb.append(":");
//		sb.append(getCurrentStatus());
//		sb.append(":");
//		return sb.toString();
//	}

}
