package com.btnc.ila.entity.task;

import java.util.Hashtable;

import com.btnc.ila.entity.common.LatLonPoint;
import com.btnc.ila.entity.traffic.Distance;

public class PointOfSale {

	private String identifier;
	private String fullName;
	private String shortName;
	private LatLonPoint locationPoint;

	private boolean isWareHouse;
	private int afterHour = 0;

	private Hashtable<PointOfSale, Distance> distances = new Hashtable<PointOfSale, Distance>();

	public PointOfSale() {
	}

	public PointOfSale(String identifier, String fullName, String shortName,
			double latitude, double longitude, boolean isWareHouse, int afterHour) {
		super();
		this.identifier = identifier;
		this.fullName = fullName;
		this.shortName = shortName;
		this.locationPoint = new LatLonPoint(latitude, longitude);
		this.isWareHouse = isWareHouse;
		this.afterHour = afterHour;
	}

	public PointOfSale(String identifier, String fullName, String shortName,
			double latitude, double longitude) {
		super();
		this.identifier = identifier;
		this.fullName = fullName;
		this.shortName = shortName;
		this.locationPoint = new LatLonPoint(latitude, longitude);
		this.isWareHouse = false;
		this.afterHour = 0;
	}

	public PointOfSale(String identifier) {
		this.identifier = identifier;
		this.isWareHouse = false;
		this.afterHour = 0;
	}

	public PointOfSale(String identifier, boolean isWareHouse, int afterHour) {
		super();
		this.identifier = identifier;
		this.isWareHouse = isWareHouse;
		this.afterHour = afterHour;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public LatLonPoint getLocationPoint() {
		return locationPoint;
	}

	public void setLocationPoint(LatLonPoint locationPoint) {
		this.locationPoint = locationPoint;
	}

	public boolean isWareHouse() {
		return isWareHouse;
	}

	public void setWareHouse(boolean isWareHouse) {
		this.isWareHouse = isWareHouse;
	}

	public int getAfterHour() {
		return afterHour;
	}

	public void setAfterHour(int afterHour) {
		this.afterHour = afterHour;
	}

	public void setDistances(Hashtable<PointOfSale, Distance> distances) {
		this.distances = distances;
	}

	public Hashtable<PointOfSale, Distance> getDistanceList() {
		return distances;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((identifier == null) ? 0 : identifier.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PointOfSale other = (PointOfSale) obj;
		if (identifier == null) {
			if (other.identifier != null)
				return false;
		} else if (!identifier.equals(other.identifier))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return getIdentifier();
	}

}
