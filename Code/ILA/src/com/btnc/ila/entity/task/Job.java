package com.btnc.ila.entity.task;

import java.util.Date;

import com.btnc.ila.entity.common.Status;
import com.btnc.ila.entity.common.TaskType;
import com.btnc.ila.entity.user.Driver;

public class Job {
	private Driver driver;
	private Task task;
	private PointOfSale place;
	private Job dependentJob;

	private TaskType action;
	private boolean isPickBill = false;
	private int afterHour = 0;

	private Status status = Status.BLANK;
	private Date statusUpdated;

	public Job(Driver driver, Task task, PointOfSale place, Job dependentJob,
			TaskType action) {
		super();
		this.driver = driver;
		this.task = task;
		this.place = place;
		this.dependentJob = dependentJob;
		this.action = action;
	}

	public Job() {
	}

	public Driver getDriver() {
		return driver;
	}

	public void setDriver(Driver driver) {
		this.driver = driver;
	}

	public Task getTask() {
		return task;
	}

	public void setTask(Task task) {
		this.task = task;
	}

	public PointOfSale getPlace() {
		return place;
	}

	public void setPlace(PointOfSale place) {
		this.place = place;
	}

	public Job getDependentJob() {
		return dependentJob;
	}

	public void setDependentJob(Job dependentJob) {
		this.dependentJob = dependentJob;
	}

	public TaskType getAction() {
		return action;
	}

	public void setAction(TaskType action) {
		this.action = action;
	}

	public boolean isPickBill() {
		return isPickBill;
	}

	public void setPickBill(boolean isPickBill) {
		this.isPickBill = isPickBill;
	}

	public int getAfterHour() {
		return afterHour;
	}

	public void setAfterHour(int afterHour) {
		this.afterHour = afterHour;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Date getStatusUpdated() {
		return statusUpdated;
	}

	public void setStatusUpdated(Date statusUpdated) {
		this.statusUpdated = statusUpdated;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(getTask());
		sb.append(":");
		sb.append(getAction());
		sb.append(":");
		sb.append(getPlace());
		sb.append(":");
		return sb.toString();
	}

}
