package com.btnc.ila.entity.task;

import java.util.Calendar;
import java.util.LinkedList;

import com.btnc.ila.entity.common.TaskType;

public class Mission {

	private PointOfSale place;
	private LinkedList<Job> jobs = new LinkedList<Job>();
	private Calendar estimatedTime = Calendar.getInstance();

	public Mission() {
	}

	public int getAfterHour() {
		int h = 0;

		for (int i = 0; i < this.jobs.size(); i++) {
			Job job = this.jobs.get(i);
			if (job.getAfterHour() > h) {
				h = job.getAfterHour();
			}
		}

		return h;
	}

	public boolean isOnlyBill() {
		boolean res = false;

		if (this.jobs.size() == 1) {
			res = this.jobs.get(0).isPickBill();
		}

		return res;
	}

	public PointOfSale getDependPos() {
		PointOfSale depend = null;

		if (this.jobs.size() > 0 && this.jobs.get(0) != null
				&& this.jobs.get(0).getDependentJob() != null
				&& this.jobs.get(0).getDependentJob().getPlace() != null) {
			depend = this.jobs.get(0).getDependentJob().getPlace();
		}

		return depend;
	}

	public Calendar getEstimatedTime() {
		return this.estimatedTime;
	}

	public LinkedList<Job> getJobList() {
		return this.jobs;
	}

	public PointOfSale getPlace() {
		return this.place;
	}

	public void setPlace(PointOfSale place) {
		this.place = place;
	}

	public boolean HasJobs() {
		return this.jobs.size() > 0 ? true : false;
	}

	public int getPriorty() {
		int priority = 99;
		
		for (int i = 0; i < this.jobs.size(); i++) {
			if(jobs.get(i).getAction()== TaskType.DROPOFF){
				int p = this.jobs.get(i).getTask().getPriority();
				if (p > 0 && p < priority) {
					priority = p;
				}
			}
		}

		return priority == 99 ? 0 : priority;
	}

}
