package com.btnc.ila.parser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.btnc.ila.entity.common.LatLonPoint;
import com.btnc.ila.entity.traffic.Link;

public class SAXLinkInfoParser extends DefaultHandler {

	List<Link> linkList;

	private String temp;

	private Link tempLink;

	public SAXLinkInfoParser() {
		linkList = new ArrayList<Link>();
	}

	public void run(InputSource inputSource) {
		parseDocument(inputSource);
		printData();
	}

	private void printData() {
		System.out.println("Traffy Links : " + linkList.size());

		Iterator<Link> itr = linkList.iterator();
		while (itr.hasNext()) {
			Link link = (Link) itr.next();
			System.out.println(link.toString());
		}

	}

	private void parseDocument(InputSource inputSource) {
		SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
		try {
			SAXParser saxParser = saxParserFactory.newSAXParser();
			saxParser.parse(inputSource, this);
		} catch (SAXException se) {
			se.printStackTrace();
		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (IOException ie) {
			ie.printStackTrace();
		}

	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		temp = "";
		if (qName.equalsIgnoreCase("linkinfo")) {
			// Create a new instance of TraffyLink
			tempLink = new Link();
			tempLink.setIdentifier(attributes.getValue("linkid"));
			tempLink.setLengthInMeters(Float.valueOf(attributes
					.getValue("length")));
		} else if (qName.equalsIgnoreCase("road")) {
			tempLink.setRoadName(attributes.getValue("name_e"));
		} else if (qName.equalsIgnoreCase("startnode")) {
			tempLink.setStartLocationPoint(new LatLonPoint(Double
					.valueOf(attributes.getValue("lat")), Double
					.valueOf(attributes.getValue("lng"))));
		} else if (qName.equalsIgnoreCase("endnode")) {
			tempLink.setEndLocationPoint(new LatLonPoint(Double
					.valueOf(attributes.getValue("lat")), Double
					.valueOf(attributes.getValue("lng"))));
		}
	}

	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		temp = new String(ch, start, length);
	}

	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		if (qName.equalsIgnoreCase("linkinfo")) {
			// Add to List
			linkList.add(tempLink);
		}
	}
}
