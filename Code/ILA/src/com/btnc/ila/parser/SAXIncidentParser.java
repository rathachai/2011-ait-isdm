package com.btnc.ila.parser;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.btnc.ila.entity.common.LatLonPoint;
import com.btnc.ila.entity.traffic.News;

public class SAXIncidentParser extends DefaultHandler {

	List<News> newsList;

	private String temp;

	private News tempNews;

	private SimpleDateFormat format = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");

	public SAXIncidentParser() {
		newsList = new ArrayList<News>();
	}

	public void run(InputSource inputSource) {
		parseDocument(inputSource);
		printData();
	}

	private void printData() {
		System.out.println("Traffy Links : " + newsList.size());

		Iterator<News> itr = newsList.iterator();
		while (itr.hasNext()) {
			News news = (News) itr.next();
			System.out.println(news.toString());
		}

	}

	private void parseDocument(InputSource inputSource) {
		SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
		try {
			SAXParser saxParser = saxParserFactory.newSAXParser();
			saxParser.parse(inputSource, this);
		} catch (SAXException se) {
			se.printStackTrace();
		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (IOException ie) {
			ie.printStackTrace();
		}

	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		temp = "";
		if (qName.equalsIgnoreCase("news")) {
			// Create a new instance of News
			tempNews = new News();
			tempNews.setNewsType(attributes.getValue("type"));
			tempNews.setPrimarySource(attributes.getValue("primarysource"));
			tempNews.setSecondarySource(attributes.getValue("secondarysource"));
			try {
				tempNews.setStartTime(format.parse(attributes.getValue(
						"starttime").substring(0, 18)));
				tempNews.setEndTime(format.parse(attributes.getValue("endtime")
						.substring(0, 18)));
			} catch (ParseException e) {
				// TODO: handle exception
			}
		} else if (qName.equalsIgnoreCase("point")) {
			tempNews.setLocation(new LatLonPoint(Double.valueOf(attributes
					.getValue("latitude")), Double.valueOf(attributes
					.getValue("longitude"))));
			tempNews.getLocation().setName(attributes.getValue("name"));
		}
	}

	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		temp = new String(ch, start, length);
	}

	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		if (qName.equalsIgnoreCase("news")) {
			// Add to List
			newsList.add(tempNews);
		} else if (qName.equalsIgnoreCase("title")) {
			tempNews.setTitle(temp);
		} else if (qName.equalsIgnoreCase("description")) {
			tempNews.setDescription(temp);
		}
	}
}
