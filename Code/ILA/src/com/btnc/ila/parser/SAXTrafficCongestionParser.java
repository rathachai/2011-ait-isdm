package com.btnc.ila.parser;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.btnc.ila.entity.traffic.Link;
import com.btnc.ila.entity.traffic.TraffyLink;

public class SAXTrafficCongestionParser extends DefaultHandler {

	List<TraffyLink> traffyLinkList;

	private String tempValue;

	private TraffyLink tempTraffyLink;

	private SimpleDateFormat format = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");

	public SAXTrafficCongestionParser() {
		traffyLinkList = new ArrayList<TraffyLink>();
	}

	public List<TraffyLink> run(InputSource inputSource) {
		parseDocument(inputSource);
		// printData();
		return traffyLinkList;
	}

	private void printData() {
		System.out.println("Traffy Links : " + traffyLinkList.size());

		Iterator<TraffyLink> itr = traffyLinkList.iterator();
		while (itr.hasNext()) {
			TraffyLink traffyLink = (TraffyLink) itr.next();
			System.out.println(traffyLink.toString());
		}

	}

	private void parseDocument(InputSource inputSource) {
		SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
		try {
			SAXParser saxParser = saxParserFactory.newSAXParser();
			saxParser.parse(inputSource, this);
		} catch (SAXException se) {
			se.printStackTrace();
		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (IOException ie) {
			ie.printStackTrace();
		}

	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		tempValue = "";
		if (qName.equalsIgnoreCase("trafficmessage")) {
			// Create a new instance of TraffyLink
			tempTraffyLink = new TraffyLink();
		}
	}

	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		tempValue = new String(ch, start, length);
	}

	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		if (qName.equalsIgnoreCase("trafficmessage")) {
			// Add to List
			traffyLinkList.add(tempTraffyLink);
		} else if (qName.equalsIgnoreCase("linkid")) {
			tempTraffyLink.setLink(new Link(tempValue));
		} else if (qName.equalsIgnoreCase("datetime")) {
			try {
				tempTraffyLink.setLastUpdate(format.parse(tempValue));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		} else if (qName.equalsIgnoreCase("status")) {
			tempTraffyLink.setStatus(tempValue);
		}
	}
}
