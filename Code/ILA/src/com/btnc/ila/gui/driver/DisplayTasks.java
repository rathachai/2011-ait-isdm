package com.btnc.ila.gui.driver;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.btnc.ila.R;
import com.btnc.ila.controller.logistic.ScheduleViewingController;
import com.btnc.ila.entity.common.Status;

public class DisplayTasks extends ListActivity {

	String currentUser;
	String selectedPlanId;
	String selectedPosId;
	int location;

	TaskAdapter taskAdapter;
	ScheduleViewingController scheduleViewingController;

	private final int MENU_DONE_ALL = 1, MENU_WAIT_ALL = 2,
			MENU_CANCEL_ALL = 3;
	private final int GROUP_DONE_ALL = 1, GROUP_WAIT_ALL = 2,
			GROUP_CANCEL_ALL = 3;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setContentView(R.layout.task_action);
		super.onCreate(savedInstanceState);

		Bundle extras = getIntent().getExtras();
		currentUser = (String) extras.getString("USERNAME").trim();
		selectedPosId = (String) extras.getString("POS").trim();
		selectedPlanId = (String) extras.getString("PLAN").trim();
		location = extras.getInt("LOCATION");

		TextView siteMap = (TextView) findViewById(R.id.tasitemap);
		siteMap.setText("Home->Schedule->" + selectedPlanId + "->"
				+ selectedPosId);

		scheduleViewingController = ScheduleViewingController.getInstance();
		taskAdapter = new TaskAdapter(this);
		this.setListAdapter(taskAdapter);
	}

	@Override
	protected void onListItemClick(ListView l, View v, final int position,
			long id) {
		super.onListItemClick(l, v, position, id);
		if (scheduleViewingController.isScheduleSelected()) {
			final CharSequence[] statues = { Status.DONE.getStatus(),
					Status.WAIT.getStatus(), Status.CANCEL.getStatus() };

			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("Status");
			builder.setSingleChoiceItems(statues, -1,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int item) {
							updateStatus(selectedPlanId, selectedPosId,
									taskAdapter.getItem(position).envelope,
									statues[item].toString());
							dialog.cancel();
						}
					});
			AlertDialog alert = builder.create();
			alert.show();
		}
	}

	private void updateStatus(String planId, String posId, String envelopeId,
			String status) {
		scheduleViewingController.updateJobStatus(planId, posId, envelopeId,
				status);
		taskAdapter = new TaskAdapter(this);
		this.setListAdapter(taskAdapter);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (scheduleViewingController.isScheduleSelected()) {
			menu.add(GROUP_DONE_ALL, MENU_DONE_ALL, 0, "Done All").setIcon(
					R.drawable.done);
			menu.add(GROUP_WAIT_ALL, MENU_WAIT_ALL, 1, "Wait All").setIcon(
					R.drawable.wait);
			menu.add(GROUP_CANCEL_ALL, MENU_CANCEL_ALL, 2, "Cancel All")
					.setIcon(R.drawable.cancel);
		}
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Confirm");
		builder.setMessage("Are you sure?").setCancelable(false)
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								doOnOptionsItemSelected(item);
							}
						}).setNegativeButton("No",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});
		AlertDialog alert = builder.create();
		alert.show();
		return super.onOptionsItemSelected(item);
	}

	private void doOnOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case MENU_DONE_ALL:
			for (int position = 0; position < taskAdapter.getCount(); position++) {
				scheduleViewingController.updateJobStatus(selectedPlanId,
						selectedPosId, taskAdapter.getItem(position).envelope,
						Status.DONE.getStatus());
			}
			break;

		case MENU_WAIT_ALL:
			for (int position = 0; position < taskAdapter.getCount(); position++) {
				scheduleViewingController.updateJobStatus(selectedPlanId,
						selectedPosId, taskAdapter.getItem(position).envelope,
						Status.WAIT.getStatus());
			}
			break;
		case MENU_CANCEL_ALL:
			for (int position = 0; position < taskAdapter.getCount(); position++) {
				scheduleViewingController.updateJobStatus(selectedPlanId,
						selectedPosId, taskAdapter.getItem(position).envelope,
						Status.CANCEL.getStatus());
			}
			break;
		}
		taskAdapter = new TaskAdapter(this);
		this.setListAdapter(taskAdapter);
	}

	@Override
	protected void onResume() {
		super.onResume();
		taskAdapter = new TaskAdapter(this);
		this.setListAdapter(taskAdapter);
	}

	private class TaskAction {
		public String envelope;
		public String action;
		public String state;
		public String stateIcon;

		public TaskAction(String envelope, String action, String state,
				String stateIcon) {
			this.envelope = envelope;
			this.action = action;
			this.state = state;
			this.stateIcon = stateIcon;
		}
	}

	private class TaskAdapter extends BaseAdapter {
		private LayoutInflater layoutInflater;
		private ArrayList<TaskAction> taskActionList;

		public TaskAdapter(Context context) {
			layoutInflater = LayoutInflater.from(context);
			taskActionList = new ArrayList<TaskAction>();
			getData();
		}

		public void getData() {
			List<Map<String, String>> posActionList = scheduleViewingController
					.getPosTasks(currentUser, selectedPlanId, selectedPosId,
							location);
			for (Map<String, String> posActionMap : posActionList) {
				TaskAction temp = new TaskAction(posActionMap.get("ENVELOPE"),
						posActionMap.get("ACTION"), posActionMap.get("STATE"),
						posActionMap.get("ICON"));
				taskActionList.add(temp);
			}
		}

		public int getCount() {
			return taskActionList.size();
		}

		public TaskAction getItem(int position) {
			return taskActionList.get(position);
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			final ViewHolder holder;
			View v = convertView;
			if ((v == null) || (v.getTag() == null)) {
				v = layoutInflater.inflate(R.layout.task_action_row, null);
				holder = new ViewHolder();
				holder.taCount = (TextView) v.findViewById(R.id.tacount);
				holder.taEnvelope = (TextView) v.findViewById(R.id.taenvelope);
				holder.taAction = (TextView) v.findViewById(R.id.taaction);
				holder.taState = (TextView) v.findViewById(R.id.tastate);
				holder.taStateIcon = (ImageView) v
						.findViewById(R.id.tastateicon);
				v.setTag(holder);
			} else {
				holder = (ViewHolder) v.getTag();
			}
			holder.taskAction = getItem(position);
			holder.taCount.setText(String.valueOf(position + 1));
			holder.taEnvelope.setText("Envelope : "
					+ holder.taskAction.envelope);
			holder.taAction.setText("Action : " + holder.taskAction.action);
			holder.taState.setText("Status :" + holder.taskAction.state);
			if (holder.taskAction.stateIcon != null) {
				holder.taStateIcon.setImageResource(Integer
						.valueOf(holder.taskAction.stateIcon));
			}
			v.setTag(holder);
			return v;
		}

		public class ViewHolder {
			TaskAction taskAction;
			TextView taCount;
			TextView taEnvelope;
			TextView taAction;
			TextView taState;
			ImageView taStateIcon;
		}

	}

}
