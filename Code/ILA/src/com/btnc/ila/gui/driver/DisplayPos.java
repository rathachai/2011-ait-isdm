package com.btnc.ila.gui.driver;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.btnc.ila.R;
import com.btnc.ila.controller.logistic.ScheduleViewingController;

public class DisplayPos extends ListActivity {

	String currentUser;
	String selectedPlanId;

	PosAdaptar posAdaptar;
	ScheduleViewingController scheduleViewingController;

	private final int MENU_SELECT = 1, MENU_CANCEL = 2;
	private final int GROUP_SELECT = 1, GROUP_CANCEL = 2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setContentView(R.layout.pos_path);
		super.onCreate(savedInstanceState);

		Bundle extras = getIntent().getExtras();
		currentUser = (String) extras.getString("USERNAME").trim();
		selectedPlanId = (String) extras.getString("PLAN").trim();

		TextView siteMap = (TextView) findViewById(R.id.ppsitemap);
		siteMap.setText("Home->Schedule->" + selectedPlanId);

		scheduleViewingController = ScheduleViewingController.getInstance();
		posAdaptar = new PosAdaptar(this);
		this.setListAdapter(posAdaptar);
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		Intent intent = new Intent(getApplicationContext(), DisplayTasks.class);

		Bundle bundle = new Bundle();
		bundle.putString("USERNAME", currentUser);
		// bundle.putString("TOKEN", "");
		bundle.putInt("LOCATION", position);
		bundle.putString("POS", posAdaptar.getItem(position).posId);
		bundle.putString("PLAN", selectedPlanId);
		intent.putExtras(bundle);

		startActivity(intent);
	}

	@Override
	protected void onResume() {
		super.onResume();
		posAdaptar = new PosAdaptar(this);
		this.setListAdapter(posAdaptar);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(GROUP_SELECT, MENU_SELECT, 0, "Select").setIcon(
				R.drawable.select);
		menu.add(GROUP_CANCEL, MENU_CANCEL, 1, "Cancel").setIcon(
				R.drawable.cancel);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case MENU_SELECT:
			scheduleViewingController.setSelectedSchedule(selectedPlanId);
			scheduleViewingController.setScheduleSelected(true);
			return true;
		case MENU_CANCEL:
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private class PosDetail {
		public String posId;
		public String posName;
		public String posStatusIcon;
		public String time;
		public String priority;
		public String afterhour;

		public PosDetail(String posId, String posName, String posStatusIcon, String time, String priority, String afterhour) {
			this.posId = posId;
			this.posName = posName;
			this.posStatusIcon = posStatusIcon;
			this.time = time;
			if(priority.equals("0")){
				this.priority = "";
			}else{
				this.priority = "Priority : " + priority;
			}
			
			if(afterhour.equals("0")){
				this.afterhour = "";
			}else{
				this.afterhour = "After : " + afterhour + ":00";
			}
			
		}
	}

	private class PosAdaptar extends BaseAdapter {
		private LayoutInflater layoutInflater;
		private ArrayList<PosDetail> posDetailList;

		public PosAdaptar(Context context) {
			layoutInflater = LayoutInflater.from(context);
			posDetailList = new ArrayList<PosDetail>();
			getData();
		}

		public void getData() {
			List<Map<String, String>> posPlanDetailList = scheduleViewingController
					.getPosPlanDetail(currentUser, selectedPlanId);
			for (Map<String, String> posPlanDetailMap : posPlanDetailList) {
				PosDetail temp = new PosDetail(
								posPlanDetailMap.get("POS"),
								posPlanDetailMap.get("POSNAME"), 
								posPlanDetailMap.get("ICON"), 
								posPlanDetailMap.get("TIME"),
								posPlanDetailMap.get("PRIORITY"), 
								posPlanDetailMap.get("AFTERHOUR")
								);
				
				posDetailList.add(temp);
			}
			Collections.reverse(posDetailList);
		}

		public int getCount() {
			return posDetailList.size();
		}

		public PosDetail getItem(int position) {
			return posDetailList.get(position);
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			final ViewHolder holder;
			View v = convertView;
			if ((v == null) || (v.getTag() == null)) {
				v = layoutInflater.inflate(R.layout.pos_path_row, null);
				holder = new ViewHolder();
				holder.pdCheck = (ImageView) v.findViewById(R.id.pdcheck);
				holder.pdCount = (TextView) v.findViewById(R.id.pdcount);
				holder.pdPos = (TextView) v.findViewById(R.id.pdpos);
				holder.pdPosTime =  (TextView) v.findViewById(R.id.pdpostime);
				holder.pdPosName = (TextView) v.findViewById(R.id.pdposname);
				holder.pdPosPriority  = (TextView) v.findViewById(R.id.pdpospriority);
				holder.pdPosAfterHour  = (TextView) v.findViewById(R.id.pdposafterhour);
				v.setTag(holder);
			} else {
				holder = (ViewHolder) v.getTag();
			}
			holder.posDetail = getItem(position);
			holder.pdCount.setText(String.valueOf(position + 1));
			holder.pdPos.setText("POS : " + holder.posDetail.posId);
			holder.pdPosTime.setText(holder.posDetail.time);
			holder.pdPosName.setText(holder.posDetail.posName);
			holder.pdPosPriority.setText(holder.posDetail.priority);
			holder.pdPosAfterHour.setText(holder.posDetail.afterhour);
			if (holder.posDetail.posStatusIcon != null) {
				holder.pdCheck.setImageResource(Integer
						.valueOf(holder.posDetail.posStatusIcon));
			}
			v.setTag(holder);
			return v;
		}

		public class ViewHolder {
			PosDetail posDetail;
			TextView pdCount;
			TextView pdPos;
			TextView pdPosTime;
			TextView pdPosName;
			TextView pdPosPriority;
			TextView pdPosAfterHour;
			ImageView pdCheck;
		}

	}
}
