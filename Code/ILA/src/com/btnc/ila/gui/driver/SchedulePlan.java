package com.btnc.ila.gui.driver;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.btnc.ila.R;
import com.btnc.ila.controller.logistic.ScheduleViewingController;
import com.btnc.ila.entity.user.Driver;
import com.btnc.ila.gui.common.MockUp;

public class SchedulePlan extends ListActivity {

	Driver currentUser;

	ScheduleViewingController scheduleViewingController;
	PlanAdaptar planAdaptar;

	private final int MENU_SELECT = 1, MENU_CANCEL = 2;
	private final int GROUP_SELECT = 1, GROUP_CANCEL = 2;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.schedule_plan);

		Bundle extras = getIntent().getExtras();

		scheduleViewingController = ScheduleViewingController.getInstance();

		if (!scheduleViewingController.isScheduleSelected()) {
			currentUser = scheduleViewingController
					.getDriverFromUserName(extras.getString("USERNAME").trim());

			MockUp mockUp = new MockUp(getApplicationContext());

			scheduleViewingController.getDriverTasksInformation(currentUser,
					mockUp.loadDriverTasks(currentUser));

			scheduleViewingController
					.findDistanceAndTraffyLinksBetweenPos(mockUp
							.loadLinksBetweenPos());
			scheduleViewingController.calculateJobList(currentUser);
			scheduleViewingController.calculateSchedule(currentUser);
		}
		planAdaptar = new PlanAdaptar(this);

		this.setListAdapter(planAdaptar);

		registerForContextMenu(getListView());

	}

	@Override
	protected void onResume() {
		if (scheduleViewingController.isScheduleSelected()) {
			planAdaptar = new PlanAdaptar(this);
			this.setListAdapter(planAdaptar);
		}
		super.onResume();
	}

	public void onListItemClick(ListView parent, View v, int position, long id) {
		Intent intent = new Intent(getApplicationContext(), DisplayPos.class);

		Bundle bundle = new Bundle();
		bundle.putString("USERNAME", currentUser.getUserName());
		bundle.putString("PLAN", planAdaptar.getItem(position).planId);
		intent.putExtras(bundle);

		startActivity(intent);
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		if (v.getId() == android.R.id.list) {
			AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
			menu.setHeaderTitle(getResources().getStringArray(
					R.array.plan_criteria)[info.position]);
			menu.add(GROUP_SELECT, MENU_SELECT, 0, "Select");
			menu.add(GROUP_CANCEL, MENU_CANCEL, 1, "Cancel");
		}
		super.onCreateContextMenu(menu, v, menuInfo);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item
				.getMenuInfo();
		String selectedPlanId = planAdaptar.getItem(info.position).planId;
		switch (item.getItemId()) {
		case MENU_SELECT:
			scheduleViewingController.setSelectedSchedule(selectedPlanId);
			scheduleViewingController.setScheduleSelected(true);
			planAdaptar = new PlanAdaptar(this);
			this.setListAdapter(planAdaptar);
			return true;
		case MENU_CANCEL:
			return true;
		}
		return super.onContextItemSelected(item);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 0, 0, "Synchronize").setIcon(R.drawable.sync);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case 0:
			scheduleViewingController.setSelectedSchedule("");
			scheduleViewingController.setScheduleSelected(false);
			scheduleViewingController.calculateSchedule(currentUser);
			planAdaptar = new PlanAdaptar(this);
			this.setListAdapter(planAdaptar);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private class PlanBrief {
		public String planId;
		public String distance;
		public String time;
		public String planFlow;
		public String planStatusIcon;

		public PlanBrief(String planId, String distance, String time,
				String planFlow, String planStatusIcon) {
			this.planId = planId;
			this.distance = distance;
			this.time = time;
			this.planFlow = planFlow;
			this.planStatusIcon = planStatusIcon;
		}
	}

	private class PlanAdaptar extends BaseAdapter {

		private LayoutInflater layoutInflater;
		private ArrayList<PlanBrief> planBriefList;

		public PlanAdaptar(Context context) {
			layoutInflater = LayoutInflater.from(context);
			planBriefList = new ArrayList<PlanBrief>();
			getData();
		}

		public void getData() {
			List<Map<String, String>> planList = scheduleViewingController
					.getSchedule(currentUser);
			for (Map<String, String> planMap : planList) {
				PlanBrief temp = new PlanBrief(planMap.get("ID"), planMap
						.get("DISTANCE"), planMap.get("TIME"), planMap
						.get("FLOW"), planMap.get("ICON"));
				planBriefList.add(temp);
			}
			Collections.reverse(planBriefList);
		}

		public int getCount() {
			return planBriefList.size();
		}

		public PlanBrief getItem(int position) {
			return planBriefList.get(position);
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			final ViewHolder holder;
			View v = convertView;
			if ((v == null) || (v.getTag() == null)) {
				v = layoutInflater.inflate(R.layout.schedule_plan_row, null);
				holder = new ViewHolder();
				holder.pbPlanCriteria = (TextView) v
						.findViewById(R.id.spcriteria);
				holder.pbPlanId = (TextView) v.findViewById(R.id.spplanid);
				holder.pbDistance = (TextView) v.findViewById(R.id.spdistance);
				holder.pbTime = (TextView) v.findViewById(R.id.sptime);
				holder.pbFlow = (TextView) v.findViewById(R.id.spflow);
				holder.pbCheck = (ImageView) v.findViewById(R.id.spcheck);
				v.setTag(holder);
			} else {
				holder = (ViewHolder) v.getTag();
			}
			holder.planBrief = getItem(position);
			holder.pbPlanId.setText(String.valueOf(position + 1));
			holder.pbPlanCriteria.setText(scheduleViewingController
					.getScheduleNameDetail().get(holder.planBrief.planId));
			holder.pbDistance.setText(holder.planBrief.distance
					+ " km");
			holder.pbTime.setText(holder.planBrief.time);
			holder.pbFlow.setText(holder.planBrief.planFlow);
			if (holder.planBrief.planStatusIcon != null) {
				holder.pbCheck.setImageResource(Integer
						.valueOf(holder.planBrief.planStatusIcon));
			}
			v.setTag(holder);
			return v;
		}

		public class ViewHolder {
			PlanBrief planBrief;
			TextView pbPlanCriteria;
			TextView pbPlanId;
			TextView pbDistance;
			TextView pbTime;
			TextView pbFlow;
			ImageView pbCheck;
		}

	}

}
