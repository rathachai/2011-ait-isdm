package com.btnc.ila.gui.driver;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.btnc.ila.R;
//import com.btnc.ila.gui.common.PosMapView;

public class Home extends Activity {
	// IlaDb ilaDb;

	String currentUser;
	String currentUserFullName;
	String currentToken;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.driver_home);

		// ilaDb = new IlaDb(this);
		// ilaDb.open();

		Bundle extras = getIntent().getExtras();
		currentUser = (String) extras.getString("USERNAME").trim();
		currentUserFullName = (String) extras.getString("FULLNAME").trim();
		currentToken = (String) extras.getString("TOKEN").trim();

		// MockUp mockUp = new MockUp(getApplicationContext());
		// mockUp.loadLinksBetweenPos();

		Toast.makeText(Home.this, "Welcome, " + currentUserFullName,
				Toast.LENGTH_LONG).show();

		Button btnSchedule = (Button) findViewById(R.id.schedule);
		Button btnSubmitStatus = (Button) findViewById(R.id.submit_status);
		Button btnMapView = (Button) findViewById(R.id.map);

		btnSchedule.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(),
						SchedulePlan.class);
				Bundle bundle = new Bundle();
				bundle.putString("USERNAME", currentUser);
				bundle.putString("TOKEN", currentToken);

				intent.putExtras(bundle);
				startActivity(intent);

			}
		});

		btnSubmitStatus.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(),
						DisplayTaskStatus.class);
				Bundle bundle = new Bundle();
				bundle.putString("USERNAME", currentUser);
				bundle.putString("TOKEN", currentToken);

				intent.putExtras(bundle);
				startActivity(intent);

			}
		});

		btnMapView.setOnClickListener(new OnClickListener() {

			public void onClick(View paramView) {
				//Intent intent = new Intent(getApplicationContext(),
				//		PosMapView.class);
				//startActivity(intent);

			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		// if (ilaDb == null) {
		// ilaDb = new IlaDb(this);
		// ilaDb.open();
		// } else {
		// ilaDb.open();
		// }
	}

	// private Person getCurrentUser(String username) {
	// Person person = null;
	// Cursor cursor = ilaDb.query(IlaConstants.PERSON_TABLE, null,
	// IlaConstants.PERSON_USER_NAME + "= ?",
	// new String[] { username }, null, null, null, null);
	// startManagingCursor(cursor);
	// if (cursor.moveToFirst()) {
	// person = new Person();
	// person.setFullName(cursor.getString(cursor
	// .getColumnIndex(IlaConstants.PERSON_FULL_NAME)));
	// person.setNickName(cursor.getString(cursor
	// .getColumnIndex(IlaConstants.PERSON_NICK_NAME)));
	// person.setUserName(cursor.getString(cursor
	// .getColumnIndex(IlaConstants.PERSON_USER_NAME)));
	// person.setRole(Role.valueOf(cursor.getString(cursor
	// .getColumnIndex(IlaConstants.PERSON_ROLE))));
	// person.setLatestToken(cursor.getString(cursor
	// .getColumnIndex(IlaConstants.PERSON_TOKEN)));
	// person.setLatestLoginIn(new Timestamp(cursor.getLong(cursor
	// .getColumnIndex(IlaConstants.PERSON_LATEST_LOGIN))));
	// }
	// ilaDb.close();
	// return person;
	//
	// }

}
