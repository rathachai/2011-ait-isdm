package com.btnc.ila.gui.driver;

import java.io.StringWriter;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlSerializer;

import android.app.ListActivity;
import android.content.Context;
import android.os.Bundle;
import android.util.Xml;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.btnc.ila.R;
import com.btnc.ila.controller.logistic.SubmitStatusController;
import com.btnc.ila.entity.task.Task;

public class DisplayTaskStatus extends ListActivity {

	String currentUser;
	StatusAdapter statusAdapter;
	SubmitStatusController submitStatusController;

	private final int MENU_SEND = 1, MENU_CANCEL = 2;
	private final int GROUP_SEND = 1, GROUP_CANCEL = 2;

	private static class Status {
		public String envelope;
		public String fromPos;
		public String toPos;
		public String status;
		public String dateRecorded;

		public Status(String envelope, String fromPos, String toPos,
				String status, String dateRecorded) {
			this.envelope = envelope;
			this.fromPos = fromPos;
			this.toPos = toPos;
			this.status = status;
			this.dateRecorded = dateRecorded;
		}

		public static String writeXml(List<Status> statuses) {
			XmlSerializer serializer = Xml.newSerializer();
			StringWriter writer = new StringWriter();
			try {
				serializer.setOutput(writer);
				serializer.startDocument("utf-8", true);
				serializer.startTag("", "tasks");
				serializer.attribute("", "number", String.valueOf(statuses
						.size()));
				for (Status status : statuses) {
					serializer.startTag("", "task");
					serializer.startTag("", "envelope");
					serializer.text(status.envelope);
					serializer.endTag("", "envelope");
					serializer.startTag("", "frompos");
					serializer.text(status.fromPos);
					serializer.endTag("", "frompos");
					serializer.startTag("", "topos");
					serializer.text(status.toPos);
					serializer.endTag("", "topos");
					serializer.startTag("", "status");
					serializer.text(status.status);
					serializer.endTag("", "status");
					serializer.startTag("", "datestamp");
					serializer.text(status.dateRecorded);
					serializer.endTag("", "datestamp");
					serializer.endTag("", "task");
				}
				serializer.endTag("", "tasks");
				serializer.endDocument();
				return writer.toString();
			} catch (Exception e) {
				throw new RuntimeException(e);
			}

		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setContentView(R.layout.task_status);
		super.onCreate(savedInstanceState);

		Bundle extras = getIntent().getExtras();
		currentUser = (String) extras.getString("USERNAME").trim();

		submitStatusController = SubmitStatusController.getInstance();
		statusAdapter = new StatusAdapter(this);
		this.setListAdapter(statusAdapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (!statusAdapter.statusList.isEmpty()) {
			menu.add(GROUP_SEND, MENU_SEND, 0, "Send").setIcon(R.drawable.send);
			menu.add(GROUP_CANCEL, MENU_CANCEL, 1, "Cancel").setIcon(
					R.drawable.cancel);
		}
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case MENU_SEND:
			submitStatusController
					.sendStatusReport(statusAdapter.statusListInXml);
			return true;
		case MENU_CANCEL:
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private class StatusAdapter extends BaseAdapter {
		private LayoutInflater layoutInflater;
		private ArrayList<Status> statusList;
		private String statusListInXml;

		public StatusAdapter(Context context) {
			layoutInflater = LayoutInflater.from(context);
			statusList = new ArrayList<Status>();
			getData();
		}

		public void getData() {
			List<Task> driverTaskList = submitStatusController
					.getDriverTaskList(currentUser);
			for (Task task : driverTaskList) {
//				if (task.getCurrentStatus().getStatus()
//						.equals(
//								com.btnc.ila.entity.common.Status.DELIVERED
//										.getStatus())) {
					DateFormat dateFormat = DateFormat.getDateTimeInstance();
					Status temp = new Status(
							task.getEnvelope().getIdentifier(), task
									.getSourcePointOfSale().getIdentifier(),
							task.getDestinationPointOfSale().getIdentifier(),
							task.getCurrentStatus().getStatus(), dateFormat
									.format(task.getCurrentRecordedDate()));
					statusList.add(temp);
//				}
			}
			statusListInXml = Status.writeXml(statusList);
		}

		public int getCount() {
			return statusList.size();
		}

		public Status getItem(int position) {
			return statusList.get(position);
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			final ViewHolder holder;
			View v = convertView;
			if ((v == null) || (v.getTag() == null)) {
				v = layoutInflater.inflate(R.layout.task_status_row, null);
				holder = new ViewHolder();
				holder.sCount = (TextView) v.findViewById(R.id.count);
				holder.sEnvelope = (TextView) v.findViewById(R.id.envelope);
				holder.sFromPos = (TextView) v.findViewById(R.id.frompos);
				holder.sToPos = (TextView) v.findViewById(R.id.topos);
				holder.sStatus = (TextView) v.findViewById(R.id.status);
				holder.sDateRecorded = (TextView) v
						.findViewById(R.id.daterecorded);
				v.setTag(holder);
			} else {
				holder = (ViewHolder) v.getTag();
			}
			holder.status = getItem(position);
			holder.sCount.setText(String.valueOf(position + 1));
			holder.sEnvelope.setText("Envelope : " + holder.status.envelope);
			holder.sFromPos.setText("From POS : " + holder.status.fromPos);
			holder.sToPos.setText("To POS : " + holder.status.toPos);
			holder.sStatus.setText("Status : " + holder.status.status);
			holder.sDateRecorded.setText("Recorded Date : "
					+ holder.status.dateRecorded);
			v.setTag(holder);
			return v;
		}

		public class ViewHolder {
			Status status;
			TextView sCount;
			TextView sEnvelope;
			TextView sFromPos;
			TextView sToPos;
			TextView sStatus;
			TextView sDateRecorded;
		}

	}

}
