package com.btnc.ila.gui.common;

import java.io.File;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.btnc.ila.R;
import com.btnc.ila.controller.authentication.LoginController;
import com.btnc.ila.entity.user.Person;
import com.btnc.ila.gui.driver.Home;

public class Login extends Activity {

	LoginController loginController;
	Person currentUser;

	// IlaDb ilaDb;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);

		File file = new File("/data/data/com.btnc.ila/files/btnc-ila.db4o");
		file.delete();

		loginController = LoginController.getInstance();

		// ilaDb = new IlaDb(this);
		// ilaDb.open();

		final EditText userName = (EditText) findViewById(R.id.username);
		final EditText passWord = (EditText) findViewById(R.id.password);
		Button submitBtn = (Button) findViewById(R.id.submit);
		submitBtn.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				if (validateUser(userName.getText().toString(), passWord
						.getText().toString())) {
					Toast.makeText(Login.this, "Login Validate !!!",
							Toast.LENGTH_SHORT).show();
					currentUser = loginController.getValidUserDetail(userName
							.getText().toString());

					// saveCurrentUserIntoDB();

					Bundle bundle = new Bundle();
					bundle.putString("FULLNAME", currentUser.getFullName());
					bundle.putString("USERNAME", currentUser.getUserName());
					bundle.putString("TOKEN", currentUser.getLatestToken());

					Intent intent = new Intent(Login.this, Home.class);
					intent.putExtras(bundle);
					startActivity(intent);
				} else {
					Toast.makeText(Login.this, "Login Failed !!!",
							Toast.LENGTH_SHORT).show();
				}
			}
		});
		
		boolean internetOK = true;
		
		try{
			final TextView internet  = (TextView) findViewById(R.id.internetstatus);
			final TextView traffy = (TextView) findViewById(R.id.traffystatus);
			final TextView btnc = (TextView) findViewById(R.id.btncstatus);
			
			traffy.setText("");
			internet.setText("");
			btnc.setText("");
			try{
				String urlstring = "http://www.google.com";
	
	    		URL url = new URL(urlstring);
	    		HttpURLConnection connection = (HttpURLConnection)url.openConnection();
	    		if(connection.getResponseCode()!=200){
	    			internet.setText("No internet connection");
	    			internetOK = false;
	    		}
			}catch(Exception e){
				internet.setText("No internet connection");
				internetOK = false;
			}
			
			if(internetOK)
			{
				try{
					String urlstring = "http://api.traffy.in.th/apis/apitraffy.php?format=csv&api=getCL&key=123&appid=123&q=1&linkid=101";
		
		    		URL url = new URL(urlstring);
		    		HttpURLConnection connection = (HttpURLConnection)url.openConnection();
		    		if(connection.getResponseCode()!=200){
		    			traffy.setText("Traffy service is not available");
		    		}
				}catch(Exception e){
					traffy.setText("Traffy service is not available");
				}
				
				try{
					
					btnc.setText("");
				}catch(Exception e){
					
				}
			}
		}catch(Exception ee){
			
		}

	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	private boolean validateUser(String username, String password) {
		// Valid User
		return loginController.validateUser(username, password);
	}

	public Person getCurrentUser() {
		return currentUser;
	}

	// private void saveCurrentUserIntoDB() {
	// ContentValues insertableValues = new ContentValues();
	// insertableValues.put(IlaConstants.PERSON_FULL_NAME, currentUser
	// .getFullName());
	// insertableValues.put(IlaConstants.PERSON_NICK_NAME, currentUser
	// .getNickName());
	// insertableValues.put(IlaConstants.PERSON_USER_NAME, currentUser
	// .getUserName());
	// insertableValues.put(IlaConstants.PERSON_ROLE, currentUser.getRole()
	// .getRole());
	// insertableValues.put(IlaConstants.PERSON_TOKEN, currentUser
	// .getLatestToken());
	// insertableValues.put(IlaConstants.PERSON_LATEST_LOGIN, currentUser
	// .getLatestLoginIn().getTime());
	// ilaDb.insert(IlaConstants.PERSON_TABLE, null, insertableValues);
	// ilaDb.close();
	// }
	
	
}
