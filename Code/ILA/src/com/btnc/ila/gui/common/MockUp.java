package com.btnc.ila.gui.common;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;
import android.util.Log;

import com.btnc.ila.entity.user.Driver;

public class MockUp {

	private final Context context;

	public MockUp(Context context) {
		this.context = context;
	}

	public String loadDriverTasks(Driver currentUser) {

		File file = new File(context.getFilesDir().getPath()
				+ "/envelope_route12.csv");
		try {
			InputStream is = context.getAssets().open("envelope_route12.csv");
			OutputStream os = new FileOutputStream(file);
			byte[] data = new byte[is.available()];
			is.read(data);
			os.write(data);
			is.close();
			os.close();
		} catch (Exception e) {
			Log.w("Exception", "Error writing " + file, e);
		}
		return file.getPath();
	}

	public String loadLinksBetweenPos() {
		File file = new File(context.getFilesDir().getPath()
				+ "/links_between_pos.csv");
		Log.d("path", file.getPath());
		try {
			InputStream is = context.getAssets().open("links_between_pos.csv");
			OutputStream os = new FileOutputStream(file);
			byte[] data = new byte[is.available()];
			is.read(data);
			os.write(data);
			is.close();
			os.close();
		} catch (Exception e) {
			Log.w("Exception", "Error writing " + file, e);
		}
		return file.getPath();
	}
}
