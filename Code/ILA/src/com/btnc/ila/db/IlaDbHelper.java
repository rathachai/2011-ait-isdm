package com.btnc.ila.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.util.Log;

public class IlaDbHelper extends SQLiteOpenHelper {

	private static final String CREATE_PERSON_TABLE = "create table "
			+ IlaConstants.PERSON_TABLE + " (" + IlaConstants.KEY_ID
			+ " integer primary key autoincrement, "
			+ IlaConstants.PERSON_FULL_NAME + " text, "
			+ IlaConstants.PERSON_NICK_NAME + " text, "
			+ IlaConstants.PERSON_USER_NAME + " text not null, "
			+ IlaConstants.PERSON_ROLE + " text, " + IlaConstants.PERSON_TOKEN
			+ " text not null, " + IlaConstants.PERSON_LATEST_LOGIN + " long);";

	public IlaDbHelper(Context context, String name, CursorFactory factory,
			int version) {
		super(context, name, factory, version);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		Log.v("MyDBhelper onCreate", "Creating all the tables");
		try {
			db.execSQL(CREATE_PERSON_TABLE);
		} catch (SQLiteException ex) {
			Log.v("Create table exception", ex.getMessage());
		}

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w("TaskDBAdapter", "Upgrading from version " + oldVersion + " to "
				+ newVersion + ", which will destroy all old data");
		// db.execSQL("drop table if exists " + Constants.TABLE_NAME);
		onCreate(db);

	}

}
