package com.btnc.ila.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

public class IlaDb {

	private SQLiteDatabase db;
	private final Context context;
	private final IlaDbHelper dbHelper;

	public IlaDb(Context c) {
		context = c;
		dbHelper = new IlaDbHelper(context, IlaConstants.DATABASE_NAME, null,
				IlaConstants.DATABASE_VERSION);
	}

	public void open() throws SQLiteException {
		try {
			db = dbHelper.getWritableDatabase();
		} catch (SQLiteException ex) {
			Log.v("Open database exception caught", ex.getMessage());
			db = dbHelper.getReadableDatabase();
		}
	}

	public void close() {
		db.close();
	}

	/**
	 * Convenience method for inserting a row into the database.
	 * @param tableName
	 * @param nullableColumn
	 * @param insertableValues
	 * @return the row ID of the newly inserted row, or -1 if an error occurred
	 */
	public long insert(String tableName, String nullableColumn,
			ContentValues insertableValues) {
		try {
			return db.insert(tableName, nullableColumn, insertableValues);
		} catch (SQLiteException ex) {
			Log.v("Insert into database exception caught", ex.getMessage());
			return -1;
		}
	}

	/**
	 * Query the given table, returning a Cursor over the result set.
	 * @param tableName The table name to compile the query against.
	 * @return A Cursor object, which is positioned before the first entry.
	 */
	public Cursor query(String tableName) {
		return db.query(tableName, null, null, null, null, null, null);
	}

	/**
	 * Query the given table, returning a Cursor over the result set.
	 * @param tableName
	 * @param columns
	 * @param selection
	 * @param selectionArgs
	 * @param groupBy
	 * @param having
	 * @param orderBy
	 * @param limit
	 * @return A Cursor object, which is positioned before the first entry.
	 */
	public Cursor query(String tableName, String[] columns, String selection,
			String[] selectionArgs, String groupBy, String having,
			String orderBy, String limit) {
		return db.query(tableName, columns, selection, selectionArgs, groupBy,
				having, orderBy, limit);
	}

	/**
	 * Convenience method for updating rows in the database.
	 * @param tableName
	 * @param updatableValues
	 * @param whereClause
	 * @param whereArgs
	 * @return the number of rows affected
	 */
	public int update(String tableName, ContentValues updatableValues,
			String whereClause, String[] whereArgs) {
		return db.update(tableName, updatableValues, whereClause, whereArgs);
	}

	/**
	 * Runs the provided SQL and returns a Cursor over the result set.
	 * @param sql
	 * @param selectionArgs
	 * @return A Cursor object, which is positioned before the first entry.
	 */
	public Cursor rawQuery(String sql, String[] selectionArgs) {
		return db.rawQuery(sql, selectionArgs);
	}

}
