package com.btnc.ila.db;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.query.Predicate;

public class Db4oManager {

	private final static String DB4OFILENAME = "/data/data/com.btnc.ila/btnc-ila.db4o";

	private ObjectContainer objectContainer = null;

	private static Db4oManager db4oManagerInstance;

	private Db4oManager() {

	}

	public static Db4oManager getInstance() {
		if (db4oManagerInstance == null) {
			db4oManagerInstance = new Db4oManager();
		}
		return db4oManagerInstance;
	}

	public void initialize() {
		objectContainer = Db4oEmbedded.openFile(
				Db4oEmbedded.newConfiguration(), DB4OFILENAME);
	}

	public void store(Object object) {
		objectContainer.store(object);
		objectContainer.commit();
	}

	@SuppressWarnings("unchecked")
	public ObjectSet retrieve(Object object) {
		return objectContainer.queryByExample(object);
	}

//	public ObjectSet retrieveSoda(Object object) {
//		Query query = objectContainer.query();
//		query.constrain(Object.class);
//		Constraint constraint = query.descend("createdDate").constrain(
//				new Date(1302616834989l));
//		query.descend("phoneNumber").constrain("772-360-1650").and(constraint);
//		return query.execute();
//	}

	@SuppressWarnings("unchecked")
	public ObjectSet retrieveByPredicate(Predicate<Object> objectPredicate) {
		return objectContainer.query(objectPredicate);
	}

	@SuppressWarnings("unchecked")
	public void update(Object oldObject, Object newObject) {
		ObjectSet result = objectContainer.queryByExample(oldObject);
		if (result.hasNext()) {
			Object found = result.next();
			found = newObject;
			objectContainer.store(found);
			objectContainer.commit();
		}
	}

	public void reset() {
		ObjectSet result = objectContainer.queryByExample(new Object());
		while (result.hasNext()) {
			objectContainer.delete(result);
		}
		objectContainer.commit();
	}

	public void finalize() {
		objectContainer.commit();
		objectContainer.close();
	}
}
