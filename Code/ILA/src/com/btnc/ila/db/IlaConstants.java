package com.btnc.ila.db;

public class IlaConstants {
	public static final String DATABASE_NAME = "ila_db";
	public static final int DATABASE_VERSION = 1;

	public static final String KEY_ID = "_id";
	
	// Person
	public static final String PERSON_TABLE = "person";
	public static final String PERSON_FULL_NAME = "fullname";
	public static final String PERSON_NICK_NAME = "nickname";
	public static final String PERSON_USER_NAME = "username";
	public static final String PERSON_ROLE = "role";
	public static final String PERSON_TOKEN = "latesttoken";
	public static final String PERSON_LATEST_LOGIN = "latestlogin";

}
