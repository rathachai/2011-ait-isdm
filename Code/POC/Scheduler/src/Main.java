import java.util.Calendar;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Collections;
import java.util.List;


public class Main {
	static String csv = 
			"3000000000069,336,349,F,54,3/24/11 10:00,TRANSFER,0,READY,336;" +
			"3000000000144,331,P21,F,51,3/24/11 10:00,TRANSFER,3,READY,331;" +
			"3000000000151,331,121,G,54,3/24/11 10:00,TRANSFER,0,READY,331;" +
			"3000000000168,360,302,G,54,3/24/11 10:00,TRANSFER,0,READY,360;" +
			"3000000000175,360,S38,G,57,3/24/11 10:00,TRANSFER,3,READY,360;" +
			"3000000000182,165,118,G,57,3/24/11 10:00,TRANSFER,2,ENROUTE,DR12;" +
			"3000000000199,185,S38,G,57,3/24/11 10:00,TRANSFER,0,ENROUTE,DR12;" +
			"3000000000205,331,151,J,57,3/24/11 10:00,TRANSFER,0,READY,331;" +
			"3000000000212,116,302,J,57,3/24/11 10:00,TRANSFER,1,ENROUTE,DR12;" +
			"3000000000229,135,S38,L,57,3/24/11 10:00,TRANSFER,1,ENROUTE,DR12;" +
			"3000000000236,129,S38,P,57,3/24/11 10:00,TRANSFER,1,ENROUTE,DR12;" +
			"3000000000243,334,336,P,57,3/24/11 10:00,TRANSFER,0,ENROUTE,DR12;" +
			"3000000000274,777,118,F,74,3/24/11 10:00,REFILL,0,ENROUTE,DR12;" +
			"3000000000281,777,336,F,74,3/24/11 10:00,REFILL,0,ENROUTE,DR12;" +
			"3000000000304,777,118,G,77,3/24/11 10:00,REFILL,2,ENROUTE,DR12;" +
			"3000000000311,777,302,J,77,3/24/11 10:00,REFILL,0,ENROUTE,DR12;" +
			"3000000000328,777,336,J,77,3/24/11 10:00,REFILL,2,ENROUTE,DR12;" +
			"3000000000342,7BP,118,G,77,3/24/11 10:00,REFILL,0,ENROUTE,DR12;" +
			"3000000000359,7BP,S38,S,85,3/24/11 10:00,REFILL,0,ENROUTE,DR12;";
	
	static PointOfSale WH;
	static LinkedList<PointOfSale> poslist = new LinkedList<PointOfSale>();
	static Hashtable <String, PointOfSale> posdic = new Hashtable <String, PointOfSale>();
	static LinkedList<Job> jobList = new  LinkedList<Job>();
	static LinkedList<Distance> routeList = new LinkedList<Distance>();
	
	static LinkedList<Schedule> scheduleList = new LinkedList<Schedule>();
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		init();
		
		LoadData();
		
		for(int i=0; i<50; i++){
			Calendar currentTime = Calendar.getInstance();
			//currentTime.set(Calendar.HOUR, 12);

			Schedule s = new Schedule(jobList, poslist, posdic, WH, currentTime);
	        s.BuildSchedule();
	        s.setShortTimeWeight(i/2+2);
	        s.setShortDistanceWeight(i/2+1);
	        scheduleList.add(s);
	        //SimplePrintMission(s);
		}
		
		Schedule shortTime;
		Schedule shortDistance;
		Schedule goodBusiness;
		
		for(int i=0; i<scheduleList.size(); i++){
			Schedule s = scheduleList.get(i);
			s.setShortTimeWeight(3);
			s.setShortDistanceWeight(1);
			s.setBusinessWight(1);
		}
		Collections.sort(scheduleList);
		shortTime = scheduleList.get(0);
		
		for(int i=0; i<scheduleList.size(); i++){
			Schedule s = scheduleList.get(i);
			s.setShortTimeWeight(1);
			s.setShortDistanceWeight(3);
			s.setBusinessWight(1);
		}
		Collections.sort(scheduleList);
		shortDistance = scheduleList.get(0);
		
		for(int i=0; i<scheduleList.size(); i++){
			Schedule s = scheduleList.get(i);
			s.setShortTimeWeight(1);
			s.setShortDistanceWeight(1);
			s.setBusinessWight(3);
		}
		Collections.sort(scheduleList);
		goodBusiness = scheduleList.get(0);
		
		
		//SimplePrintMission(shortTime);
		//SimplePrintMission(shortDistance);
		//SimplePrintMission(goodBusiness);
		
		PrintMissionList(shortTime);
		PrintMissionList(shortDistance);
		PrintMissionList(goodBusiness);
        
	}
	
	static void init()
	{
		PointOfSale p;
        p = new PointOfSale("777"); poslist.add(p); posdic.put("777", p); p.setIsWarehouse(true); WH = p;
        p = new PointOfSale("S38"); poslist.add(p); posdic.put("S38", p);
        p = new PointOfSale("302"); poslist.add(p); posdic.put("302", p); p.setAfterHour(16);
        p = new PointOfSale("118"); poslist.add(p); posdic.put("118", p);
        p = new PointOfSale("360"); poslist.add(p); posdic.put("360", p);
        p = new PointOfSale("309"); poslist.add(p); posdic.put("309", p); p.setAfterHour(16);
        p = new PointOfSale("336"); poslist.add(p); posdic.put("336", p);
        p = new PointOfSale("331"); poslist.add(p); posdic.put("331", p);
        p = new PointOfSale("X1"); posdic.put("X1", p);
        p = new PointOfSale("X2"); posdic.put("X2", p);
        
        
        routeList.add(new Distance(posdic.get("777"),posdic.get("331"),16,24));
        routeList.add(new Distance(posdic.get("331"),posdic.get("777"),16,24));
        routeList.add(new Distance(posdic.get("331"),posdic.get("X1"),3.6f,6));
        routeList.add(new Distance(posdic.get("X1"),posdic.get("331"),4,5));
        routeList.add(new Distance(posdic.get("309"),posdic.get("X1"),0,0));
        routeList.add(new Distance(posdic.get("X1"),posdic.get("309"),0,0));
        routeList.add(new Distance(posdic.get("118"),posdic.get("X1"),0,0));
        routeList.add(new Distance(posdic.get("X1"),posdic.get("118"),0,0));
        routeList.add(new Distance(posdic.get("331"),posdic.get("360"),5.5f,11));
        routeList.add(new Distance(posdic.get("360"),posdic.get("331"),6.7f,12));
        routeList.add(new Distance(posdic.get("360"),posdic.get("336"),15,26));
        routeList.add(new Distance(posdic.get("336"),posdic.get("360"),15,26));
        routeList.add(new Distance(posdic.get("X1"),posdic.get("336"),16.6f,18));
        routeList.add(new Distance(posdic.get("336"),posdic.get("X1"),14.3f,20));
        routeList.add(new Distance(posdic.get("360"),posdic.get("X2"),17,30));
        routeList.add(new Distance(posdic.get("X2"),posdic.get("360"),17,30));
        routeList.add(new Distance(posdic.get("302"),posdic.get("X2"),0,0));
        routeList.add(new Distance(posdic.get("X2"),posdic.get("302"),0,0));
        routeList.add(new Distance(posdic.get("S38"),posdic.get("X2"),0,0));
        routeList.add(new Distance(posdic.get("X2"),posdic.get("S38"),0,0));
        routeList.add(new Distance(posdic.get("X2"),posdic.get("336"),17.1f,21));
        routeList.add(new Distance(posdic.get("336"),posdic.get("X2"),17.1f,21));
        routeList.add(new Distance(posdic.get("777"),posdic.get("360"),13.4f,24));
        routeList.add(new Distance(posdic.get("360"),posdic.get("777"),13.4f,24));
        routeList.add(new Distance(posdic.get("777"),posdic.get("X2"),35,30));
        routeList.add(new Distance(posdic.get("X2"),posdic.get("777"),35,30));
        routeList.add(new Distance(posdic.get("777"),posdic.get("336"),23,31));
        routeList.add(new Distance(posdic.get("336"),posdic.get("777"),23,31));
        routeList.add(new Distance(posdic.get("360"),posdic.get("X1"),4.8f,8));
        routeList.add(new Distance(posdic.get("X1"),posdic.get("360"),17.4f,11));

        
        for(int i=0; i<routeList.size(); i++){
        	Distance route = routeList.get(i);
        	route.getStartPointOfSale().getDistanceList().put(route.getEndPointOfSale(), route);
        }
        
	}
	
	
	static void LoadData()
	{
		String[] lines = csv.split(";" );


        for(int i=0; i<lines.length; i++)
        {
        	String line = lines[i];
        	String[] cols = line.split(",");
        	
        	if(cols.length>8){

	            String envid = cols[0];
	            PointOfSale from = posdic.containsKey(cols[1]) == true ? posdic.get(cols[1]) : WH;
	            PointOfSale to = posdic.containsKey(cols[2]) == true ? posdic.get(cols[2]) : WH;
	
	            if (posdic.containsKey(cols[1]) && posdic.containsKey(cols[2]))
	            {
	
	                int priority = Integer.parseInt(cols[7]);
	
	                Job j1 = new Job();
	                j1.setEnvId(envid);
	                j1.setPlace(from);
	                j1.setAction("Pick Up");
	                j1.setPriority(0);
	
	                Job j2 = new Job();
	                j2.setEnvId(envid);
	                j2.setPlace(to);
	                j2.setAction("Drop Off");
	                j2.setDependentJob(j1);
	                j2.setPriority(priority);
	                j2.setAfterHour(to.getAfterHour()) ;
	                jobList.add(j1);
	                jobList.add(j2);
	
	            }
        	}
        }
        
        for(int i=0; i<poslist.size(); i++){
        	PointOfSale pos = poslist.get(i);
        	if(!pos.isWareHouse()){
        		Job j1 = new Job();
                j1.setEnvId("Bill") ;
                j1.setPlace(pos);
                j1.setAction("Pick Up") ;
                j1.setPriority(0);
                j1.setIsPickBill(true);
                jobList.add(j1);
        	}
        }
	}
	
	static void SimplePrintMission(Schedule s ){
		
		System.out.print(s.getAllDistanceScore() + "km   \t" + s.getAllTimeScore() + " minute   \t" + s.getAllBusinessScore() + "%      \t");
		for(int i=0; i<s.getMissionList().size(); i++){
			Mission m = s.getMissionList().get(i);
			System.out.print("[" + m.getPlace().getIdentifier() + "]  ");
		}
		
		System.out.println();
	}
	
	static void PrintMissionList(Schedule s){
		
		LinkedList<Mission> missionList = s.getMissionList();
		System.out.println("------------------------------------------------------");
		System.out.println(" Schedule " + s.getAllDistanceScore() + "km   \t" + s.getAllTimeScore() + " minute   \t" + s.getAllBusinessScore() + "% ");
		for(int i=0; i<missionList.size(); i++){
			Mission m = missionList.get(i);
			
			String mDepend = "\t";
			if(m.GetDependPos()!=null){
				mDepend = " (" + m.GetDependPos().getIdentifier() + ")";
			}
			
			System.out.println("   ["+ m.getPlace().getIdentifier() + "] " + mDepend + "  Time " + m.getEstimatedTime().get(Calendar.HOUR_OF_DAY)+ ":" + m.getEstimatedTime().get(Calendar.MINUTE)  + " \tPriority=" + m.GetPriorty() + "  \tAfter=" + m.GetAfterHour());
			
			for (int j=0; j<m.getJobList().size(); j++)
			{
				Job job = m.getJobList().get(j);
				String jDepend = "";
				if(job.getDependentJob()!=null){
					jDepend = " <--" +  job.getDependentJob().getPlace().getIdentifier() ;
				}
				System.out.println("       - "  + job.getAction() + " " + job.getEnvId()  + " (" + job.getPlace().getIdentifier() + jDepend + ")" );
			}
			
			System.out.println();
		}
		
		
	}

}

//------------------- Schedule ----------------------

class Schedule  implements Comparable<Schedule>{

	
	private PointOfSale WH;
	private PointOfSale startPos;
	private LinkedList<PointOfSale> poslist = new LinkedList<PointOfSale>();
	private Hashtable <String, PointOfSale> posdic = new Hashtable <String, PointOfSale>();
	private LinkedList<PointOfSale> allPoses = new LinkedList<PointOfSale>();
	private LinkedList<Job> depJobList = new LinkedList<Job>();
	private LinkedList<Job> indepJobList = new LinkedList<Job>();
	
	private Calendar startTime = Calendar.getInstance();
	
	
	private LinkedList<Mission> missionList = new LinkedList<Mission>();
	private int businessWeight = 1;
	private int shortTimeWeight = 1;
	private int shortDistanceWeight =1;
	
	private double allDistanceKm = 0;
	private double allTimeMin = 0;
	private double allBusiness = 0;
	
	
	
	 public int compareTo(Schedule o) {
		 	int compare = 0;
	        int time = (int) this.allTimeMin - (int)o.allTimeMin ;
	        
	        if(shortDistanceWeight>2)
	        {
	        	compare = (int) this.allDistanceKm - (int)o.allDistanceKm;
	        }else if(businessWeight>2)
	        {
	        	compare = (int) o.allBusiness - (int)this.allBusiness;
	        }
	        
	        if(compare==0){
	        	compare = time;
	        }
	        
	        return compare;
	}
	
	public Schedule(LinkedList<Job> jobs, LinkedList<PointOfSale> poses,Hashtable <String, PointOfSale> posdict, PointOfSale startPos,Calendar startTime)
	{
		this.allPoses.addAll(poses);
		this.startPos = startPos;
		this.posdic = posdict;
		this.startTime.set(Calendar.HOUR, startTime.get(Calendar.HOUR));
		this.startTime.set(Calendar.MINUTE, startTime.get(Calendar.MINUTE));
		
		
		//Find WH
		for(int i=0; i<poses.size();i++){
			PointOfSale pos = poses.get(i);
			if(pos.isWareHouse()){
				this.WH = pos;
				break;
			}
		}
		
		this.poslist.add(this.WH);
		
		//Separate independence and dependence mission
		for(int i=0; i<jobs.size();i++){
			Job job = jobs.get(i);
			if(job.getDependentJob()==null){
				this.indepJobList.add(job);
			}else{
				this.depJobList.add(job);
			}
			
			if(!this.poslist.contains(job.getPlace()))
			{
				this.poslist.add(job.getPlace());
			}
		}
		
	}
	
	public double getAllDistanceScore(){
		return this.allDistanceKm;
	}
	
	public double getAllTimeScore(){
		return this.allTimeMin;
	}
	
	public double getAllBusinessScore(){
		return this.allBusiness;
	}
	
	public LinkedList<Mission> getMissionList(){
		return this.missionList;
	}
	
	public int getBusinessWeight(){
		return this.businessWeight;
	}
	
	public void setBusinessWight(int businessWeight){
		this.businessWeight = businessWeight;
	}
	
	public int getShortDistanceWeight(){
		return this.shortDistanceWeight;
	}
	
	public void setShortDistanceWeight(int shortDistanceWeight){
		this.shortDistanceWeight = shortDistanceWeight;
	}
	
	public int getShortTimeWeight(){
		return this.shortTimeWeight;
	}
	
	public void setShortTimeWeight(int shortTimeWeight){
		this.shortTimeWeight = shortTimeWeight;
	}
	
	public void BuildSchedule(){
		GetherIndepPos();
        SortIndepPos(); //Not implement yet
        
        AddDependence();
        
        CleanSchedule1();
        CleanSchedule2();
        CleanSchedule3();
        CleanSchedule2();
        
        CalculateScore();
	}
	
	public void CalculateScore()
	{
		allDistanceKm = 0;
		allTimeMin = 0;
		allBusiness = 0;
		
		Calendar estimate = Calendar.getInstance();
		//estimate.set(Calendar.HOUR, startTime.get(Calendar.HOUR));
		//estimate.set(Calendar.MINUTE, startTime.get(Calendar.MINUTE));
	
		double bizNum = 0;
		double bizVal =0;
		
		for(int i=0; i<missionList.size()-1; i++){
			Mission e = missionList.get(i);
			Mission f = missionList.get(i+1);
			
			LinkedList<Distance> r = GetDistance(e.getPlace(),f.getPlace());
			
			if(r.size()>0){
				for(int j=0; j<r.size(); j++){
					Distance ro = r.get(j);
					allDistanceKm += ro.getDistanceInKiloMeters();
					allTimeMin += ro.getTimeInMinutes() +20;
					estimate.add(Calendar.MINUTE, (int)ro.getTimeInMinutes() +20);
				}
			}else{
				allDistanceKm +=40;
				allTimeMin += 60;
				estimate.add(Calendar.MINUTE, 60);
			}
			
			f.getEstimatedTime().set(Calendar.HOUR, estimate.get(Calendar.HOUR));
			f.getEstimatedTime().set(Calendar.MINUTE, estimate.get(Calendar.MINUTE));
			
			if(f.GetPriorty()>0){
				bizNum++;
				
				Calendar target = Calendar.getInstance();
				target.set(Calendar.HOUR, startTime.get(Calendar.HOUR));
				target.set(Calendar.MINUTE, startTime.get(Calendar.MINUTE));
				
				target.add(Calendar.HOUR, f.GetPriorty());
				
				//System.out.println( estimate.get(Calendar.HOUR_OF_DAY) + ":" +estimate.get(Calendar.MINUTE) + " <= " + target.get(Calendar.HOUR_OF_DAY) + ":" + target.get(Calendar.MINUTE));
				
				if( estimate.get(Calendar.HOUR_OF_DAY) <= target.get(Calendar.HOUR_OF_DAY)){
					bizVal++;
					
				}
			}
			
		}
		
		allDistanceKm = Math.round(allDistanceKm*100)/100.00;
		allTimeMin = Math.round(allTimeMin*100)/100.00;
		
		if(bizNum>0){
			allBusiness = bizVal*100.0/bizNum;
		}else{
			allBusiness = 100;
		}
		allBusiness = Math.round(allBusiness*100)/100.00;
	}
	
	void GetherIndepPos()
    { 
		// Add first WH in mission
		boolean setStart = false;
		
		if(startPos == WH){
	        Mission missionFromWH = new Mission();
	        missionFromWH.setPlace(WH);
	        missionList.add(missionFromWH);
	        
	        for(int i = 0; i<indepJobList.size(); i++)
	        {
	        	Job job = indepJobList.get(i);
	            if (job.getPlace().isWareHouse())
	            {
	            	missionFromWH.getJobList().add(job);
	            }
	        }
	
	        if (missionFromWH.getJobList().size() == 0)
	        {
	            Job job = new Job();
	            job.setPlace(WH);
	            job.setEnvId("");
	            job.setAction("Start");
	            missionFromWH.getJobList().add(job);
	        }
	        
	        setStart=true;
		}
        // Add all POS to be Mission
        
        for (int i=0; i<poslist.size(); i++)
        {
        	if(!poslist.get(i).isWareHouse()){
        		Mission m = new Mission();
        		m.setPlace(poslist.get(i));
        		
        		if(!setStart && m.getPlace() == startPos){
        			missionList.add(0,m);
        			setStart=true;
        		}else{
        			missionList.add(m);
        		}
        	}
        }
        
        if(!setStart){
        	Mission m = new Mission();
    		m.setPlace(startPos);
    		missionList.add(0,m);
        }
        
        // Add job to Mission
        for(int j = 0; j<indepJobList.size(); j++)
        {
        	Job job = indepJobList.get(j);
        	if (!job.getPlace().isWareHouse())
            {
	        	for(int m = 0; m<missionList.size(); m++){
	        		Mission mission = missionList.get(m);
	        		if(job.getPlace().getIdentifier() == mission.getPlace().getIdentifier())
	        		{
	        			mission.getJobList().add(job);
	        		}
	        	}
            }
        }
        
        //Add last mission
        Mission missionToWH = new Mission();
        missionToWH.setPlace(WH);
        missionList.add(missionToWH);
        
        for(int i = 0; i<depJobList.size(); i++)
        {
        	Job job = depJobList.get(i);
            if (job.getPlace().isWareHouse())
            {
            	missionToWH.getJobList().add(job);
            }
        }

        if (missionToWH.getJobList().size() == 0)
        {
            Job job = new Job();
            job.setPlace(WH);
            job.setEnvId("") ;
            job.setAction("End");
            missionToWH.getJobList().add(job);
        }

    }

	void SortIndepPos()
    {
		LinkedList<Mission> suff = new LinkedList<Mission>();
		
		suff.addAll(missionList);
		Mission first = suff.getFirst();
		Mission last = suff.getLast();
		
		
		double cost = 9999999;
		
		for (int i=0; i<40; i++){
			LinkedList<Mission> s = new LinkedList<Mission>();
			s.addAll(suff);
			s.remove(last);
			s.remove(first);
			Collections.shuffle(s);
			
			
		/*	//Add
			Mission p118 = null;
			Mission p309 = null;
			
			Mission pS38 = null;
			Mission p302 = null;
			
			for(int j=0; j<s.size(); j++){
				Mission m = s.get(j);
				if(m.Place.ID == "118"){
					p118 = m;
				}else
				if(m.Place.ID == "S38"){
					pS38 = m;
				}else
				if(m.Place.ID == "302"){
					p302 = m;
				}else
				if(m.Place.ID == "309"){
					p309 = m;
				}
			}
			
			//Swap X1
			if(p118!=null && p309!=null){
				double b = Math.random();
				if(b>0.5){
					Mission x = p118;
					p118=p309;
					p309=x;
				}
				s.remove(p118);
				for(int j=0; j<s.size(); j++){
					Mission m = s.get(j);
					if(m==p309){
						s.add(j+1, p118);
					}
				}
			}
			
			//Swap X2
			if(p302!=null && pS38!=null){
				double b = Math.random();
				if(b>0.5){
					Mission x = p302;
					p302=pS38;
					pS38=x;
				}
				s.remove(pS38);
				for(int j=0; j<s.size(); j++){
					Mission m = s.get(j);
					if(m==p302){
						s.add(j+1, pS38);
					}
				}
			}
			
		*/
			
			//add first / last
			s.addFirst(first);
			s.addLast(last);
			double c = GetCost(s);
			if(c<cost){
				cost=c;
				suff= s;
			}
		}
		
		//System.out.println("Total = " + cost);
		
		missionList = suff;
    }
	
	public double GetCost(){
		return Math.round( GetCost(missionList) *100)/100.0;
	}
	
	 double GetCost(LinkedList<Mission> missions){
		double d = 0.0;
		
		for(int i=0; i<missions.size()-1; i++){
			Mission e = missions.get(i);
			Mission f = missions.get(i+1);
			
			LinkedList<Distance> r = GetDistance(e.getPlace(),f.getPlace());
			double c = 0.0;
			if(r.size()>0){
				for(int j=0; j<r.size(); j++){
					c+= getDistanceCost(r.get(j));
				}
			}else{
				c=40.0;
			}
			//c*=r.size();
			//c = Math.scalb(c, 2);
			c = Math.round(c*100)/100.0;
			
			d+= c;
			
			//System.out.print(e.Place.ID + "(" + c +  ") , ");
		}
		
		
		//System.out.print(missions.getLast().Place.ID);
		
		//System.out.println("      =     " + d);
		
		return d;
	}
	
	double getDistanceCost(Distance r)
	{
		double cost = 0;
		
		if(this.shortDistanceWeight > this.shortDistanceWeight){
			cost = r.getDistanceInKiloMeters();
		}else{
			cost = r.getTimeInMinutes();
		}
		
		return cost;
	}
	
	LinkedList<Distance> GetDistance(PointOfSale f, PointOfSale t){
		
		PointOfSale from = posdic.get(f.getIdentifier());
		PointOfSale to = posdic.get(t.getIdentifier());
		
		if(from.getIdentifier()=="309" || from.getIdentifier()=="118"){
			from = posdic.get("X1");
		}
		if(from.getIdentifier()=="302" || from.getIdentifier()=="S38"){
			from = posdic.get("X2");
		}
		
		if(to.getIdentifier()=="309" || to.getIdentifier()=="118"){
			to = posdic.get("X1");
		}
		if(to.getIdentifier()=="302" || to.getIdentifier()=="S38"){
			to = posdic.get("X2");
		}
		
		
		LinkedList<Distance> r = new LinkedList<Distance>();
		if(from.getDistanceList().containsKey(to)){
			r.add(from.getDistanceList().get(to));
		}else{
			double distance = 999999;
			
			for(PointOfSale p : from.getDistanceList().keySet()){
				Distance r1 = from.getDistanceList().get(p);
				if(p.getDistanceList().containsKey(to)){
					Distance r2 = p.getDistanceList().get(to);
					double d = getDistanceCost(r1) + getDistanceCost(r2);
					if(d<distance){
						distance = d;
						r.clear();
						r.add(r1);
						r.add(r2);
					}
				}
			}
		}
		return r;
	}
	
	void AddDependence()
	{
		LinkedList<Mission> depMissionList = new LinkedList<Mission>();
		//Add indepJob
		for(int j = 0; j<depJobList.size(); j++)
        {
        	Job job = depJobList.get(j);
        	
        	if (!job.getPlace().isWareHouse())
            {
        		boolean isAdded = false;
	        	for(int m = 0; m<depMissionList.size(); m++){
	        		Mission mission = depMissionList.get(m);
	        		if(job.getPlace() == mission.getPlace() && job.getDependentJob().getPlace() == mission.GetDependPos())
	        		{
	        			mission.getJobList().add(job);
	        			isAdded = true;
	        			break;
	        		}
	        	}
	        	
	        	if(!isAdded)
	        	{
        			Mission newMission = new Mission();
        			newMission.setPlace(job.getPlace());
        			newMission.getJobList().add(job);
        			depMissionList.add(newMission);
        		}
            }
        }
		
		
		//Try to add mission
		for(int i=0; i<depMissionList.size(); i++)
		{
			Mission m = depMissionList.get(i);
			AddMission(m);
		}
		
		
		
	}
	
	void AddMission(Mission m)
	{
		//Find start index
		int startIndex = 0;
		
		for(int i=0; i<missionList.size(); i++)
		{
			if(m.GetDependPos() == missionList.get(i).getPlace())
			{
				startIndex = i+1;
				break;
			}
		}
		
		if(startIndex==0){
			
			startIndex = missionList.size()-1;
		}
		
		
		//Loop to get the best position
		double cost = 99999;
		int suitableIndex = startIndex;
		for(int i=startIndex; i<missionList.size(); i++)
		{
			if(missionList.get(i).getPlace()==m.getPlace())
			{
				suitableIndex = i;
				break;
			}
			double c = CalculateCostFromAddedIndex(m,i);
			if(c < cost){
				cost = c;
				suitableIndex = i;
			}
		}
		//System.out.println(startIndex+"<"+suitableIndex);
		//Add the best position

		missionList.add(suitableIndex, m);
	}
	
	double CalculateCostFromAddedIndex(Mission m, int index)
	{
		LinkedList<Mission> missions = new LinkedList<Mission>();
		missions.addAll(missionList);
		missions.add(index, m);
		
		return GetCost(missions);
	}
	
	void CleanSchedule1()
	{
		//Generate EmptyMission and JopedMission
		LinkedList<Mission> billJob = new LinkedList<Mission>();
		LinkedList<Mission> havingJob = new LinkedList<Mission>();
		
		for(int i=1; i<missionList.size()-1; i++){
			Mission m = missionList.get(i);
			if(m.IsOnlyBill()){
				billJob.add(m);
			}else{
				havingJob.add(m);
			}
		}
		
		for(int e=0; e<billJob.size(); e++){
			Mission bill = billJob.get(e);
			for(int h=0; h<havingJob.size(); h++){
				Mission having = havingJob.get(h);
				if(bill!=having && bill.getPlace()==having.getPlace()){
					having.getJobList().add(bill.getJobList().get(0));
					missionList.remove(bill);
					break;
				}
				
			}
		}
	
	}
	
	void CleanSchedule2()
	{
		LinkedList<Mission> result = new LinkedList<Mission>();
		for(int i=0; i<missionList.size()-1; i++){
			Mission m1 = missionList.get(i);
			Mission m2 = missionList.get(i+1);
			
			if(m1.getPlace() == m2.getPlace()){
				m1.getJobList().addAll(m2.getJobList());
				result.add(m1);
				i++;
			}else{
				result.add(m1);
			}
		}
		result.add(missionList.getLast());
		
		missionList = result;
	}
	
	void CleanSchedule3()
	{
		CalculateScore();

		LinkedList<Mission> wrong = new LinkedList<Mission>();
		
		for(int i=0; i<missionList.size(); i++){
			Mission m = missionList.get(i);
			int targetHour = m.GetAfterHour() ;
			int estHour = m.getEstimatedTime().get(Calendar.HOUR_OF_DAY);
			if(m.GetAfterHour()>0 && estHour<targetHour){
				wrong.add(m);
				//System.out.println(estHour + "<" + targetHour);
			}
		}
		
		for(int i=0; i<wrong.size(); i++){
			missionList.remove(wrong.get(i));
		}
		
		for(int i=0; i<wrong.size(); i++)
		{
			Mission m = wrong.get(i);
			AddMission2(m);
		}
	}

	void AddMission2(Mission m)
	{
		//Find start index
		int startIndex = 0;
		
		for(int i=0; i<missionList.size(); i++)
		{
			int targetHour = m.GetAfterHour() ;
			int estHour = m.getEstimatedTime().get(Calendar.HOUR_OF_DAY);
			if(estHour > targetHour)
			{
				startIndex = i+1;
				break;
			}
		}
		
		if(startIndex==0){
			
			startIndex = missionList.size()-1;
		}
		
		
		//Loop to get the best position
		double cost = 99999;
		int suitableIndex = startIndex;
		for(int i=startIndex; i<missionList.size(); i++)
		{
			if(missionList.get(i).getPlace()==m.getPlace())
			{
				suitableIndex = i;
				break;
			}
			double c = CalculateCostFromAddedIndex(m,i);
			if(c < cost){
				cost = c;
				suitableIndex = i;
			}
		}
		//System.out.println(startIndex+"<"+suitableIndex);
		//Add the best position
		missionList.add(suitableIndex, m);
	}
}

//------------------- POS ----------------------
class LatLonPoint{
	public LatLonPoint(double a, double b){
		
	}
}

class PointOfSale {

	private String identifier;
	private String fullName;
	private String shortName;
	private LatLonPoint locationPoint;

	private boolean isWarehouse;
    private int afterHour = 0;
    
    private Hashtable <PointOfSale, Distance> distances = new Hashtable <PointOfSale, Distance>();

    
	public PointOfSale() {
	}
	

	public PointOfSale(String identifier, String fullName, String shortName,
			double latitude, double longitude) {
		super();
		this.identifier = identifier;
		this.fullName = fullName;
		this.shortName = shortName;
		this.isWarehouse=false;
		this.locationPoint = new LatLonPoint(latitude, longitude);
	}
	
	public Hashtable <PointOfSale, Distance> getDistanceList(){
		return distances;
	}
	
	//public void setDistanceList(Hashtable <PointOfSale, Distance> distances){
	//	this.distances = distances;
	//}

	public PointOfSale(String identifier) {
		this.identifier = identifier;
		this.isWarehouse = false;
	}
	
	public boolean isWareHouse(){
		return this.isWarehouse;
	}
	
	public void setIsWarehouse(boolean isWarehouse){
		this.isWarehouse = isWarehouse;
	}
	
	public int getAfterHour(){
		return this.afterHour;
	}
	
	public void setAfterHour(int afterHour){
		this.afterHour = afterHour;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public LatLonPoint getLocationPoint() {
		return locationPoint;
	}

	public void setLocationPoint(LatLonPoint locationPoint) {
		this.locationPoint = locationPoint;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((identifier == null) ? 0 : identifier.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PointOfSale other = (PointOfSale) obj;
		if (identifier == null) {
			if (other.identifier != null)
				return false;
		} else if (!identifier.equals(other.identifier))
			return false;
		return true;
	}

}

//------------------- JOB ----------------------
class Driver {
	public Driver(){
		
	}
}

class Task{
	public Task(){
		
	}
}

class Job {
	private Driver driver;
	private Task task;
	private PointOfSale place;
	private Job dependentJob;
	
	private String envId;
	private int priority;
    private String action = "";
    private boolean isPickBill = false;
    private int afterHour =0;
    
	public Job(Driver driver, Task task, PointOfSale place, Job dependentJob) {
		super();
		this.driver = driver;
		this.task = task;
		this.place = place;
		this.dependentJob = dependentJob;
	}
	
	public Job(){
		
	}

	public boolean getIsPickBill(){
    	return this.isPickBill;
    }
    
    public void setIsPickBill(boolean isPickBill){
    	this.isPickBill = isPickBill;
    }
    
    public int getAfterHour(){
    	return this.afterHour;
    }
    
    public void setAfterHour(int afterHour){
    	this.afterHour = afterHour;
    }
    
	public String getAction(){
    	return this.action;
    }
    
    public void setAction(String action){
    	this.action = action;
    }
    
	public String getEnvId(){
		return this.envId;
	}
	
	public void setEnvId(String envId){
		this.envId = envId;
	}
	
    public int getPriority(){
    	return this.priority;
    }
    
    public void setPriority(int priority){
    	this.priority = priority;
    }
    
	public Driver getDriver() {
		return driver;
	}

	public void setDriver(Driver driver) {
		this.driver = driver;
	}

	public Task getTask() {
		return task;
	}

	public void setTask(Task task) {
		this.task = task;
	}

	public PointOfSale getPlace() {
		return place;
	}

	public void setPlace(PointOfSale place) {
		this.place = place;
	}

	public Job getDependentJob() {
		return dependentJob;
	}

	public void setDependentJob(Job dependentJob) {
		this.dependentJob = dependentJob;
	}

}

//------------------- Mission ----------------------
class Mission
{
	private LinkedList<Job> jobs = new  LinkedList<Job>();
	private PointOfSale place;
	private Calendar estimatedTime = Calendar.getInstance();
	
	
	
	public int GetAfterHour(){
		int h = 0;
		
		for(int i=0; i<this.jobs.size();i++){
			Job job = this.jobs.get(i);
			if(job.getAfterHour()>h)
			{
				h=job.getAfterHour();
			}
		}
		
		return h;
	}
	public boolean IsOnlyBill(){
		boolean res = false;
		
		if(this.jobs.size()==1){
			res = this.jobs.get(0).getIsPickBill();
		}
		
		return res;
	}
	
	public PointOfSale GetDependPos(){
		PointOfSale depend = null;
		
		if(this.jobs.size()>0 && this.jobs.get(0)!=null && this.jobs.get(0).getDependentJob()!=null && this.jobs.get(0).getDependentJob().getPlace()!=null)
		{
			depend = this.jobs.get(0).getDependentJob().getPlace();
		}
		
		return depend;
	}
	
	public Calendar getEstimatedTime(){
		return this.estimatedTime;
	}
	
	public LinkedList<Job> getJobList(){
		return this.jobs;
	}
	
	public PointOfSale getPlace(){
		return this.place;
	}
	
	public void setPlace(PointOfSale place){
		this.place = place;
	}
	
	public boolean HasJobs(){
		return this.jobs.size()>0?true:false;
	}

	public int GetPriorty()
	{
		int priority = 99;
		
		for(int i=0; i<this.jobs.size(); i++)
		{
			int p = this.jobs.get(i).getPriority();
			if(p>0 && p<priority )
			{
				priority = p;
			}
		}
		
		return priority==99?0:priority;
	}
	
	public Mission()
	{
	}
	
	
}

//---------------------------
class Link{
	public Link(){
		
	}
}

class Distance {

	private PointOfSale startPointOfSale;
	private PointOfSale endPointOfSale;
	private float distanceInKiloMeters = 0.0f;
	private int timeInMinutes = 0;
	private List<Link> traffyLinksInBetween;

	public Distance(PointOfSale startPointOfSale, PointOfSale endPointOfSale,
			float distanceInKiloMeters, int timeInMinutes,
			List<Link> traffyLinksInBetween) {
		super();
		this.startPointOfSale = startPointOfSale;
		this.endPointOfSale = endPointOfSale;
		this.distanceInKiloMeters = distanceInKiloMeters;
		this.timeInMinutes = timeInMinutes;
		this.traffyLinksInBetween = traffyLinksInBetween;
	}
	
	public Distance(PointOfSale startPointOfSale, PointOfSale endPointOfSale, float distanceInKiloMeters, int timeInMinutes){
		this.startPointOfSale = startPointOfSale;
		this.endPointOfSale = endPointOfSale;
		this.distanceInKiloMeters = distanceInKiloMeters;
		this.timeInMinutes = timeInMinutes;
		
	}

	public PointOfSale getStartPointOfSale() {
		return startPointOfSale;
	}

	public void setStartPointOfSale(PointOfSale startPointOfSale) {
		this.startPointOfSale = startPointOfSale;
	}

	public PointOfSale getEndPointOfSale() {
		return endPointOfSale;
	}

	public void setEndPointOfSale(PointOfSale endPointOfSale) {
		this.endPointOfSale = endPointOfSale;
	}

	public float getDistanceInKiloMeters() {
		return distanceInKiloMeters;
	}

	public void setDistanceInKiloMeters(float distanceInKiloMeters) {
		this.distanceInKiloMeters = distanceInKiloMeters;
	}

	public int getTimeInMinutes() {
		return timeInMinutes;
	}

	public void setTimeInMinutes(int timeInMinutes) {
		this.timeInMinutes = timeInMinutes;
	}

	public List<Link> getTraffyLinksInBetween() {
		return traffyLinksInBetween;
	}

	public void setTraffyLinksInBetween(List<Link> traffyLinksInBetween) {
		this.traffyLinksInBetween = traffyLinksInBetween;
	}

}
