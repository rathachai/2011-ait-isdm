import java.util.Calendar;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Collections;


public class Main {
	static String csv = 
			"3000000000069,336,349,F,54,3/24/11 10:00,TRANSFER,0,READY,336;" +
			"3000000000144,331,P21,F,51,3/24/11 10:00,TRANSFER,3,READY,331;" +
			"3000000000151,331,121,G,54,3/24/11 10:00,TRANSFER,0,READY,331;" +
			"3000000000168,360,302,G,54,3/24/11 10:00,TRANSFER,0,READY,360;" +
			"3000000000175,360,S38,G,57,3/24/11 10:00,TRANSFER,3,READY,360;" +
			"3000000000182,165,118,G,57,3/24/11 10:00,TRANSFER,2,ENROUTE,DR12;" +
			"3000000000199,185,S38,G,57,3/24/11 10:00,TRANSFER,0,ENROUTE,DR12;" +
			"3000000000205,331,151,J,57,3/24/11 10:00,TRANSFER,0,READY,331;" +
			"3000000000212,116,302,J,57,3/24/11 10:00,TRANSFER,1,ENROUTE,DR12;" +
			"3000000000229,135,S38,L,57,3/24/11 10:00,TRANSFER,1,ENROUTE,DR12;" +
			"3000000000236,129,S38,P,57,3/24/11 10:00,TRANSFER,1,ENROUTE,DR12;" +
			"3000000000243,334,336,P,57,3/24/11 10:00,TRANSFER,0,ENROUTE,DR12;" +
			"3000000000274,777,118,F,74,3/24/11 10:00,REFILL,0,ENROUTE,DR12;" +
			"3000000000281,777,336,F,74,3/24/11 10:00,REFILL,0,ENROUTE,DR12;" +
			"3000000000304,777,118,G,77,3/24/11 10:00,REFILL,2,ENROUTE,DR12;" +
			"3000000000311,777,302,J,77,3/24/11 10:00,REFILL,0,ENROUTE,DR12;" +
			"3000000000328,777,336,J,77,3/24/11 10:00,REFILL,2,ENROUTE,DR12;" +
			"3000000000342,7BP,118,G,77,3/24/11 10:00,REFILL,0,ENROUTE,DR12;" +
			"3000000000359,7BP,S38,S,85,3/24/11 10:00,REFILL,0,ENROUTE,DR12;";
	
	static Pos WH;
	static LinkedList<Pos> poslist = new LinkedList<Pos>();
	static Hashtable <String, Pos> posdic = new Hashtable <String, Pos>();
	static LinkedList<Job> jobList = new  LinkedList<Job>();
	static LinkedList<Route> routeList = new LinkedList<Route>();
	
	static LinkedList<Schedule> scheduleList = new LinkedList<Schedule>();
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		init();
		
		LoadData();
		
		for(int i=0; i<100; i++){
			Calendar currentTime = Calendar.getInstance();
			//currentTime.set(Calendar.HOUR, 12);

			Schedule s = new Schedule(jobList, poslist, posdic, WH, currentTime);
	        s.BuildSchedule();
	        s.ShortDistance = i/2+2;
	        s.ShortDistance = i/2+1;
	        scheduleList.add(s);
	        //SimplePrintMission(s);
		}
		
		Schedule shortTime;
		Schedule shortDistance;
		Schedule goodBusiness;
		
		for(int i=0; i<scheduleList.size(); i++){
			Schedule s = scheduleList.get(i);
			s.ShortTime=3;
			s.ShortDistance=1;
			s.HighBusiness=1;
		}
		Collections.sort(scheduleList);
		shortTime = scheduleList.get(0);
		
		for(int i=0; i<scheduleList.size(); i++){
			Schedule s = scheduleList.get(i);
			s.ShortTime=1;
			s.ShortDistance=3;
			s.HighBusiness=1;
		}
		Collections.sort(scheduleList);
		shortDistance = scheduleList.get(0);
		
		for(int i=0; i<scheduleList.size(); i++){
			Schedule s = scheduleList.get(i);
			s.ShortTime=1;
			s.ShortDistance=1;
			s.HighBusiness=3;
		}
		Collections.sort(scheduleList);
		goodBusiness = scheduleList.get(0);
		
		
		//SimplePrintMission(shortTime);
		//SimplePrintMission(shortDistance);
		//SimplePrintMission(goodBusiness);
		
		PrintMissionList(shortTime);
		PrintMissionList(shortDistance);
		PrintMissionList(goodBusiness);
        
	}
	
	static void init()
	{
		Pos p;
        p = new Pos("777"); poslist.add(p); posdic.put("777", p); p.IsWH = true; WH = p;
        p = new Pos("S38"); poslist.add(p); posdic.put("S38", p);
        p = new Pos("302"); poslist.add(p); posdic.put("302", p); p.AfterHour = 16;
        p = new Pos("118"); poslist.add(p); posdic.put("118", p);
        p = new Pos("360"); poslist.add(p); posdic.put("360", p);
        p = new Pos("309"); poslist.add(p); posdic.put("309", p); p.AfterHour = 16;
        p = new Pos("336"); poslist.add(p); posdic.put("336", p);
        p = new Pos("331"); poslist.add(p); posdic.put("331", p);
        p = new Pos("X1"); posdic.put("X1", p);
        p = new Pos("X2"); posdic.put("X2", p);
        
        
        routeList.add(new Route(posdic.get("777"),posdic.get("331"),16,24));
        routeList.add(new Route(posdic.get("331"),posdic.get("777"),16,24));
        routeList.add(new Route(posdic.get("331"),posdic.get("X1"),3.6,6));
        routeList.add(new Route(posdic.get("X1"),posdic.get("331"),4,5));
        routeList.add(new Route(posdic.get("309"),posdic.get("X1"),0,0));
        routeList.add(new Route(posdic.get("X1"),posdic.get("309"),0,0));
        routeList.add(new Route(posdic.get("118"),posdic.get("X1"),0,0));
        routeList.add(new Route(posdic.get("X1"),posdic.get("118"),0,0));
        routeList.add(new Route(posdic.get("331"),posdic.get("360"),5.5,11));
        routeList.add(new Route(posdic.get("360"),posdic.get("331"),6.7,12));
        routeList.add(new Route(posdic.get("360"),posdic.get("336"),15,26));
        routeList.add(new Route(posdic.get("336"),posdic.get("360"),15,26));
        routeList.add(new Route(posdic.get("X1"),posdic.get("336"),16.6,18));
        routeList.add(new Route(posdic.get("336"),posdic.get("X1"),14.3,20));
        routeList.add(new Route(posdic.get("360"),posdic.get("X2"),17,30));
        routeList.add(new Route(posdic.get("X2"),posdic.get("360"),17,30));
        routeList.add(new Route(posdic.get("302"),posdic.get("X2"),0,0));
        routeList.add(new Route(posdic.get("X2"),posdic.get("302"),0,0));
        routeList.add(new Route(posdic.get("S38"),posdic.get("X2"),0,0));
        routeList.add(new Route(posdic.get("X2"),posdic.get("S38"),0,0));
        routeList.add(new Route(posdic.get("X2"),posdic.get("336"),17.1,21));
        routeList.add(new Route(posdic.get("336"),posdic.get("X2"),17.1,21));
        routeList.add(new Route(posdic.get("777"),posdic.get("360"),13.4,24));
        routeList.add(new Route(posdic.get("360"),posdic.get("777"),13.4,24));
        routeList.add(new Route(posdic.get("777"),posdic.get("X2"),35,30));
        routeList.add(new Route(posdic.get("X2"),posdic.get("777"),35,30));
        routeList.add(new Route(posdic.get("777"),posdic.get("336"),23,31));
        routeList.add(new Route(posdic.get("336"),posdic.get("777"),23,31));
        routeList.add(new Route(posdic.get("360"),posdic.get("X1"),4.8,8));
        routeList.add(new Route(posdic.get("X1"),posdic.get("360"),17.4,11));

        
        for(int i=0; i<routeList.size(); i++){
        	Route route = routeList.get(i);
        	route.From.Routes.put(route.To, route);
        }
        
	}
	
	
	static void LoadData()
	{
		String[] lines = csv.split(";" );


        for(int i=0; i<lines.length; i++)
        {
        	String line = lines[i];
        	String[] cols = line.split(",");
        	
        	if(cols.length>8){

	            String envid = cols[0];
	            Pos from = posdic.containsKey(cols[1]) == true ? posdic.get(cols[1]) : WH;
	            Pos to = posdic.containsKey(cols[2]) == true ? posdic.get(cols[2]) : WH;
	
	            if (posdic.containsKey(cols[1]) && posdic.containsKey(cols[2]))
	            {
	
	                int priority = Integer.parseInt(cols[7]);
	
	                Job j1 = new Job();
	                j1.EnvId = envid;
	                j1.Place = from;
	                j1.Action = "Pick Up";
	                j1.Priority=0;
	
	                Job j2 = new Job();
	                j2.EnvId = envid;
	                j2.Place = to;
	                j2.Action = "Drop Off";
	                j2.Depend = j1;
	                j2.Priority = priority;
	                j2.AfterHour = to.AfterHour;
	                jobList.add(j1);
	                jobList.add(j2);
	
	            }
        	}
        }
        
        for(int i=0; i<poslist.size(); i++){
        	Pos pos = poslist.get(i);
        	if(!pos.IsWH){
        		Job j1 = new Job();
                j1.EnvId = "Bill";
                j1.Place = pos;
                j1.Action = "Pick Up";
                j1.Priority=0;
                j1.IsPickBill=true;
                jobList.add(j1);
        	}
        }
	}
	
	static void SimplePrintMission(Schedule s ){
		
		System.out.print(s.AllDistanceKm + "km   \t" + s.AllTimeMin + " minute   \t" + s.AllBusiness + "%      \t");
		for(int i=0; i<s.MissionList.size(); i++){
			Mission m = s.MissionList.get(i);
			System.out.print("[" + m.Place.ID + "]  ");
		}
		
		System.out.println();
	}
	
	static void PrintMissionList(Schedule s){
		
		LinkedList<Mission> missionList = s.MissionList;
		System.out.println("------------------------------------------------------");
		System.out.println(" Schedule " + s.AllDistanceKm + "km   \t" + s.AllTimeMin + " minute   \t" + s.AllBusiness + "% ");
		for(int i=0; i<missionList.size(); i++){
			Mission m = missionList.get(i);
			
			String mDepend = "\t";
			if(m.GetDependPos()!=null){
				mDepend = " (" + m.GetDependPos().ID + ")";
			}
			
			System.out.println("   ["+ m.Place.ID + "] " + mDepend + "  Time " + m.EstimatedTime.get(Calendar.HOUR_OF_DAY)+ ":" + m.EstimatedTime.get(Calendar.MINUTE)  + " \tPriority=" + m.GetPriorty() + "  \tAfter=" + m.GetAfterHour());
			
			for (int j=0; j<m.Jobs.size(); j++)
			{
				Job job = m.Jobs.get(j);
				String jDepend = "";
				if(job.Depend!=null){
					jDepend = " <--" +  job.Depend.Place.ID ;
				}
				System.out.println("       - "  + job.Action + " " + job.EnvId  + " (" + job.Place.ID + jDepend + ")" );
			}
			
			System.out.println();
		}
		
		
	}

}

//------------------- Schedule ----------------------

class Schedule  implements Comparable<Schedule>{

	
	private Pos WH;
	private Pos startPos;
	private LinkedList<Pos> poslist = new LinkedList<Pos>();
	private Hashtable <String, Pos> posdic = new Hashtable <String, Pos>();
	private LinkedList<Pos> allPoses = new LinkedList<Pos>();
	private LinkedList<Job> depJobList = new LinkedList<Job>();
	private LinkedList<Job> indepJobList = new LinkedList<Job>();
	public LinkedList<Mission> MissionList = new LinkedList<Mission>();
	private Calendar startTime = Calendar.getInstance();
	
	public int HighBusiness = 1;
	public int ShortTime = 1;
	public int ShortDistance =1;
	
	public double AllDistanceKm = 0;
	public double AllTimeMin = 0;
	public double AllBusiness = 0;
	
	 public int compareTo(Schedule o) {
		 	int compare = 0;
	        int time = (int) this.AllTimeMin - (int)o.AllTimeMin ;
	        
	        if(ShortDistance>2)
	        {
	        	compare = (int) this.AllDistanceKm - (int)o.AllDistanceKm;
	        }else if(HighBusiness>2)
	        {
	        	compare = (int) o.AllBusiness - (int)this.AllBusiness;
	        }
	        
	        if(compare==0){
	        	compare = time;
	        }
	        
	        return compare;
	}
	
	public Schedule(LinkedList<Job> jobs, LinkedList<Pos> poses,Hashtable <String, Pos> posdict, Pos startPos,Calendar startTime)
	{
		this.allPoses.addAll(poses);
		this.startPos = startPos;
		this.posdic = posdict;
		this.startTime.set(Calendar.HOUR, startTime.get(Calendar.HOUR));
		this.startTime.set(Calendar.MINUTE, startTime.get(Calendar.MINUTE));
		
		
		//Find WH
		for(int i=0; i<poses.size();i++){
			Pos pos = poses.get(i);
			if(pos.IsWH){
				this.WH = pos;
				break;
			}
		}
		
		this.poslist.add(this.WH);
		
		//Separate independence and dependence mission
		for(int i=0; i<jobs.size();i++){
			Job job = jobs.get(i);
			if(job.Depend==null){
				this.indepJobList.add(job);
			}else{
				this.depJobList.add(job);
			}
			
			if(!this.poslist.contains(job.Place))
			{
				this.poslist.add(job.Place);
			}
		}
		
	}
	
	public void BuildSchedule(){
		GetherIndepPos();
        SortIndepPos(); //Not implement yet
        
        AddDependence();
        
        CleanSchedule1();
        CleanSchedule2();
        CleanSchedule3();
        CleanSchedule2();
        
        CalculateScore();
	}
	
	public void CalculateScore()
	{
		AllDistanceKm = 0;
		AllTimeMin = 0;
		AllBusiness = 0;
		
		Calendar estimate = Calendar.getInstance();
		//estimate.set(Calendar.HOUR, startTime.get(Calendar.HOUR));
		//estimate.set(Calendar.MINUTE, startTime.get(Calendar.MINUTE));
	
		double bizNum = 0;
		double bizVal =0;
		
		for(int i=0; i<MissionList.size()-1; i++){
			Mission e = MissionList.get(i);
			Mission f = MissionList.get(i+1);
			
			LinkedList<Route> r = GetRoute(e.Place,f.Place);
			
			if(r.size()>0){
				for(int j=0; j<r.size(); j++){
					Route ro = r.get(j);
					AllDistanceKm += ro.DistanceKM;
					AllTimeMin += ro.TimeMinutes +20;
					estimate.add(Calendar.MINUTE, (int)ro.TimeMinutes +20);
				}
			}else{
				AllDistanceKm +=40;
				AllTimeMin += 60;
				estimate.add(Calendar.MINUTE, 60);
			}
			
			f.EstimatedTime.set(Calendar.HOUR, estimate.get(Calendar.HOUR));
			f.EstimatedTime.set(Calendar.MINUTE, estimate.get(Calendar.MINUTE));
			
			if(f.GetPriorty()>0){
				bizNum++;
				
				Calendar target = Calendar.getInstance();
				target.set(Calendar.HOUR, startTime.get(Calendar.HOUR));
				target.set(Calendar.MINUTE, startTime.get(Calendar.MINUTE));
				
				target.add(Calendar.HOUR, f.GetPriorty());
				
				//System.out.println( estimate.get(Calendar.HOUR_OF_DAY) + ":" +estimate.get(Calendar.MINUTE) + " <= " + target.get(Calendar.HOUR_OF_DAY) + ":" + target.get(Calendar.MINUTE));
				
				if( estimate.get(Calendar.HOUR_OF_DAY) <= target.get(Calendar.HOUR_OF_DAY)){
					bizVal++;
					
				}
			}
			
		}
		
		AllDistanceKm = Math.round(AllDistanceKm*100)/100.00;
		AllTimeMin = Math.round(AllTimeMin*100)/100.00;
		
		if(bizNum>0){
			AllBusiness = bizVal*100.0/bizNum;
		}else{
			AllBusiness = 100;
		}
		AllBusiness = Math.round(AllBusiness*100)/100.00;
	}
	
	void GetherIndepPos()
    { 
		// Add first WH in mission
		boolean setStart = false;
		
		if(startPos == WH){
	        Mission missionFromWH = new Mission();
	        missionFromWH.Place = WH;
	        MissionList.add(missionFromWH);
	        
	        for(int i = 0; i<indepJobList.size(); i++)
	        {
	        	Job job = indepJobList.get(i);
	            if (job.Place.IsWH)
	            {
	            	missionFromWH.Jobs.add(job);
	            }
	        }
	
	        if (missionFromWH.Jobs.size() == 0)
	        {
	            Job job = new Job();
	            job.Place = WH;
	            job.EnvId = "";
	            job.Action = "Start";
	            missionFromWH.Jobs.add(job);
	        }
	        
	        setStart=true;
		}
        // Add all POS to be Mission
        
        for (int i=0; i<poslist.size(); i++)
        {
        	if(!poslist.get(i).IsWH){
        		Mission m = new Mission();
        		m.Place = poslist.get(i);
        		
        		if(!setStart && m.Place == startPos){
        			MissionList.add(0,m);
        			setStart=true;
        		}else{
        			MissionList.add(m);
        		}
        	}
        }
        
        if(!setStart){
        	Mission m = new Mission();
    		m.Place = startPos;
    		MissionList.add(0,m);
        }
        
        // Add job to Mission
        for(int j = 0; j<indepJobList.size(); j++)
        {
        	Job job = indepJobList.get(j);
        	if (!job.Place.IsWH)
            {
	        	for(int m = 0; m<MissionList.size(); m++){
	        		Mission mission = MissionList.get(m);
	        		if(job.Place.ID == mission.Place.ID)
	        		{
	        			mission.Jobs.add(job);
	        		}
	        	}
            }
        }
        
        //Add last mission
        Mission missionToWH = new Mission();
        missionToWH.Place = WH;
        MissionList.add(missionToWH);
        
        for(int i = 0; i<depJobList.size(); i++)
        {
        	Job job = depJobList.get(i);
            if (job.Place.IsWH)
            {
            	missionToWH.Jobs.add(job);
            }
        }

        if (missionToWH.Jobs.size() == 0)
        {
            Job job = new Job();
            job.Place = WH;
            job.EnvId = "";
            job.Action="End";
            missionToWH.Jobs.add(job);
        }

    }

	void SortIndepPos()
    {
		LinkedList<Mission> suff = new LinkedList<Mission>();
		
		suff.addAll(MissionList);
		Mission first = suff.getFirst();
		Mission last = suff.getLast();
		
		
		double cost = 9999999;
		
		for (int i=0; i<20; i++){
			LinkedList<Mission> s = new LinkedList<Mission>();
			s.addAll(suff);
			s.remove(last);
			s.remove(first);
			Collections.shuffle(s);
			
			
		/*	//Add
			Mission p118 = null;
			Mission p309 = null;
			
			Mission pS38 = null;
			Mission p302 = null;
			
			for(int j=0; j<s.size(); j++){
				Mission m = s.get(j);
				if(m.Place.ID == "118"){
					p118 = m;
				}else
				if(m.Place.ID == "S38"){
					pS38 = m;
				}else
				if(m.Place.ID == "302"){
					p302 = m;
				}else
				if(m.Place.ID == "309"){
					p309 = m;
				}
			}
			
			//Swap X1
			if(p118!=null && p309!=null){
				double b = Math.random();
				if(b>0.5){
					Mission x = p118;
					p118=p309;
					p309=x;
				}
				s.remove(p118);
				for(int j=0; j<s.size(); j++){
					Mission m = s.get(j);
					if(m==p309){
						s.add(j+1, p118);
					}
				}
			}
			
			//Swap X2
			if(p302!=null && pS38!=null){
				double b = Math.random();
				if(b>0.5){
					Mission x = p302;
					p302=pS38;
					pS38=x;
				}
				s.remove(pS38);
				for(int j=0; j<s.size(); j++){
					Mission m = s.get(j);
					if(m==p302){
						s.add(j+1, pS38);
					}
				}
			}
			
		*/
			
			//add first / last
			s.addFirst(first);
			s.addLast(last);
			double c = GetCost(s);
			if(c<cost){
				cost=c;
				suff= s;
			}
		}
		
		//System.out.println("Total = " + cost);
		
		MissionList = suff;
    }
	
	public double GetCost(){
		return Math.round( GetCost(MissionList) *100)/100.0;
	}
	
	 double GetCost(LinkedList<Mission> missions){
		double d = 0.0;
		
		for(int i=0; i<missions.size()-1; i++){
			Mission e = missions.get(i);
			Mission f = missions.get(i+1);
			
			LinkedList<Route> r = GetRoute(e.Place,f.Place);
			double c = 0.0;
			if(r.size()>0){
				for(int j=0; j<r.size(); j++){
					c+= getRouteCost(r.get(j));
				}
			}else{
				c=40.0;
			}
			//c*=r.size();
			//c = Math.scalb(c, 2);
			c = Math.round(c*100)/100.0;
			
			d+= c;
			
			//System.out.print(e.Place.ID + "(" + c +  ") , ");
		}
		
		
		//System.out.print(missions.getLast().Place.ID);
		
		//System.out.println("      =     " + d);
		
		return d;
	}
	
	double getRouteCost(Route r)
	{
		double cost = 0;
		
		if(this.ShortDistance > this.ShortDistance){
			cost = r.DistanceKM;
		}else{
			cost = r.TimeMinutes;
		}
		
		return cost;
	}
	
	LinkedList<Route> GetRoute(Pos f, Pos t){
		
		Pos from = posdic.get(f.ID);
		Pos to = posdic.get(t.ID);
		
		if(from.ID=="309" || from.ID=="118"){
			from = posdic.get("X1");
		}
		if(from.ID=="302" || from.ID=="S38"){
			from = posdic.get("X2");
		}
		
		if(to.ID=="309" || to.ID=="118"){
			to = posdic.get("X1");
		}
		if(to.ID=="302" || to.ID=="S38"){
			to = posdic.get("X2");
		}
		
		
		LinkedList<Route> r = new LinkedList<Route>();
		if(from.Routes.containsKey(to)){
			r.add(from.Routes.get(to));
		}else{
			double distance = 999999;
			
			for(Pos p : from.Routes.keySet()){
				Route r1 = from.Routes.get(p);
				if(p.Routes.containsKey(to)){
					Route r2 = p.Routes.get(to);
					double d = getRouteCost(r1) + getRouteCost(r2);
					if(d<distance){
						distance = d;
						r.clear();
						r.add(r1);
						r.add(r2);
					}
				}
			}
		}
		return r;
	}
	
	void AddDependence()
	{
		LinkedList<Mission> depMissionList = new LinkedList<Mission>();
		//Add indepJob
		for(int j = 0; j<depJobList.size(); j++)
        {
        	Job job = depJobList.get(j);
        	
        	if (!job.Place.IsWH)
            {
        		boolean isAdded = false;
	        	for(int m = 0; m<depMissionList.size(); m++){
	        		Mission mission = depMissionList.get(m);
	        		if(job.Place == mission.Place && job.Depend.Place == mission.GetDependPos())
	        		{
	        			mission.Jobs.add(job);
	        			isAdded = true;
	        			break;
	        		}
	        	}
	        	
	        	if(!isAdded)
	        	{
        			Mission newMission = new Mission();
        			newMission.Place = job.Place;
        			newMission.Jobs.add(job);
        			depMissionList.add(newMission);
        		}
            }
        }
		
		
		//Try to add mission
		for(int i=0; i<depMissionList.size(); i++)
		{
			Mission m = depMissionList.get(i);
			AddMission(m);
		}
		
		
		
	}
	
	void AddMission(Mission m)
	{
		//Find start index
		int startIndex = 0;
		
		for(int i=0; i<MissionList.size(); i++)
		{
			if(m.GetDependPos() == MissionList.get(i).Place)
			{
				startIndex = i+1;
				break;
			}
		}
		
		if(startIndex==0){
			
			startIndex = MissionList.size()-1;
		}
		
		
		//Loop to get the best position
		double cost = 99999;
		int suitableIndex = startIndex;
		for(int i=startIndex; i<MissionList.size(); i++)
		{
			if(MissionList.get(i).Place==m.Place)
			{
				suitableIndex = i;
				break;
			}
			double c = CalculateCostFromAddedIndex(m,i);
			if(c < cost){
				cost = c;
				suitableIndex = i;
			}
		}
		//System.out.println(startIndex+"<"+suitableIndex);
		//Add the best position

		MissionList.add(suitableIndex, m);
	}
	
	double CalculateCostFromAddedIndex(Mission m, int index)
	{
		LinkedList<Mission> missions = new LinkedList<Mission>();
		missions.addAll(MissionList);
		missions.add(index, m);
		
		return GetCost(missions);
	}
	
	void CleanSchedule1()
	{
		//Generate EmptyMission and JopedMission
		LinkedList<Mission> billJob = new LinkedList<Mission>();
		LinkedList<Mission> havingJob = new LinkedList<Mission>();
		
		for(int i=1; i<MissionList.size()-1; i++){
			Mission m = MissionList.get(i);
			if(m.IsOnlyBill()){
				billJob.add(m);
			}else{
				havingJob.add(m);
			}
		}
		
		for(int e=0; e<billJob.size(); e++){
			Mission bill = billJob.get(e);
			for(int h=0; h<havingJob.size(); h++){
				Mission having = havingJob.get(h);
				if(bill!=having && bill.Place==having.Place){
					having.Jobs.add(bill.Jobs.get(0));
					MissionList.remove(bill);
					break;
				}
				
			}
		}
	
	}
	
	void CleanSchedule2()
	{
		LinkedList<Mission> result = new LinkedList<Mission>();
		for(int i=0; i<MissionList.size()-1; i++){
			Mission m1 = MissionList.get(i);
			Mission m2 = MissionList.get(i+1);
			
			if(m1.Place == m2.Place){
				m1.Jobs.addAll(m2.Jobs);
				result.add(m1);
				i++;
			}else{
				result.add(m1);
			}
		}
		result.add(MissionList.getLast());
		
		MissionList = result;
	}
	
	void CleanSchedule3()
	{
		CalculateScore();

		LinkedList<Mission> wrong = new LinkedList<Mission>();
		
		for(int i=0; i<MissionList.size(); i++){
			Mission m = MissionList.get(i);
			int targetHour = m.GetAfterHour() ;
			int estHour = m.EstimatedTime.get(Calendar.HOUR_OF_DAY);
			if(m.GetAfterHour()>0 && estHour<targetHour){
				wrong.add(m);
				//System.out.println(estHour + "<" + targetHour);
			}
		}
		
		for(int i=0; i<wrong.size(); i++){
			MissionList.remove(wrong.get(i));
		}
		
		for(int i=0; i<wrong.size(); i++)
		{
			Mission m = wrong.get(i);
			AddMission2(m);
		}
	}

	void AddMission2(Mission m)
	{
		//Find start index
		int startIndex = 0;
		
		for(int i=0; i<MissionList.size(); i++)
		{
			int targetHour = m.GetAfterHour() ;
			int estHour = m.EstimatedTime.get(Calendar.HOUR_OF_DAY);
			if(estHour > targetHour)
			{
				startIndex = i+1;
				break;
			}
		}
		
		if(startIndex==0){
			
			startIndex = MissionList.size()-1;
		}
		
		
		//Loop to get the best position
		double cost = 99999;
		int suitableIndex = startIndex;
		for(int i=startIndex; i<MissionList.size(); i++)
		{
			if(MissionList.get(i).Place==m.Place)
			{
				suitableIndex = i;
				break;
			}
			double c = CalculateCostFromAddedIndex(m,i);
			if(c < cost){
				cost = c;
				suitableIndex = i;
			}
		}
		//System.out.println(startIndex+"<"+suitableIndex);
		//Add the best position
		MissionList.add(suitableIndex, m);
	}
}

//------------------- POS ----------------------
class Pos
{
    public String ID;
    public boolean IsWH;
    public int AfterHour = 0;
    public Hashtable <Pos, Route> Routes = new Hashtable <Pos, Route>();

    public Pos(String id)
    {
        this.ID = id;
        IsWH = false;
    }
}

//------------------- JOB ----------------------
class Job
{
    public String EnvId;
    public Pos Place;
    public int Priority;
    public Job Depend;
    public String Action = "";
    public boolean IsPickBill = false;
    public int AfterHour =0;
    
    public Job()
    {

    }
}

//------------------- Mission ----------------------
class Mission
{
	public LinkedList<Job> Jobs = new  LinkedList<Job>();
	public Pos Place;
	public Calendar EstimatedTime = Calendar.getInstance();
	public int GetAfterHour(){
		int h = 0;
		
		for(int i=0; i<Jobs.size();i++){
			Job job = Jobs.get(i);
			if(job.AfterHour>h)
			{
				h=job.AfterHour;
			}
		}
		
		return h;
	}
	public boolean IsOnlyBill(){
		boolean res = false;
		
		if(Jobs.size()==1){
			res = Jobs.get(0).IsPickBill;
		}
		
		return res;
	}
	
	public Pos GetDependPos(){
		Pos depend = null;
		
		if(Jobs.size()>0 && Jobs.get(0)!=null && Jobs.get(0).Depend!=null && Jobs.get(0).Depend.Place!=null)
		{
			depend = Jobs.get(0).Depend.Place;
		}
		
		return depend;
	}
	
	
	
	public boolean HasJobs(){
		return Jobs.size()>0?true:false;
	}

	public int GetPriorty()
	{
		int priority = 99;
		
		for(int i=0; i<Jobs.size(); i++)
		{
			int p = Jobs.get(i).Priority;
			if(p>0 && p<priority )
			{
				priority = p;
			}
		}
		
		return priority==99?0:priority;
	}
	
	public Mission()
	{
	}
	
	
}

//---------------------------
class Route{
	public Pos From;
	public Pos To;
	public double DistanceKM;
	public double TimeMinutes;
	public Route(Pos from, Pos to, double km, double min){
		this.From=from;
		this.To = to;
		this.DistanceKM = km;
		this.TimeMinutes = min;
	}
	
	public double GetTrafficCost(){
		return Math.random()*10;
	}
}
